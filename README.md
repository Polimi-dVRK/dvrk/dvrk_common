
## `dvrk_common` 

This is a kitchen sink library. Documentation can be found [here](https://dvrk.comkieffer.com/dvrk_common)
 It used as the base for the `dvrk_*` family of libraries and is divided in 3 parts. 

The `dvrk::error` module provides a consistent set of error classes based on [boost::exception](http://www.boost.org/doc/libs/1_65_1/libs/exception/doc/boost-exception.html).

The `dvrk::opencv` module provides helpers for __OpenCV__ including:

- Pre-defined colour names for all the colours in the CSS3 standard such as `dvrk::Colour::LawnGreen` and `dvrk::Colour::DarkSalmon`. These make it significantly easier to create pretty(-er) interfaces using OpenCV
- A C++ enum for the common keys on a keyboard in the `dvrk::KeyCode` enum and a `dvrk::waitKey` function that returns a `dvrk::KeyCode` which helps to make OpenCV keyboard handling code more readable
- Utility functions such as `dvrk::toggleFullScreen` and `dvrk::drawCross` to fill some gaps in the OpenCV drawing utilities. 
- A handful of functions to parse `cv::Point`, `cv::Rect` and `cv::Size` objects from strings

The third module, `dvrk::ros`, is the most developed of the lot. It contains

- Helpers for ROS cameras. The `dvrk::SimpleCamera` class for example removes all the boilerplate required to connect to a ROS camera/camera_info pair. It implements common patterns such as ensuring that at least one image has been received or that the subscribed topic actually exists whilst providing a simple interface. The `dvrk::StereoCamera` extends the interface to subscribe and synchronise two image topics without complicating the interface and the `dvrk::ExtendedStereoCamera` covers the use-case where you might want to subscribe to both the colour and mono version of a given set of stereo images. 
- A handful of conversion functions to convert between ROS messages and OpenCV types
- Some functions that we felt were missing from the ROS libraries such as as version of `ros::names::append` that takes an arbitrary number of arguments or equality operators for some message types

It also contains a handful of math functions such as `clamp` whilst we wait for `C++17` (which includes a clamp function) and the deceptively complicated `dvrk::aprox_equal` function to compare floats. 

This library, in its current state would probably not be of much use to a user outside of our lab but I believe that some of the classes and functions it provides might be of use to others. The ROS cameras for example are very convenient and make developing image processing code that much easier.
