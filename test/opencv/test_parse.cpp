//
// Created by tibo on 12/07/17.
//

#include <gtest/gtest.h>

#include <dvrk_common/opencv/parse.hpp>

using namespace dvrk;

TEST(TestParseRect, simpleConversions) {
    EXPECT_EQ(cv::Rect(0, 0, 10, 10), parseRect("0, 0, 10, 10").value());
    EXPECT_EQ(cv::Rect(10, 10, 0, 0), parseRect("10, 10, 0, 0").value());
    EXPECT_EQ(cv::Rect(10, 10, 100, 100), parseRect("10, 10, 100, 100").value());
}

TEST(TestParseRect, whitespaceInsensitivity) {
    cv::Rect result(10, 10, 100, 100);

    EXPECT_EQ(result, parseRect("  10, 10, 100, 100").value());
    EXPECT_EQ(result, parseRect("10  , 10, 100, 100").value());
    EXPECT_EQ(result, parseRect("10,   10, 100, 100").value());
    EXPECT_EQ(result, parseRect("10, 10, 100, 100  ").value());
}

TEST(TestParseRect, incompleteStringFailures) {
    // Since we use boost::optional to return the value, if an error occurs the result will evaluate to false
    EXPECT_FALSE(parseRect(""));
    EXPECT_FALSE(parseRect("10, 10, 100,"));
}

TEST(TestParseRect, invalidSeparator) {
    EXPECT_FALSE(parseRect("10 10 100 100"));
    EXPECT_FALSE(parseRect("10-10-100-100"));
}

TEST(TestParsePoint, simpleConversions) {
    EXPECT_EQ(cv::Point(0, 0), parsePoint("0, 0").value());
    EXPECT_EQ(cv::Point(10, 0), parsePoint("10, 0").value());
    EXPECT_EQ(cv::Point(-10, 0), parsePoint("-10, 0").value());
    EXPECT_EQ(cv::Point(0, -10), parsePoint("0, -10").value());
}

TEST(TestParsePoint, whitespaceInsensitivity) {
    cv::Point result(10, 10);

    EXPECT_EQ(result, parsePoint("  10, 10").value());
    EXPECT_EQ(result, parsePoint("10  ,10").value());
    EXPECT_EQ(result, parsePoint("10,   10").value());
    EXPECT_EQ(result, parsePoint("10, 10  ").value());
}

TEST(TestParsePoint, incompleteStringFailures) {
    // Since we use boost::optional to return the value, if an error occurs the result will evaluate to false
    EXPECT_FALSE(parsePoint(""));
    EXPECT_FALSE(parsePoint("10, "));
}

TEST(TestParsePoint, invalidSeparator) {
    EXPECT_FALSE(parsePoint("10 10"));
    EXPECT_FALSE(parsePoint("10-10"));
    EXPECT_FALSE(parsePoint("10x10"));
}

TEST(TestParseSize, simpleConversions) {
    EXPECT_EQ(cv::Size(0, 0), parseSize("0x0").value());
    EXPECT_EQ(cv::Size(10, 0), parseSize("10x0").value());
    EXPECT_EQ(cv::Size(0, 10), parseSize("0x10").value());
}

TEST(TestParseSize, whitespaceInsensitivity) {
    cv::Size result(10, 10);

    EXPECT_EQ(result, parseSize("  10x 10").value());
    EXPECT_EQ(result, parseSize("10  x10").value());
    EXPECT_EQ(result, parseSize("10x   10").value());
    EXPECT_EQ(result, parseSize("10x 10  ").value());
}

TEST(TestParseSize, incompleteStringFailures) {
    // Since we use boost::optional to return the value, if an error occurs the result will evaluate to false
    EXPECT_FALSE(parseSize(""));
    EXPECT_FALSE(parseSize("10x "));
}

TEST(TestParseSize, invalidSeparator) {
    EXPECT_FALSE(parseSize("10 10"));
    EXPECT_FALSE(parseSize("10-10"));
    EXPECT_FALSE(parseSize("10,10"));
}
