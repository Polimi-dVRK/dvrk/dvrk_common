//
// Created by tibo on 07/09/17.
//

#include <iostream> // TODO: REMOVE ME

#include <gtest/gtest.h>

#include <dvrk_common/opencv/util.hpp>
#include <dvrk_common/opencv/image/hamming_distance_image.hpp>

TEST(TestHamming, TestHammingDistance)
{
    std::vector<uint8_t> lhs_data = {
        0b11100111, 0b00101101, 0b10110011, 0b11111010, 0b11101110,
        0b11001100, 0b01010101, 0b00111011, 0b11000000, 0b00101111,
        0b10101110, 0b01110111, 0b00001101, 0b01100000, 0b11101001,
        0b00001001, 0b00111100, 0b11001001, 0b11011100, 0b10110111,
        0b11100000, 0b01110111, 0b10011110, 0b10000011, 0b11010010,
    };
    cv::Mat lhs(5, 5, CV_8UC1, lhs_data.data());

    std::vector<uint8_t> rhs_data{
        0b01011010, 0b01110001, 0b01111101, 0b11111001, 0b01010100,
        0b10100011, 0b00101111, 0b01001011, 0b00000101, 0b10000001,
        0b00111010, 0b11001001, 0b00011011, 0b11001101, 0b11100110,
        0b01011001, 0b10110011, 0b01110010, 0b00000100, 0b10011001,
        0b11100000, 0b01100110, 0b11111111, 0b00100011, 0b10011001,
    };
    cv::Mat rhs(5, 5, CV_8UC1, rhs_data.data());
    
    std::vector<uint16_t> result_data{
        6, 4, 5, 2, 5,
        6, 5, 3, 4, 5,
        3, 6, 3, 5, 4,
        2, 5, 6, 4, 4,
        0, 2, 3, 2, 4,
    };
    cv::Mat expected_hd_result(5, 5, CV_16U, result_data.data());
    
    dvrk::HammingDistanceImage hdi;
    EXPECT_TRUE(dvrk::is_equal(expected_hd_result, hdi.apply(lhs, rhs)));
}
