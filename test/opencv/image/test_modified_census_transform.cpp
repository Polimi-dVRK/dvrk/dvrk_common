
#include <iostream>
#include <gtest/gtest.h>

#include <dvrk_common/error_base.hpp>
#include <dvrk_common/opencv/image/modified_census_transform.hpp>

using namespace dvrk;

// TODO: fix test pixel index stuff
class TestableModifiedCensusTransform : public ModifiedCensusTransform {
public:
    TestableModifiedCensusTransform(
        const cv::Size &src_img_size, const cv::Size &window_size
    ) : ModifiedCensusTransform( window_size) {
        // Here we cheat. Normally the _data member would be initialised when we call apply().
        // Since, in tests, most of the time we are simply setting a pixel and checking its value we need to the _data
        // to not be empty.
        _data = cv::Mat::zeros(src_img_size, CV_8UC(_bytes_per_pixel));
    }

    using ModifiedCensusTransform::setPixelAt;
    using ModifiedCensusTransform::getPixelAt;
    using ModifiedCensusTransform::processPixel;

    inline cv::Mat& getDataMember() { return _data; }

};

// Small utility function used to debug tests by printing a range of _data
void printRange(const std::vector<uint8_t> &data, const size_t px_size, const size_t start_idx, const size_t end_idx)
{
    std::cout << "Printing range [" << start_idx << "; " << end_idx << "]\n";
    for (auto i = start_idx; i < end_idx; i++) {
        if (i % px_size == 0) {
            std::cout << " - ";
        } else {    
            std::cout << " ";
        };

        std::cout << static_cast<int>(data.at(i));

    }
    std::cout << std::endl;
}

TEST(TestMCT, TestBytesPerPixel) {
    // TODO: Require a window of at least 3 pixels ?
    TestableModifiedCensusTransform mct_1(cv::Size(12, 12), cv::Size(3, 3));
    EXPECT_EQ(2, mct_1.getBytesPerPixel());

    TestableModifiedCensusTransform mct_2(cv::Size(12, 12), cv::Size(5, 5));
    EXPECT_EQ(6, mct_2.getBytesPerPixel());

    TestableModifiedCensusTransform mct_3(cv::Size(12, 12), cv::Size(7, 7));
    EXPECT_EQ(12, mct_3.getBytesPerPixel());

    TestableModifiedCensusTransform mct_4(cv::Size(12, 12), cv::Size(9, 9));
    EXPECT_EQ(20, mct_4.getBytesPerPixel());

    TestableModifiedCensusTransform mct_5(cv::Size(12, 12), cv::Size(11, 11));
    EXPECT_EQ(30, mct_5.getBytesPerPixel());
}


TEST(TestMCT, TestSetPixelAtWithSingleByte)
{
    std::vector<uint8_t> px_values{0b10100101};

    TestableModifiedCensusTransform mct_1(cv::Size(10, 10), cv::Size(1, 1));
    mct_1.setPixelAt(0, 0, px_values);
    const auto& mct_1_data = mct_1.getDataMember();
    EXPECT_EQ(mct_1_data.ptr<uint8_t>(0, 0)[0], px_values[0]);

    TestableModifiedCensusTransform mct_2(cv::Size(10, 10), cv::Size(1, 1));
    mct_2.setPixelAt(0, 5, px_values);
    const auto& mct_2_data = mct_2.getDataMember();
    EXPECT_EQ(mct_2_data.ptr<uint8_t>(0, 5)[0], px_values[0]);

    TestableModifiedCensusTransform mct_3(cv::Size(10, 10), cv::Size(1, 1));
    mct_3.setPixelAt(5, 5, px_values);
    const auto& mct_3_data = mct_3.getDataMember();
    EXPECT_EQ(mct_3_data.ptr<uint8_t>(5, 5)[0], px_values[0]);
}

TEST(TestMCT, TestSetPixelAtWithTwoBytes)
{
    std::vector<uint8_t> px_values{0b10000001, 0b01000010};
    
    TestableModifiedCensusTransform mct_1(cv::Size(10, 10), cv::Size(3, 3));
    mct_1.setPixelAt(0, 0, px_values);
    
    const auto& mct_1_data = mct_1.getDataMember();
    EXPECT_EQ(mct_1_data.ptr<uint8_t>(0, 0)[0], px_values[0]);
    EXPECT_EQ(mct_1_data.ptr<uint8_t>(0, 0)[1], px_values[1]);

    TestableModifiedCensusTransform mct_2(cv::Size(10, 10), cv::Size(3, 3));
    mct_2.setPixelAt(0, 3, px_values);
    
    const auto& mct_2_data = mct_2.getDataMember();
    EXPECT_EQ(mct_2_data.ptr<uint8_t>(0, 3)[0], px_values[0]);
    EXPECT_EQ(mct_2_data.ptr<uint8_t>(0, 3)[1], px_values[1]);

    TestableModifiedCensusTransform mct_3(cv::Size(10, 10), cv::Size(3, 3));
    mct_3.setPixelAt(6, 0, px_values);
    
    const auto& mct_3_data = mct_3.getDataMember();
    EXPECT_EQ(mct_3_data.ptr<uint8_t>(6, 0)[0], px_values[0]);
    EXPECT_EQ(mct_3_data.ptr<uint8_t>(6, 0)[1], px_values[1]);


    TestableModifiedCensusTransform mct_4(cv::Size(10, 10), cv::Size(3, 3));
    mct_4.setPixelAt(3, 9, px_values);
    
    const auto& mct_4_data = mct_4.getDataMember();
    EXPECT_EQ(mct_4_data.ptr<uint8_t>(3, 9)[0], px_values[0]);
    EXPECT_EQ(mct_4_data.ptr<uint8_t>(3, 9)[1], px_values[1]);
}

TEST(TestMCT, TestSetPixelAtWithSixBytes)
{
    std::vector<uint8_t> px_values{0b00000001, 0b00000010, 0b0000100, 0b00001000, 0b00010000, 0b00100000};

    TestableModifiedCensusTransform mct_1(cv::Size(10, 10), cv::Size(5, 5));
    mct_1.setPixelAt(3, 7, px_values);
    
    const auto& mct_1_data = mct_1.getDataMember();
    EXPECT_EQ(mct_1_data.ptr<uint8_t>(3, 7)[0], px_values[0]);
    EXPECT_EQ(mct_1_data.ptr<uint8_t>(3, 7)[1], px_values[1]);
    EXPECT_EQ(mct_1_data.ptr<uint8_t>(3, 7)[2], px_values[2]);
    EXPECT_EQ(mct_1_data.ptr<uint8_t>(3, 7)[3], px_values[3]);
    EXPECT_EQ(mct_1_data.ptr<uint8_t>(3, 7)[4], px_values[4]);
    EXPECT_EQ(mct_1_data.ptr<uint8_t>(3, 7)[5], px_values[5]);
}

TEST(TestMCT, TestGetPixelAtWithSingleByte) {
    std::vector<uint8_t> px_values{0b10100101};

    TestableModifiedCensusTransform mct_1(cv::Size(10, 10), cv::Size(1, 1));
    mct_1.setPixelAt(0, 0, px_values);
    EXPECT_EQ(mct_1.getPixelAt(0, 0), px_values);

    TestableModifiedCensusTransform mct_2(cv::Size(10, 10), cv::Size(1, 1));
    mct_2.setPixelAt(0, 5, px_values);
    EXPECT_EQ(mct_2.getPixelAt(0, 5), px_values);

    TestableModifiedCensusTransform mct_3(cv::Size(10, 10), cv::Size(1, 1));
    mct_3.setPixelAt(5, 5, px_values);
    EXPECT_EQ(mct_3.getPixelAt(5, 5), px_values);
}

TEST(TestMCT, TestGetPixelAtWithTwoBytes)
{
    std::vector<uint8_t> px_values{0b10000001, 0b01000010};

    TestableModifiedCensusTransform mct_1(cv::Size(10, 10), cv::Size(3, 3));
    mct_1.setPixelAt(0, 0, px_values);
    EXPECT_EQ(mct_1.getPixelAt(0, 0), px_values);

    TestableModifiedCensusTransform mct_2(cv::Size(10, 10), cv::Size(3, 3));
    mct_2.setPixelAt(0, 3, px_values);
    EXPECT_EQ(mct_2.getPixelAt(0, 3), px_values);

    TestableModifiedCensusTransform mct_3(cv::Size(10, 10), cv::Size(3, 3));
    mct_3.setPixelAt(6, 0, px_values);
    EXPECT_EQ(mct_3.getPixelAt(6, 0), px_values);

    TestableModifiedCensusTransform mct_4(cv::Size(10, 10), cv::Size(3, 3));
    mct_4.setPixelAt(3, 9, px_values);
    EXPECT_EQ(mct_4.getPixelAt(3, 9), px_values);
}

TEST(TestMCT, TestGetPixelAtWithSixBytes)
{
    std::vector<uint8_t> px_values{0b00000001, 0b00000010, 0b0000100, 0b00001000, 0b00010000, 0b00100000};

    TestableModifiedCensusTransform mct_1(cv::Size(10, 10), cv::Size(5, 5));
    mct_1.setPixelAt(9, 3, px_values);
    EXPECT_EQ(mct_1.getPixelAt(9, 3), px_values);
}

TEST(TestMCT, TestProcessPixel) {
    TestableModifiedCensusTransform mct(cv::Size(10, 10), cv::Size(3, 3));

    std::vector<uint8_t> img_data{
        255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
        255, 128, 53, 128, 128, 128, 128, 128, 128, 255,
        255, 211, 162, 128, 128, 128, 128, 128, 128, 255,
        255, 128, 128, 128, 128, 128, 128, 128, 128, 255,
        255, 128, 128, 128,  32,  91, 92, 128, 128, 255,
        255, 128, 128, 128,  67, 103, 101, 128, 128, 255,
        255, 128, 128, 128, 111,  12, 133, 128, 128, 255,
        255, 128, 128, 128, 128, 128, 128, 128, 128, 255,
        255, 128, 128, 128, 128, 128, 128, 128, 128, 255,
        255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    };

    cv::Mat img(10, 10, CV_8U, static_cast<void*>(img_data.data()));
    std::vector<uint8_t> result;

    // Process the pixel at (1, 1). The support mask is:
    //
    // | 255, 255, 255 |        p = 128
    // | 255, 128,  53 |  ==>   mean = 192  ==> C = 11 11 11 11 00 11 11 10
    // | 255, 211, 162 |
    //
    cv::Mat first_window(img, cv::Rect(0, 0, 3, 3));
    cv::Point first_pt(1, 1);

    mct.processPixel(first_window, result);
    EXPECT_EQ(result.size(), mct.getBytesPerPixel());
    EXPECT_EQ(result.at(0), 0b11111111);
    EXPECT_EQ(result.at(1), 0b00111110);

    result.clear();

    // Process the pixel at (5, 5). The support mask is:
    //
    // |  32,  91,  92 |        p = 103
    // |  67, 103, 101 |  ==>   mean = 82  ==> C = 00 01 01 00 01 11 00 11
    // | 111,  12, 133 |
    //
    cv::Mat second_window(img, cv::Rect(4, 4, 3, 3));
    cv::Point second_pt(5, 5);
    mct.processPixel(second_window, result);
    EXPECT_EQ(result.at(0), 0b00010100);
    EXPECT_EQ(result.at(1), 0b01110011);
}
