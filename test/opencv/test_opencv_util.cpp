//
// Created by tibo on 07/09/17.
//

#include <gtest/gtest.h>

#include <dvrk_common/opencv/util.hpp>

TEST(TestIsEqual, IsEqual) {

    std::vector<uint8_t> first_mat_data{
        6, 4, 5, 2, 4,
        6, 6, 3, 4, 5,
        3, 6, 3, 5, 4,
        2, 5, 6, 5, 4,
        0, 2, 3, 2, 4,
    };
    cv::Mat first_mat(5, 5, CV_8UC1, first_mat_data.data());

    std::vector<uint8_t> second_mat_data{
        6, 6, 3, 4, 5,
        6, 4, 5, 2, 4,
        2, 5, 6, 5, 4,
        0, 2, 3, 2, 4,
        3, 6, 3, 5, 4,
    };
    cv::Mat second_mat(5, 5, CV_8UC1, second_mat_data.data());

    std::vector<uint8_t> third_mat_data{
        6, 4, 5, 2, 4, 6, 4, 5, 2, 4,
        6, 6, 3, 4, 5, 2, 5, 6, 5, 4,
        0, 2, 3, 2, 4, 3, 6, 3, 5, 4,
    };
    cv::Mat third_mat(5, 5, CV_8UC1, third_mat_data.data());
    
    // Check that two empty matrices are considered equal
    cv::Mat first_empty_mat, second_empty_mat;
    EXPECT_TRUE(dvrk::is_equal(first_empty_mat, second_empty_mat));

    // Test that two matrices that share the same data pointer are equal
    cv::Mat first_mat_copy = first_mat;
    EXPECT_TRUE(dvrk::is_equal(first_mat, first_mat_copy));

    // Test that two matrices that have the same contents are equal
    std::vector<uint8_t> new_first_mat_data = first_mat_data;
    cv::Mat new_first_mat(5, 5, CV_8UC1, new_first_mat_data.data());
    EXPECT_TRUE(dvrk::is_equal(first_mat, new_first_mat));
    
    // Test that two matrices with the same size but different contents are not equal
    EXPECT_FALSE(dvrk::is_equal(first_mat, second_mat));
    
    // Test that two different matrices with differennt size are not equal
    EXPECT_FALSE(dvrk::is_equal(first_mat, third_mat));

    // Test that two images with different numbers of channels are different
    cv::Mat ones_mat_0 = cv::Mat::ones(10, 10, CV_MAKE_TYPE(CV_8U, 3));
    EXPECT_FALSE(dvrk::is_equal(first_mat, ones_mat_0));

    // Test that two idential 3-channel images are equal
    cv::Mat ones_mat_1 = cv::Mat::ones(10, 10, CV_MAKE_TYPE(CV_8U, 3));
    EXPECT_TRUE(dvrk::is_equal(ones_mat_0, ones_mat_1));

    // Test that two different 3-channel images are different
    cv::Mat zeros_mat_0 = cv::Mat::zeros(10, 10, CV_MAKE_TYPE(CV_8U, 3));
    EXPECT_FALSE(dvrk::is_equal(ones_mat_0, zeros_mat_0));
}


TEST(TestMatTypeToString, TestOk) {
    cv::Mat m1(10, 10, CV_8U);
    EXPECT_EQ("8UC1", dvrk::matTypeToString(m1));
    
    cv::Mat m2(10, 10, CV_8UC1);
    EXPECT_EQ("8UC1", dvrk::matTypeToString(m2));
    
    cv::Mat m3(10, 10, CV_8UC2);
    EXPECT_EQ("8UC2", dvrk::matTypeToString(m3));
    
    cv::Mat m4(10, 10, CV_8UC4);
    EXPECT_EQ("8UC4", dvrk::matTypeToString(m4));
    
    cv::Mat m5(10, 10, CV_8UC(7));
    EXPECT_EQ("8UC7", dvrk::matTypeToString(m5));
    
    cv::Mat m6(10, 10, CV_8SC2);
    EXPECT_EQ("8SC2", dvrk::matTypeToString(m6));
    
    cv::Mat m7(10, 10, CV_16UC3);
    EXPECT_EQ("16UC3", dvrk::matTypeToString(m7));
    
    cv::Mat m8(10, 10, CV_16SC4);
    EXPECT_EQ("16SC4", dvrk::matTypeToString(m8));
    
    cv::Mat m9(10, 10, CV_32SC2);
    EXPECT_EQ("32SC2", dvrk::matTypeToString(m9));
    
    cv::Mat m10(10, 10, CV_32FC1);
    EXPECT_EQ("32FC1", dvrk::matTypeToString(m10));
    
    cv::Mat m11(10, 10, CV_64FC4);
    EXPECT_EQ("64FC4", dvrk::matTypeToString(m11));
    
    cv::Mat m12(10, 10, CV_64FC(5));
    EXPECT_EQ("64FC5", dvrk::matTypeToString(m12));
}
