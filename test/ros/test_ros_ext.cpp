//
// Created by nearlab on 28/09/17.
//

#include <gtest/gtest.h>

#include <dvrk_common/ros/ext.hpp>

TEST(TestAppend, TestAppend) {

    // Make sure that we can still call the "old" version
    EXPECT_EQ("base/leaf", ros::names::append("base", "leaf"));
    
    // Make sure that our new multi-arg versions work.
    EXPECT_EQ("/base/leaf/twig", ros::names::append("base", "leaf", "twig"));
    EXPECT_EQ("/this/is/a/long/topic/name", ros::names::append("this", "is", "a", "long", "topic", "name"));
}
