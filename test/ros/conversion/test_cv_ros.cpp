//
// Created by tibo on 19/07/17.
//

#include <iostream>
#include <gtest/gtest.h>

#include <dvrk_common/ros/ext.hpp>
#include <dvrk_common/ros/conversions/cv_ros.hpp>

using namespace dvrk;

TEST(TestROIConversions, ROIToRectConversions) {

    sensor_msgs::RegionOfInterest r1_roi;
    r1_roi.x_offset = 0;
    r1_roi.y_offset = 0;
    r1_roi.width = 100;
    r1_roi.height = 100;
    EXPECT_EQ(r1_roi, toROI(cv::Rect(0, 0, 100, 100)));

    sensor_msgs::RegionOfInterest r2_roi;
    r2_roi.x_offset = 0;
    r2_roi.y_offset = 0;
    r2_roi.width = 110;
    r2_roi.height = 220;
    EXPECT_EQ(r2_roi, toROI(cv::Rect2f(0.5f, 0.5f, 110.1f, 220.3f)));

    sensor_msgs::RegionOfInterest r3_roi;
    r3_roi.x_offset = 0;
    r3_roi.y_offset = 0;
    r3_roi.width = 220;
    r3_roi.height = 115;
    EXPECT_EQ(r3_roi, toROI(cv::Rect2d(0.75, 0.85, 220.9, 115.3)));
}

TEST(TestROIConversions, RectToROIConversions) {

    sensor_msgs::RegionOfInterest r1_roi;
    r1_roi.x_offset = 0;
    r1_roi.y_offset = 0;
    r1_roi.width = 100;
    r1_roi.height = 100;
    EXPECT_EQ(toRect(r1_roi), cv::Rect(0, 0, 100, 100));
}

TEST(TestPointConversion, RosToCV) {

    auto ros_p1 = geometry_msgs::make_point(1.2, 10.0, 14.5);
    auto ros_p2 = geometry_msgs::make_point(-1.2, 10.0, -14.5);

    EXPECT_EQ(toPoint(ros_p1), cv::Point(1, 10));
    EXPECT_EQ(toPoint(ros_p2), cv::Point(-1, 10));

    EXPECT_EQ(toPoint2i(ros_p1), cv::Point2i(1, 10));
    EXPECT_EQ(toPoint2i(ros_p2), cv::Point2i(-1, 10));

    EXPECT_EQ(toPoint2f(ros_p1), cv::Point2f(1.2f, 10.0f));
    EXPECT_EQ(toPoint2f(ros_p2), cv::Point2f(-1.2f, 10.0f));

    EXPECT_EQ(toPoint2d(ros_p1), cv::Point2d(1.2, 10.0));
    EXPECT_EQ(toPoint2d(ros_p2), cv::Point2d(-1.2, 10.0));


    EXPECT_EQ(toPoint3i(ros_p1), cv::Point3i(1, 10, 14));
    EXPECT_EQ(toPoint3i(ros_p2), cv::Point3i(-1, 10, -14));

    EXPECT_EQ(toPoint3f(ros_p1), cv::Point3f(1.2f, 10.0f, 14.5f));
    EXPECT_EQ(toPoint3f(ros_p2), cv::Point3f(-1.2f, 10.0f, -14.5f));

    EXPECT_EQ(toPoint3d(ros_p1), cv::Point3d(1.2, 10.0, 14.5));
    EXPECT_EQ(toPoint3d(ros_p2), cv::Point3d(-1.2, 10.0, -14.5));

}

TEST(TestPointConversion, CVToRos) {

    cv::Point2i cv_pt1(10, 14), cv_pt2(-11, 13);
    cv::Point2f cv_pt3(10.2f, 14.3f), cv_pt4(-11.4f, 13.8f);
    cv::Point2d cv_pt5(13.2, 9.3), cv_pt6(-1.4, 3.8);

    EXPECT_EQ(geometry_msgs::make_point(10, 14, 0.0), toPoint(cv_pt1));
    EXPECT_EQ(geometry_msgs::make_point32(10, 14, 0.0), toPoint32(cv_pt1));
    EXPECT_EQ(geometry_msgs::make_point(-11, 13, 0.0), toPoint(cv_pt2));
    EXPECT_EQ(geometry_msgs::make_point32(-11, 13, 0.0), toPoint32(cv_pt2));
    EXPECT_EQ(geometry_msgs::make_point(10.2, 14.3, 0.0), toPoint(cv_pt3));
    EXPECT_EQ(geometry_msgs::make_point32(10.2, 14.3, 0.0), toPoint32(cv_pt3));
    EXPECT_EQ(geometry_msgs::make_point(-11.4, 13.8, 0.0), toPoint(cv_pt4));
    EXPECT_EQ(geometry_msgs::make_point32(-11.4, 13.8, 0.0), toPoint32(cv_pt4));
    EXPECT_EQ(geometry_msgs::make_point(13.2, 9.3, 0.0), toPoint(cv_pt5));
    EXPECT_EQ(geometry_msgs::make_point32(13.2, 9.3, 0.0), toPoint32(cv_pt5));
    EXPECT_EQ(geometry_msgs::make_point(-1.4, 3.8, 0.0), toPoint(cv_pt6));
    EXPECT_EQ(geometry_msgs::make_point32(-1.4, 3.8, 0.0), toPoint32(cv_pt6));

}
