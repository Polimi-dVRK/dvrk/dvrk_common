cmake_minimum_required(VERSION 2.8.3)
project(dvrk_common)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
        roscpp
        nodelet

        image_geometry
        image_transport
        cv_bridge
        sensor_msgs
        )

# No need to specify compoents since we only used `optional` atm which is header only.
find_package(Boost REQUIRED COMPONENTS filesystem program_options)

find_package(OpenCV 3 REQUIRED)


###################################
## catkin specific configuration ##
###################################

catkin_package(
        INCLUDE_DIRS include
        LIBRARIES ${PROJECT_NAME} ${PROJECT_NAME}_nodelets
        CATKIN_DEPENDS roscpp nodelet image_geometry image_transport cv_bridge sensor_msgs
)

###########
## Build ##
###########

include_directories(include
        SYSTEM ${Boost_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS} ${catkin_INCLUDE_DIRS}
        )

set(SOURCES
        src/dvrk_common/system.cpp

        src/dvrk_common/opencv/parse.cpp
        src/dvrk_common/opencv/util.cpp
        src/dvrk_common/opencv/highgui.cpp
        src/dvrk_common/opencv/ui/slides-ui.cpp
        src/dvrk_common/opencv/image/specular_reflections_mask.cpp
        src/dvrk_common/opencv/image/modified_census_transform.cpp
        src/dvrk_common/opencv/image/hamming_distance_image.cpp

        src/dvrk_common/ros/ext.cpp
        src/dvrk_common/ros/util.cpp
        src/dvrk_common/ros/ros_graph_state.cpp
        src/dvrk_common/ros/advertisement_checker.cpp
        src/dvrk_common/ros/camera/simple_camera.cpp
        src/dvrk_common/ros/camera/stereo_camera.cpp
        src/dvrk_common/ros/camera/extended_stereo_camera.cpp
        )

add_library(${PROJECT_NAME} ${SOURCES})
target_link_libraries(${PROJECT_NAME} PUBLIC ${Boost_LIBRARIES} ${OpenCV_LIBRARIES} ${catkin_LIBRARIES})

# Build nodelets
set(NODELET_SOURCES
        src/nodelets/modified_census_transform_nodelet.cpp
        src/nodelets/hamming_distance_nodelet.cpp
        src/nodelets/image_viewer_nodelet.cpp
        src/nodelets/stereo_viewer_nodelet.cpp
        )

add_library(${PROJECT_NAME}_nodelets ${NODELET_SOURCES})
target_link_libraries(${PROJECT_NAME}_nodelets ${PROJECT_NAME})

# Build dvrk-image-tool
include(samples/CMakeLists.txt)

add_executable(ros-dump src/nodes/rosdump.cpp)
target_link_libraries(ros-dump ${PROJECT_NAME} ${catkin_LIBRARIES})

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executables and/or libraries for installation
install(TARGETS ${PROJECT_NAME} ${PROJECT_NAME}_nodelets
        ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
        )

# Mark cpp header files for installation
install(DIRECTORY include/${PROJECT_NAME}/
        DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
        FILES_MATCHING PATTERN "*.hpp"
        )

install(FILES nodelet_plugins.xml
        DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
        )

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
catkin_add_gtest(${PROJECT_NAME}-test
        test/test.cpp
        test/opencv/test_parse.cpp
        test/opencv/test_opencv_util.cpp
        test/opencv/image/test_modified_census_transform.cpp
        test/opencv/image/test_hamming_distance.cpp

        test/ros/conversion/test_cv_ros.cpp
        test/ros/test_ros_ext.cpp
        )

if (TARGET ${PROJECT_NAME}-test)
    target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
endif ()

# Dummy target for CLion
FILE (GLOB_RECURSE clion_all_headers
        ${CMAKE_SOURCE_DIR}/**/*.hpp
        ${CMAKE_SOURCE_DIR}/**/*.h)
ADD_CUSTOM_TARGET(all_clion SOURCES ${clion_all_headers})
