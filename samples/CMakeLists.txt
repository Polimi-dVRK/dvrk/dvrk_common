# Compile and install

add_executable(dvrk-image-tool samples/image_tool.cpp)
target_link_libraries(dvrk-image-tool ${PROJECT_NAME})

add_executable(stereo_viewer samples/stereo_viewer.cpp)
target_link_libraries(stereo_viewer ${PROJECT_NAME})

add_executable(image_viewer samples/image_viewer.cpp)
target_link_libraries(image_viewer ${PROJECT_NAME})

## Mark executables and/or libraries for installation
install(TARGETS dvrk-image-tool stereo_viewer
        ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
        )