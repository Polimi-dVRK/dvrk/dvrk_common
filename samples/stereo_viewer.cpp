//
// Created by tibo on 04/09/17.
//

#include <ros/ros.h>
#include <nodelet/loader.h>

int main(int argc, char** argv) {
    
    ros::init(argc, argv, "stereo_viwer", ros::init_options::AnonymousName);
    ros::NodeHandle node;

    nodelet::Loader nodelet(false); // Don't bring up the manager ROS API
    nodelet::M_string remap(ros::names::getRemappings());
    nodelet::V_string nodelet_argv;
    
    nodelet.load("stereo_viewer_nodelet", "dvrk_common/stereo_viewer", remap, nodelet_argv);
    
    ros::Rate loop_rate(100);
    while (ros::ok()) {
        ros::spinOnce();
        loop_rate.sleep();
    }
    
    return 0;
}
