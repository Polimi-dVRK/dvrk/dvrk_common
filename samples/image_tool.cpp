/**
 * @file
 *
 * The dvrk-image-tool is a one-stop shop to preview the results of the various filters in this repository.
 */

#include <iostream>
#include <algorithm>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <boost/range/iterator_range.hpp>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include "dvrk_common/error_base.hpp"
#include "dvrk_common/opencv/parse.hpp"
#include "dvrk_common/opencv/ui/slides-ui.hpp"
#include "dvrk_common/opencv/image/specular_reflections_mask.hpp"
#include "dvrk_common/opencv/image/modified_census_transform.hpp"

/// @todo: Proper USAGE string

const std::vector<std::string> image_extensions{".jpg", ".png", ".ppm"};

enum ExitCodes {
    SUCCESS = 0,
    
    CMDLINE_ERROR = -1,
    INPUT_NOT_A_FILE_OR_DIRECTORY_ERROR = -2,
    UNABLE_TO_CREATE_OUTPUT_DIR_ERROR = -3,
    NOT_AN_IMAGE_FILE = -4,
    INVALID_FILTER_NAME = -5,
    UNABLE_TO_OPEN_IMAGE_FILE_ERROR = -6,
    UNKNOWN_ERROR = -7,
};


bool is_image_file(fs::path filename) {
    auto ext = filename.extension();
    return (std::find(image_extensions.begin(), image_extensions.end(), ext) != image_extensions.end());
}


void validate(boost::any& v,
    const std::vector<std::string>& values,
    cv::Size* target_type, int)
{
    // Make sure no previous assignment to 'a' was made.
    po::validators::check_first_occurrence(v);
    // Extract the first string from 'values'. If there is more than
    // one string, it's an error, and exception will be thrown.
    const std::string& param = po::validators::get_single_string(values);
    
    auto result = dvrk::parseSize(param);
    if (result) {
        v = boost::any(*result);
    } else {
        throw po::validation_error(po::validation_error::invalid_option_value);
    }
}


void process_file(const fs::path &input_file, const fs::path &output_dir, const po::variables_map& cmdline)
{
    if (!is_image_file(input_file)) {
        std::cerr << "Error: \"" << input_file.string() << "\" is not an image file" << std::endl;
        exit(NOT_AN_IMAGE_FILE);
    }
    
    cv::Mat source_image = cv::imread(input_file.string());
    if (source_image.empty()) {
        std::cerr << "Unable to open file " << input_file.string() << "\n";
        exit(UNABLE_TO_OPEN_IMAGE_FILE_ERROR);
    }
    
    const std::string& operation = cmdline["filter"].as<std::string>();
    if (operation == "specular-identification") {
        std::cout << "Applying specular identifcation filter to " << fs::basename(input_file) << " ...\n";
        cv::Mat processed_img = dvrk::create_specular_reflections_mask(source_image);
    
        // The specular reflections mask is a single channel image. In order to show it usefully we consider it as the
        // B channel of a new image that we overlay on top of the original image.
        cv::Mat rgb_result, empty_channel = cv::Mat::zeros(processed_img.size(), processed_img.type());
    
        std::vector<cv::Mat> channels{processed_img, empty_channel, empty_channel};
        cv::merge(channels, rgb_result);
        cv::addWeighted(source_image, 0.75, rgb_result, 0.25, 0.0, rgb_result);
    
        namespace ui = dvrk::ui;
        auto title = (new ui::Text("Specular Removal Filter"))
            ->setAlignment(ui::Alignment::Centre)
            ->setScale(ui::Text::Size::ExtraBig);
    
        auto image_grid = (new ui::ImageGrid())
            ->addImage(source_image, std::string("Original (") + fs::basename(input_file) + ")")
            ->addImage(rgb_result, std::string("Result"));
    
        ui::Window window("Specular Reflection Filter");
        window.addChild(title);
        window.addChild(image_grid);
        window.render();
    
        const auto output_file = output_dir / input_file.filename();
        std::cout << "    Saving results to " << output_file << " ...\n";
        window.save(output_file.string());
    
    } else if (operation == "modified-census-transform" || operation == "mct") {
        std::cout << "Applying modified census transform to " << fs::basename(input_file) << " ...\n";
        
        cv::Mat gray_source;
        cv::cvtColor(source_image, gray_source, cv::COLOR_RGB2GRAY);
        
        // In order to export the results of the Modified Census Transform as an image we MUST use a 3x3 kernel.
        // Anything larger would require the use of a datatype larger than an a CV_16UC1 which means that we would't be
        // able to save the result as an image
        auto mct = dvrk::ModifiedCensusTransform({3, 3});
        cv::Mat result = mct.apply(gray_source);
        
        namespace ui = dvrk::ui;
        auto title = (new ui::Text("Modified Census Transform"))
            ->setAlignment(ui::Alignment::Centre)
            ->setScale(ui::Text::Size::ExtraBig);
    
        auto image_grid = (new ui::ImageGrid())
            ->addImage(source_image, std::string("Original (") + fs::basename(input_file) + ")")
            ->addImage(result, std::string("Result"));
    
        ui::Window window("Modified Census Transform");
        window.addChild(title);
        window.addChild(image_grid);
        window.render();
    
        const auto output_file = output_dir / input_file.filename();
        std::cout << "    Saving results to " << output_file << " ...\n";
        window.save(output_file.string());
        
    } else {
        std::cerr << "\"" << operation << "\" is not a recognised filter" << std::endl;
        exit(INVALID_FILTER_NAME);
    }
}


void process_directory(const fs::path &input_dir, const fs::path &output_dir, const po::variables_map& cmdline) {
    for (const fs::path& file: boost::make_iterator_range(fs::directory_iterator(input_dir), {})) {
        if (fs::is_regular_file(file)) {
            process_file(file, output_dir, cmdline);
        }
    }
}


int main(int argc, char** argv) {
    
    std::vector< std::string > input_files;
    
    po::positional_options_description positional_opts;
    positional_opts.add("input", -1);
    
    po::options_description main_opts("Options");
    main_opts.add_options()
        ("help", "Show this help")
        (
            "input,i",
            po::value< std::vector<std::string> >(&input_files)->required(),
            "The file or folder containing the images(s) to process"
        )
        (
            "output-directory,o",
            po::value<std::string>()->required(),
            "The file or folder into which the processed image(s) will be saved"
        )
        (
            "filter,f",
            po::value<std::string>()->required(),
            "The operation to perform on the image"
        );
        
    po::variables_map cmdline;
    try {
        auto parsed_opts = po::command_line_parser(argc, argv)
            .options(main_opts)
            .positional(positional_opts)
            .run();
    
        po::store(parsed_opts, cmdline);
        if (cmdline.count("help")) {
            std::cout << main_opts << std::endl;
            exit(CMDLINE_ERROR);
        }
        
        po::notify(cmdline);
    } catch (po::required_option& ex) {
        std::cerr << "Error: rerquired option \"" << ex.get_option_name() << "\" missing from command line arguments"
                  << std::endl;
        exit(CMDLINE_ERROR);
    } catch (po::unknown_option &ex) {
        std::cerr << "Error: unknown option \"" << ex.get_option_name() << "\"" << std::endl << main_opts << std::endl;
        exit(CMDLINE_ERROR);
    }

    fs::path output_dir = fs::absolute(cmdline["output-directory"].as<std::string>());
    if (fs::exists(output_dir) && !fs::is_directory(output_dir)) {
        std::cerr << "Error: unable to create output directory \"" << output_dir.string() << "\"" << std::endl;
        exit(UNABLE_TO_CREATE_OUTPUT_DIR_ERROR);
    }
    
    // TODO: check that the directory could be created
    if (!fs::exists(output_dir)) {
        fs::create_directories(output_dir);
    }
    
    try {
        const auto operation = cmdline["filter"].as<std::string>();
        for (const auto input: input_files) {
            fs::path input_path = fs::absolute(input);
            if (fs::is_regular_file(input_path)) {
                process_file(input_path, output_dir, cmdline);
            }
            else if (fs::is_directory(input_path)) {
                process_directory(input_path, output_dir, cmdline);
            }
            else {
                std::cerr << "Error: \"" << fs::absolute(input) << "\" is not a file or a directory." << std::endl;
                exit(INPUT_NOT_A_FILE_OR_DIRECTORY_ERROR);
            }
        }
    } catch (const boost::exception& ex) {
        std::cerr << "Ooops! An unexpected error occurred. \n\n" << boost::diagnostic_information(ex, true) << std::endl;
        exit(UNKNOWN_ERROR);
    }
    
    return SUCCESS;
}
