//
// Created by nearlab on 26/10/17.
//

#pragma once

#include <string>
using namespace std::string_literals;

#include <chrono>
#include <mutex>
#include <vector>
#include <map>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
using namespace boost::accumulators;

#define CONCAT3(A, B, C) A##B
#define ProfileThis(scope_name) \
    dvrk::SimpleProf CONCAT3(profile_, __LINE__, (scope_name))

namespace dvrk {

class SimpleProfMaster {

public: /* Static Methods */
    static SimpleProfMaster& instance() {
        static SimpleProfMaster instance;
        return instance;
    }

public: /* Methods */
    
    ~SimpleProfMaster() {
        print_summary();
    }
    
    void add_record(const std::string& scope_name, const long long duration) {
        std::lock_guard<std::mutex> lock_records(_record_guard);
        
        if (!_records.count(scope_name)) {
            _records[scope_name] = timing_stats();
        }
        
        _records[scope_name](duration);
    }
    
private: /* Methods */
    
    void print_summary() {
        auto duration = std::chrono::high_resolution_clock::now() - _start_time;
        const auto total_seconds = std::chrono::duration_cast<std::chrono::seconds>(duration);
        
        std::cout << "\nTotal duration: ";
        const auto hours = std::chrono::duration_cast<std::chrono::hours>(duration);
        if (hours.count() > 0) {
            std::cout << hours.count() << " hours, ";
            duration = duration - hours;
        }
    
        const auto minutes = std::chrono::duration_cast<std::chrono::minutes>(duration);
        if (minutes.count() > 0) {
            std::cout << minutes.count() << " minutes, ";
            duration = duration - minutes;
        }
    
        const auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration);
        if (seconds.count() > 0) {
            std::cout << seconds.count() << " seconds";
            duration = duration - seconds;
        }
        
        std::cout << "\n\n";
        
        std::cout << std::setw(40) << "Scope" << " | "
                  << std::setw(10) << "Calls" << " | "
                  << std::setw(10) << "Freq. (Hz)" << " | "
                  << std::setw(10) << "Mean (ms)" << " | "
                  << std::setw(10) << "Std. Dev" << " | "
                  << std::setw(10) << "Min (ms)" << " | "
                  << std::setw(10) << "Max (ms)\n";
    
        std::cout << std::setw(40) << std::setfill('-') << "" << "-+-"
                  << std::setw(10) << "" << "-+-"
                  << std::setw(10) << "" << "-+-"
                  << std::setw(10) << "" << "-+-"
                  << std::setw(10) << "" << "-+-"
                  << std::setw(10) << "" << "-+-"
                  << std::setw(10) << "" << "\n" << std::setfill(' ');
        // TODO: Horiz separator
    
        std::string scope_name;
        timing_stats timings;
        for (const auto kv : _records) {
            std::tie(scope_name, timings) = kv;
        
            std::cout << std::setw(40) << scope_name << " | "
                      << std::setw(10) << count(timings) << " | "
                      << std::setw(10) << double{count(timings)} / total_seconds.count() << " | "
                      << std::setw(10) << mean(timings) / 1e6 << " | "
                      << std::setw(10) << sqrt(variance(timings))  << " | "
                      << std::setw(10) << min(timings) / 1e6 << " | "
                      << std::setw(10) << max(timings) / 1e6 << "\n";
        }
    }
    
    
private: /* Members */
    
    SimpleProfMaster() = default;
    
    std::mutex _record_guard;
    
    using timing_stats = accumulator_set<double, stats<tag::count, tag::mean, tag::variance, tag::min, tag::max>>;
    std::map<std::string, timing_stats> _records;
    
    std::chrono::high_resolution_clock::time_point _start_time = std::chrono::high_resolution_clock::now();
};


class SimpleProf {
public: /* Methods */
    SimpleProf(const std::string& scope_name)
        : _scope_name(scope_name),
          _start_time(std::chrono::high_resolution_clock::now())
    {
        // Do Nothing
    };
    
    ~SimpleProf() {
        stop();
    }
    
    void stop() {
        if (_finished) return;
        
        const auto duration = std::chrono::high_resolution_clock::now() - _start_time;
        const auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(duration);
        SimpleProfMaster::instance().add_record(_scope_name, nanoseconds.count());
        
        _finished = true;
    }
    
private: /* Members */
    
    const std::string _scope_name;
    bool _finished = false;
    
    const std::chrono::high_resolution_clock::time_point _start_time;
    
};

}
