//
// Created by tibo on 14/07/17.
//

#pragma once

#include <cassert>
#include <cmath>
#include <complex>
#include <vector>
#include <algorithm>

namespace dvrk {

// Note: The clamp functions will become redundant once C++17 is supported on all compilers in use at the lab.

/**
 * Clamp the value \p v to the range \p lo \p hi using the comparision function \p comp
 *
 * @tparam T The type of values to operate on.
 * @tparam Compare
 *  A callable object that implements a comparison between two quantities, e.g. \c std::less<>
 *
 * @param v The value to clamp
 * @param lo The bottom of the range to clamp the value to
 * @param hi The top of the range to clamp the value to. A precondition is that \c lo < \c hi
 * @param comp The comparision function to use.
 *
 * @return
 *  The value of \p v clamped to the range \p lo; \p hi. Whether this range is inclusive or exclusive depends on the
 *  specified comparator.
 */
template<class T, class Compare>
constexpr const T& clamp( const T& v, const T& lo, const T& hi, Compare comp )
{
    return assert( !comp(hi, lo) ),
        comp(v, lo) ? lo : comp(hi, v) ? hi : v;
}
/**
 * Clamp the value \p v to the exclusive range \p lo \p hi. The comparision is performed using the \c std::less<T>
 * function of the standard library.
 *
 * @tparam T The type of values to operate on.
 *
 * @param v The value to clamp
 * @param lo The bottom of the range to clamp the value to
 * @param hi The top of the range to clamp the value to. A precondition is that \c lo < \c hi
 *
 * @return
 *  The value of \p v clamped to the range ] \p lo; \p hi [.
 */
template<class T>
constexpr const T& clamp( const T& v, const T& lo, const T& hi )
{
    return clamp<T>( v, lo, hi, std::less<>() );
}

/**
 * Floating point number comparison.
 *
 * To comapre two floating point numbers we must take into account the fact that not all numbers can be represented as
 * either floats or doubles. This means that seemingly innocuous looking values can cause issues. To workaround this
 * problem we check that the two numbers are close.
 *
 * THe problem is deciding how close is close enough. If we compare small numbers we want the to fit within an epsilon
 * that is small however if we compare larger numbers this epsilon mugh tbe bigger than the distance between two
 * adjacent floats. The workaround is to scale this epsilon by the largest oprand of the comparison.
 *
 * The function implements this approach. It is not perfect but "better" alternatives are significatly more complicated
 * and cannot be generalised and need to be re-implemented for each floating point number type.
 *
 * @note The code for this function is taken from realtimecollisiondetection.net/blog/?p=89.
 *
 * @tparam T The type of the floating point numbers to compare
 * @param lhs The first operand
 * @param rhs The second operand
 * @param epsilon
 *  The tolerance factor to use in deciding whther two numbers are equal or not. THe default vlaue (10e-6) makes senses
 *  in the context of the `dvrk` system where numbers will never be very small or very large. It might not makes sense
 *  in yours.
 * @return True if the two operands are approximately equal, false otherwise
 */
template <typename T>
bool approx_equal(const T& lhs, const T&  rhs, const float epsilon = 10e-6) {
    return (
      std::fabs(lhs - rhs) <= epsilon * std::max({ T(1.0), std::fabs(lhs), std::fabs(rhs) })
    );
}


template <typename T>
T mean(std::vector<T> vec) {
    T accumulator = static_cast<T>(0);
    
    if (vec.empty())
        return accumulator;
        
    std::for_each(vec.begin(), vec.end(), [&accumulator](const auto el) { accumulator += el; });
    
    return accumulator / vec.size();
}

}
