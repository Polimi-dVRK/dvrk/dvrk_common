//
// Created by tibo on 21/07/17.
//


#pragma once

#include <memory>

#include <ros/node_handle.h>
#include <image_transport/image_transport.h>
#include <image_transport/subscriber_filter.h>
#include <image_geometry/stereo_camera_model.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include "dvrk_common/ros/tools/advertisement_checker.hpp"

namespace dvrk
{

struct StereoImage
{
    const sensor_msgs::Image& left; /// The image from the left camera
    const sensor_msgs::Image& right; /// The image from the right camera
};

/**
 * This class provides an abstraction similar to that provided by the #SimpleCamera class but for stereo cameras. All
 * the extra complexity comes from having to manually synchronise the image topics.
 */
class StereoCamera
{
public: /* Type Definitions */
    
    using image_cb_t = std::function<void(
        const sensor_msgs::ImageConstPtr& left,
        const sensor_msgs::ImageConstPtr& right,
        const image_geometry::StereoCameraModel& camera_model
    )>;
    
    template <class Parent>
    using image_cb_method_t = void (Parent::*)(
        const sensor_msgs::ImageConstPtr& left,
        const sensor_msgs::ImageConstPtr& right,
        const image_geometry::StereoCameraModel& camera_model
    );
    
public:
    StereoCamera(ros::NodeHandle nh);
    StereoCamera(
        ros::NodeHandle nh,
        const std::string &left_camera_topic,
        const std::string &right_camera_topic
    );
    
    StereoCamera(
        ros::NodeHandle nh,
        const std::string &left_camera_topic,
        const std::string &right_camera_topic,
        image_cb_t cb
    );
    
    template <typename T>
    StereoCamera(
        ros::NodeHandle nh,
        const std::string &left_camera_topic,
        const std::string &right_camera_topic,
        image_cb_method_t <T> cb,
        T *owner
    );
    
    void subscribe(
        const std::string &left_camera_topic,
        const std::string &right_camera_topic,
        const image_transport::TransportHints &hints = image_transport::TransportHints()
    );
    
    void subscribe(
        const std::string &left_camera_topic,
        const std::string &right_camera_topic,
        image_cb_t cb
    );
    
    void subscribe(
        const std::string &left_camera_topic,
        const std::string &right_camera_topic,
        const image_transport::TransportHints &hints,
        image_cb_t cb
    );
    
    template <class T>
    void subscribe(
        const std::string &left_camera_topic,
        const std::string &right_camera_topic,
        image_cb_method_t <T> cb,
        T *owner
    );
    
    template <class T>
    void subscribe(
        const std::string &left_camera_topic,
        const std::string &right_camera_topic,
        const image_transport::TransportHints &hints,
        image_cb_method_t <T> cb,
        T *owner
    );
    
    void unsubscribe();

    bool waitForFirstImage(float timeout = -1.0f, unsigned int freq = 100);
    
    /**
     * Set the callback function called each time an image is received to the specified function object.
     *
     * @param cb The callback function that will be called each time an image is received.
     */
    void setOnImageCallback(image_cb_t cb) { _on_image_cb = std::move(cb); }
    
    template <class T>
    void setOnImageCallback(image_cb_method_t<T> cb, T *owner);
    
    void clearOnImageCallback() { _on_image_cb = {}; }
    
    void setMaxImageInterval(const ros::Duration& max_interval);

    const StereoImage getLatestImage();
    
    const sensor_msgs::Image& getLeftImage();
    
    const sensor_msgs::Image& getRightImage();

    const sensor_msgs::CameraInfo& getLeftCameraInfo();

    const sensor_msgs::CameraInfo& getRightCameraInfo();

    const image_geometry::StereoCameraModel getCameraModel();
    
    /// Get the name of the topic from which the left camera images are being read
    std::string getLeftImageTopic() const { return _left_image_sub.getTopic(); }
    
    /// Get the name of the topic from which the left camera info messages are being read
    std::string getLeftInfoTopic() const { return _left_image_camera_info_sub.getTopic(); }
    
    /// Get the name of the topic from which the right camera images are being read
    std::string getRightImageTopic() const { return _right_image_sub.getTopic(); }
    
    /// Get the name of the topic from which the right camera info messages are being read
    std::string getRightInfoTopic() const { return _right_image_camera_info_sub.getTopic(); }
    
    bool is_subscribed() { return _subscribed; }
    
    explicit operator bool() { return is_subscribed(); }

public: /* Callbacks */

    virtual void onStereoImagesReceived(
        const sensor_msgs::ImageConstPtr &left,
        const sensor_msgs::CameraInfoConstPtr &left_camera_info,
        const sensor_msgs::ImageConstPtr &right,
        const sensor_msgs::CameraInfoConstPtr &right_camera_info
    );
    
private:

    // ROS topics & Plumbing
    
    ros::NodeHandle _nh;
    image_transport::ImageTransport _it;

    /// Subscriber for the left image topic.
    image_transport::SubscriberFilter _left_image_sub;
    /// Subscriber for the left \c camera_info topic
    message_filters::Subscriber<sensor_msgs::CameraInfo> _left_image_camera_info_sub;

    /// Subscriber for the right image topic
    image_transport::SubscriberFilter _right_image_sub;
    /// Subscriber for the right \c camera_info_topic
    message_filters::Subscriber<sensor_msgs::CameraInfo> _right_image_camera_info_sub;

    /// Overload the \c ApproximateTimeFilter to handle 2 images & 2 camera_info topics.
    typedef message_filters::sync_policies::ApproximateTime<
        sensor_msgs::Image, sensor_msgs::CameraInfo, sensor_msgs::Image, sensor_msgs::CameraInfo
    > ApproximateTimePolicy;

    /**
     * The synchroniser is responsible for pairing up L/R images and L/R camera_infos.
     *
     * To use it we create subscribers to all the relevant topics, in this case the two image topics and the two camera
     * info topics and connect them to the _synchroniser with the \c connectInput function. The type of the function is
     * given by the specialisation of the \c message_filters::sync_policies we just created.
     *
     * When messages are received from all the connected topics within the allotted time-frame the callback registered
     * onto the synchroniser is called. This is when we see the data for the first time.
     */
    message_filters::Synchronizer<ApproximateTimePolicy> _synchroniser;

    AdvertisementChecker _adv_checker;
    
    // State variables
    
    sensor_msgs::Image _left_image;
    sensor_msgs::Image _right_image;
    image_geometry::StereoCameraModel _camera_model;
    
    /// The number of complete stereo images received so far
    uint32_t _stereo_image_count = 0;
    
    bool _subscribed = false;
    
    image_cb_t _on_image_cb;
};


template<typename T>
StereoCamera::StereoCamera(
    ros::NodeHandle nh,
    const std::string& left_camera_topic,
    const std::string& right_camera_topic,
    StereoCamera::image_cb_method_t<T> cb,
    T *owner
)
    : StereoCamera(nh)
{
    subscribe(left_camera_topic, right_camera_topic);
    setOnImageCallback(std::move(cb), owner);
}


template<class T>
void StereoCamera::subscribe(
    const std::string &left_camera_topic,
    const std::string &right_camera_topic,
    image_cb_method_t <T> cb,
    T *owner
) {
    subscribe(left_camera_topic, right_camera_topic);
    setOnImageCallback(std::move(cb), owner);
}

template<class T>
void StereoCamera::subscribe(
    const std::string &left_camera_topic,
    const std::string &right_camera_topic,
    const image_transport::TransportHints &hints,
    image_cb_method_t <T> cb,
    T *owner
) {
    subscribe(left_camera_topic, right_camera_topic, hints);
    setOnImageCallback(std::move(cb), owner);
}

/**
 * Set the callback function called each time a new image is received to the specified member function \p cb of the
 * instance of \p T passed in as \p owner.
 *
 * @tparam T The type of the class to which the specified callback belongs.
 * @param cb The callback function to call each time an image is received.
 * @param owner The class instance on which to call the callback.
 */
template <class T>
void StereoCamera::setOnImageCallback(image_cb_method_t<T> cb, T* owner)
{
    // Whilst using std::bind seems more natural apparently lambdas are more idiomatic
    _on_image_cb = [cb, owner](
        const sensor_msgs::ImageConstPtr& left,
        const sensor_msgs::ImageConstPtr& right,
        const image_geometry::StereoCameraModel& camera_model
    ) {
        assert(owner != nullptr && cb != nullptr);
        return (owner->*cb)(left, right, camera_model);
    };
}

}
