//
// Created by tibo on 07/07/17.
//

#pragma once

#include <string>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <image_geometry/pinhole_camera_model.h>

#include <dvrk_common/ros/tools/advertisement_checker.hpp>

namespace dvrk
{


/**
 * A simple camera class to expose a ROS \c image_transport compatible \c sensor_msgs::Image topic.
 *
 * It abstracts away the functioning of the topics and provides a simple interface for common operations. Specifically
 * the SimpleCamera::waitForFirstImage() function makes it easy to ensure that a topic is publishing since it will
 * produce an error if it times out and the SimpleCamera::getLatestImage() function will always return an image. If no
 * Images have been received it will throw an exception.
 *
 * These two functions alone have saved me a significant amount of time.
 */
class SimpleCamera
{
public: /* Type Definitions */

    using image_cb_t = std::function<void(
        const sensor_msgs::ImageConstPtr& image,
        const image_geometry::PinholeCameraModel& camera_model
    )>;
    
    template <class Parent>
    using image_cb_method_t = void (Parent::*)(
        const sensor_msgs::ImageConstPtr& image,
        const image_geometry::PinholeCameraModel& camera_model
    );
    
public:
    explicit SimpleCamera(const ros::NodeHandle &node);
    
    SimpleCamera(
        const ros::NodeHandle &node,
        const std::string &topic
    );
    
    SimpleCamera(
        const ros::NodeHandle &node,
        const std::string &topic,
        image_cb_t cb
    );

    template <class Parent>
    SimpleCamera(
        const ros::NodeHandle &node,
        const std::string &topic,
        image_cb_method_t<Parent> cb,
        Parent* owner
    );
    
    void subscribe(
        const std::string &topic,
        const image_transport::TransportHints &hints = image_transport::TransportHints()
    );
    
    void subscribe(
        const std::string &topic,
        image_cb_t cb
    );
    
    void subscribe(
        const std::string &topic,
        const image_transport::TransportHints &hints,
        image_cb_t cb
    );
    
    template <class Parent>
    void subscribe(
        const std::string &topic,
        image_cb_method_t<Parent> cb,
        Parent* owner
    );
    
    template <class Parent>
    void subscribe(
        const std::string &topic,
        const image_transport::TransportHints &hints,
        image_cb_method_t<Parent> cb,
        Parent* owner
    );
    
    void unsubscribe();
    
    bool waitForFirstImage(float timeout = -1.0f, unsigned int freq = 100);
    
    /**
     * Set the callback function called each time an image is received to the specified function object.
     *
     * @param cb The callback function that will be called each time an image is received.
     */
    void setOnImageCallback(image_cb_t cb) { _on_image_cb = std::move(cb); }
    
    template <class T>
    void setOnImageCallback(image_cb_method_t<T> cb, T *owner);

    void clearOnImageCallback() { _on_image_cb = nullptr; }
    
    bool is_subscribed() { return _camera != nullptr; };
    
    explicit operator bool() { return is_subscribed(); }

    const sensor_msgs::Image &getLatestImage() const;
    
    u_int getImageCount() const;

    /// Get the name of the underlying raw image topic
    std::string getTopic() const { return _camera.getTopic(); }

    /// Get the name of the underlying \c CameraInfo topic
    std::string getInfoTopic() const { return _camera.getInfoTopic(); }

    // Get the name of the actual transport used for the images (e.g. compressed, theora, ...)
    std::string getTransport() const { return _camera.getTransport(); }
    
    const image_geometry::PinholeCameraModel& getCameraModel() const { return _camera_model; }
    
    uint32_t  getNumPublishers() const { return _camera.getNumPublishers(); }

private: /* callbacks */

    void OnImageReceived(
        const sensor_msgs::ImageConstPtr &img,
        const sensor_msgs::CameraInfoConstPtr &camera_info
    );

protected:
    
    /* ROS Stuff */
    
    ros::NodeHandle _node;
    image_transport::ImageTransport _it;
    image_transport::CameraSubscriber _camera;
    
    AdvertisementChecker _adv_checker;
    
    /* State Variables */
    
    /// Number of images received since the node has started
    u_int _image_count = 0;

    /// The latest image that the node has received
    sensor_msgs::Image _latest_image;

    ///
    image_geometry::PinholeCameraModel _camera_model;
    
    /// Callback called when a new image is received
    image_cb_t _on_image_cb;
};

/**
 * Set the callback function called each time a new image is received to the specified member function \p cb of the
 * instance of \p T passed in as \p owner.
 *
 * @tparam T The type of the class to which the specified callback belongs.
 * @param cb The callback function to call each time an image is received.
 * @param owner The class instance on which to call the callback.
 */
template <class T>
void SimpleCamera::setOnImageCallback(image_cb_method_t<T> cb, T *owner) {
    // Whilst using std::bind seems more natural apparently lambdas are more idiomatic
    _on_image_cb = [cb, owner](
        const sensor_msgs::ImageConstPtr& image,
        const image_geometry::PinholeCameraModel& camera_model
    ) {
        assert(owner != nullptr && cb != nullptr);
        return (owner->*cb)(image, camera_model);
    };
}


/**
 * Constructor
 *
 * @param node The ROS node used to initialise the \c image_transport::ImageTransport.
 * @param topic The topic on which to listen for images.
 * @param cb The callback function to call each time a new image is received.
 */
template <class Parent>
SimpleCamera::SimpleCamera(
    const ros::NodeHandle &node,
    const std::string &topic,
    SimpleCamera::image_cb_method_t<Parent> cb,
    Parent* owner
)
    : SimpleCamera(node)
{
       subscribe(topic, std::move(cb), owner);
}


template <class Parent>
void SimpleCamera::subscribe(const std::string &topic, image_cb_method_t<Parent> cb, Parent* owner)
{
    subscribe(topic);
    setOnImageCallback(std::move(cb), owner);
}

template <class Parent>
void SimpleCamera::subscribe(
    const std::string &topic,
    const image_transport::TransportHints &hints,
    image_cb_method_t<Parent> cb,
    Parent* owner
) {
    subscribe(topic, hints);
    setOnImageCallback(std::move(cb), owner);
    
}

}


