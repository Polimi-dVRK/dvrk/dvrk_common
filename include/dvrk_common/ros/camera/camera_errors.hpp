//
// Created by tibo on 21/07/17.
//

#pragma once

#include <opencv2/core/types.hpp>

#include "dvrk_common/error_base.hpp"

namespace dvrk
{

namespace error
{

    /**
    * @ brief
    *  Thrown when no images are received within a specified timeframe in, for example, @see
    *  SimpleCamera::WaitForFirstImage, @see StereoCamera::WaitForFirstImage.
    */
    struct NoImageReceivedError: public ErrorBase
    {
    };

    /// Context for #NoImageReceivedError. Used to provide the camera topic to the exception context
    typedef boost::error_info<struct tag_camera_topic, const std::string> camera_topic;
    
    struct EmptyImageError: public ErrorBase
    {
    };

    /// Used when the size of an image is different from the expected image size
    struct ImageSizeMismatch: public ErrorBase
    {
    };
    
    typedef boost::error_info<struct tag_expected_image_size, cv::Size> expected_image_size;
    
    typedef boost::error_info<struct tag_actual_image_size, cv::Size> actual_image_size;
    
} // end namespace dvrk::error

} // end namespace dvrk
