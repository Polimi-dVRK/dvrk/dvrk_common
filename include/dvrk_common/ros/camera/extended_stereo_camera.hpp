//
// Created by nearlab on 05/10/17.
//


#pragma once

#include <ros/node_handle.h>
#include <image_transport/image_transport.h>
#include <image_transport/subscriber_filter.h>
#include <image_geometry/stereo_camera_model.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include "dvrk_common/ros/tools/advertisement_checker.hpp"

namespace dvrk
{
    
    struct ExtendedStereoImage
    {
        const sensor_msgs::Image& left_mono; /// The image from the left camera in grayscale
        const sensor_msgs::Image& right_mono; /// The image from the right camera in grayscale
        const sensor_msgs::Image& left_rgb; /// The image from the left camera in colour
        const sensor_msgs::Image& right_rgb; /// The image from the right camera in colour
    };
    
    /**
     *
     * @note
     *  The mono and colour topics subscribed to by the camera should come from the same camera. The
     *  camera subscribes to the \c camera_info topic of the mono images and if the color images
     *  have a different \c camera_info then the geometry information obtained from the camera
     *  models will be wrong for the color images.
     */
    class ExtendedStereoCamera
    {
    public: /* Type Definitions */

        using image_cb_t = std::function<void(
            const sensor_msgs::ImageConstPtr &left_mono,
            const sensor_msgs::ImageConstPtr &right_mono,
            const sensor_msgs::ImageConstPtr &left_rgb,
            const sensor_msgs::ImageConstPtr &right_rgb,
            const image_geometry::StereoCameraModel &camera_model
        )>;
        
        template<class Parent>
        using image_cb_method_t = void (Parent::*)(
            const sensor_msgs::ImageConstPtr &left_mono,
            const sensor_msgs::ImageConstPtr &right_mono,
            const sensor_msgs::ImageConstPtr &left_rgb,
            const sensor_msgs::ImageConstPtr &right_rgb,
            const image_geometry::StereoCameraModel &camera_model
        );
        
        
    public:
        ExtendedStereoCamera(ros::NodeHandle nh);
        
        ExtendedStereoCamera(
            ros::NodeHandle nh,
            const std::string &left_mono_topic,
            const std::string &right_mono_topic,
            const std::string &left_rgb_topic,
            const std::string &right_rgb_topic
        );
    
        ExtendedStereoCamera(
            ros::NodeHandle nh,
            const std::string &left_mono_topic,
            const std::string &right_mono_topic,
            const std::string &left_rgb_topic,
            const std::string &right_rgb_topic,
            image_cb_t cb
        );
    
        template <typename T>
        ExtendedStereoCamera(
            ros::NodeHandle nh,
            const std::string &left_mono_topic,
            const std::string &right_mono_topic,
            const std::string &left_rgb_topic,
            const std::string &right_rgb_topic,
            image_cb_method_t <T> cb,
            T *owner
        );
        
        void subscribe(
            const std::string &left_mono_topic,
            const std::string &right_mono_topic,
            const std::string &left_color_topic,
            const std::string &right_color_topic,
            const image_transport::TransportHints &hints = image_transport::TransportHints()
        );
    
        void subscribe(
            const std::string &left_mono_topic,
            const std::string &right_mono_topic,
            const std::string &left_color_topic,
            const std::string &right_color_topic,
            image_cb_t cb
        );
    
        void subscribe(
            const std::string &left_mono_topic,
            const std::string &right_mono_topic,
            const std::string &left_color_topic,
            const std::string &right_color_topic,
            const image_transport::TransportHints &hints,
            image_cb_t cb
        );
    
        template <class T>
        void subscribe(
            const std::string &left_mono_topic,
            const std::string &right_mono_topic,
            const std::string &left_color_topic,
            const std::string &right_color_topic,
            image_cb_method_t <T> cb,
            T *owner
        );
    
        template <class T>
        void subscribe(
            const std::string &left_mono_topic,
            const std::string &right_mono_topic,
            const std::string &left_color_topic,
            const std::string &right_color_topic,
            const image_transport::TransportHints &hints,
            image_cb_method_t <T> cb,
            T *owner
        );
    
        void unsubscribe();
    
        bool waitForFirstImage(float timeout = -1.0f, unsigned int freq = 100);
    
        /**
     * Set the callback function called each time an image is received to the specified function object.
     *
     * @param cb The callback function that will be called each time an image is received.
     */
        void setOnImageCallback(image_cb_t cb) { _on_image_cb = std::move(cb); }
    
        template <class T>
        void setOnImageCallback(image_cb_method_t<T> cb, T *owner);
    
        void clearOnImageCallback() { _on_image_cb = {}; }
    
        void setMaxImageInterval(const ros::Duration& max_interval);
    
        const ExtendedStereoImage getLatestImage();
    
        const sensor_msgs::CameraInfo& getLeftCameraInfo();
    
        const sensor_msgs::CameraInfo& getRightCameraInfo();
    
        const image_geometry::StereoCameraModel getCameraModel();
    
        bool is_subscribed() { return _subscribed; }
    
        explicit operator bool() { return is_subscribed(); }

    public: /* Callbacks */
    
        virtual void onStereoImagesReceived(
            const sensor_msgs::ImageConstPtr &left_mono,
            const sensor_msgs::ImageConstPtr &left_rgb,
            const sensor_msgs::CameraInfoConstPtr &left_camera_info,
            const sensor_msgs::ImageConstPtr &right_rgb,
            const sensor_msgs::ImageConstPtr &right_mono,
            const sensor_msgs::CameraInfoConstPtr &right_camera_info
        );
        
    private: /* Member Variables */
    
        // ROS topics & Plumbing
        
        ros::NodeHandle _nh;
        image_transport::ImageTransport _it;
    
        /// Subscriber for the left mono image topic.
        image_transport::SubscriberFilter _left_image_mono_sub;
        
        /// Subscriber for the left rgb image topic.
        image_transport::SubscriberFilter _left_image_rgb_sub;
        
        /// Subscriber for the left \c camera_info topic
        message_filters::Subscriber<sensor_msgs::CameraInfo> _left_image_camera_info_sub;
    
        /// Subscriber for the right mono image topic
        image_transport::SubscriberFilter _right_image_mono_sub;
        
        /// Subscriber for the right rgb image topic.
        image_transport::SubscriberFilter _right_image_rgb_sub;
    
        /// Subscriber for the right \c camera_info_topic
        message_filters::Subscriber<sensor_msgs::CameraInfo> _right_image_camera_info_sub;
    
        /// Overload the \c ApproximateTimeFilter to handle 2 images & 2 camera_info topics.
        typedef message_filters::sync_policies::ApproximateTime<
            sensor_msgs::Image, sensor_msgs::Image, sensor_msgs::CameraInfo,
            sensor_msgs::Image, sensor_msgs::Image, sensor_msgs::CameraInfo
        > ApproximateTimePolicy;
    
        /**
         * The synchroniser is responsible for pairing up L/R images and L/R camera_infos.
         *
         * To use it we create subscribers to all the relevant topics, in this case the two image topics and the two camera
         * info topics and connect them to the _synchroniser with the \c connectInput function. The type of the function is
         * given by the specialisation of the \c message_filters::sync_policies we just created.
         *
         * When messages are received from all the connected topics within the allotted time-frame the callback registered
         * onto the synchroniser is called. This is when we see the data for the first time.
         */
        message_filters::Synchronizer<ApproximateTimePolicy> _synchroniser;
    
        AdvertisementChecker _adv_checker;
        
        // State variables
    
        sensor_msgs::Image _left_mono_image;
        sensor_msgs::Image _right_mono_image;
        sensor_msgs::Image _left_rgb_image;
        sensor_msgs::Image _right_rgb_image;
        image_geometry::StereoCameraModel _camera_model;
    
        /// The number of complete stereo images received so far
        uint32_t _stereo_image_count = 0;
    
        bool _subscribed = false;
    
        image_cb_t _on_image_cb;
        
    };
    
    
    template<typename T>
    ExtendedStereoCamera::ExtendedStereoCamera(
        ros::NodeHandle nh,
        const std::string& left_mono_topic,
        const std::string& right_mono_topic,
        const std::string& left_rgb_topic,
        const std::string& right_rgb_topic,
        ExtendedStereoCamera::image_cb_method_t<T> cb,
        T *owner
    )
        : ExtendedStereoCamera(nh)
    {
        subscribe(left_mono_topic, right_mono_topic, left_rgb_topic, right_rgb_topic);
        setOnImageCallback(std::move(cb), owner);
    }
    
    
    template<class T>
    void ExtendedStereoCamera::subscribe(
        const std::string &left_mono_topic,
        const std::string &right_mono_topic,
        const std::string &left_color_topic,
        const std::string &right_color_topic,
        image_cb_method_t <T> cb,
        T *owner
    ) {
        subscribe(left_mono_topic, right_mono_topic, left_color_topic, right_color_topic);
        setOnImageCallback(std::move(cb), owner);
    }
    
    template<class T>
    void ExtendedStereoCamera::subscribe(
        const std::string &left_mono_topic,
        const std::string &right_mono_topic,
        const std::string &left_color_topic,
        const std::string &right_color_topic,
        const image_transport::TransportHints &hints,
        image_cb_method_t <T> cb,
        T *owner
    ) {
        subscribe(left_mono_topic, right_mono_topic, left_color_topic, right_color_topic, hints);
        setOnImageCallback(std::move(cb), owner);
    }

    /**
     * Set the callback function called each time a new image is received to the specified member function \p cb of the
     * instance of \p T passed in as \p owner.
     *
     * @tparam T The type of the class to which the specified callback belongs.
     * @param cb The callback function to call each time an image is received.
     * @param owner The class instance on which to call the callback.
     */
    template <class T>
    void ExtendedStereoCamera::setOnImageCallback(image_cb_method_t<T> cb, T* owner)
    {
        // Whilst using std::bind seems more natural apparently lambdas are more idiomatic
        _on_image_cb = [cb, owner](
            const sensor_msgs::ImageConstPtr &left_mono,
            const sensor_msgs::ImageConstPtr &right_mono,
            const sensor_msgs::ImageConstPtr &left_rgb,
            const sensor_msgs::ImageConstPtr &right_rgb,
            const image_geometry::StereoCameraModel& camera_model
        ) {
            assert(owner != nullptr && cb != nullptr);
            return (owner->*cb)(left_mono, right_mono, left_rgb, right_rgb, camera_model);
        };
    }
}
