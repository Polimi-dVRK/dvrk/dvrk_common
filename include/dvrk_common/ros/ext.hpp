
/**
 * \file
 *
 * This module implements functions that are inexplicably missing from ROS: These are mostly comparison operators and
 * factory methods.
 *
 */

#pragma once

#include <string>

#include <ros/names.h>
#include <sensor_msgs/RegionOfInterest.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Point32.h>

namespace ros {
    namespace names {
        
        /**
         * Append an arbitrary number of names together to form a complete ROS resource name.
         *
         * This method is an extension of the built-in ros::names::append method that only accepts two names using
         * modern C++ features to ensure type safety.
         *
         * @tparam Name Any type that is convertible to std::string
         * @param names The list of names to concatenate
         * @return The fully formed ROS resource name
         */
        template <typename ...Name>
        std::string append(const std::string& first, const std::string& second, Name&& ... names) {
            std::string merged_name;
            for (const auto& name : std::initializer_list<std::string>{ first, second, names... }) {
                merged_name = ros::names::append(merged_name, name);
            }
            return merged_name;
        }
        
    } // end namespace names
} // end namespace ros

namespace sensor_msgs
{

bool operator==(const RegionOfInterest &lhs, const RegionOfInterest &rhs);

}

namespace geometry_msgs {

bool operator==(const Point &lhs, const Point &rhs);

Point make_point(const double x, const double y, const double z);

bool operator==(const Point32 &lhs, const Point32 &rhs);

Point32 make_point32(const float x, const float y, const float z);

}
