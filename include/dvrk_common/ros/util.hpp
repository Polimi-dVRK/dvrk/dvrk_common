//
// Created by luca18 on 9/26/17.
//

/**
 * This module implements a function useful for extracting the ROS_HOME environment parameter or, in case
 * the latter isn't set, the HOME environment parameter to build the /.ros directory path.
 */

#pragma once

#include <ros/ros.h>

#include <boost/filesystem.hpp>

namespace dvrk
{
    const boost::filesystem::path getROSHome();
}
