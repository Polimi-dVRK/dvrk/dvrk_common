//
// Created by tibo on 12/07/17.
//

#pragma once

#include <boost/optional.hpp>

#include <ros/node_handle.h>
#include "dvrk_common/error_base.hpp"

namespace dvrk
{

namespace error {
    typedef boost::error_info<struct tag_missing_ros_param, const std::string> missing_ros_param;

    /// Error used to indicate that a required parameter could not be found.
    struct RosParamError: public virtual ErrorBase
    {
    };
}

/**
 * Read a parameter from a ROS Node. If the parameter is absent the optional will be empty.
 *
 * This pattern makes it simple to retrieve a parameter and use it immediately if it was set. Look at this example with
 * the normal ROS API:
 *
 * \code
 * ros::NodeHandle node;
 *
 * std::string my_param;
 * if (node.getParam("my_param", &my_param)) {
 *     do_something(my_param);
 * }
 * \endcode
 *
 * Using the new API is is not necessary to create the string ahead of time:
 *
 * \code
 * ros::Nodehandle node;
 *
 * if (const auto my_param = getParam<std::string>(node, "my_param") {
 *    do_something(*my_param);
 * }
 * \endcode
 *
 * It also has the advantage that the scope of the \c my_param variable is limited to the \c if statement.
 *
 * @tparam T The type of the parameter to retrieve
 * @param node The ros::NodeHandle to query for the parameter
 * @param name The of the parameter to retrieve
 * @return An optional type containing the value if the parameter was set, empty otherwise.
 */
template <class T>
boost::optional<T> getParam(ros::NodeHandle &node, const std::string& name) {
    T result;
    if (node.getParam(name, result))
        return result;
    
    return {};
}

/**
 * Get a parameter from the private namespace of a node.
 *
 * If the parameter is not set a message is printed with \c ROS_ERROR and an exception of type
 * #RosParamError is thrown.
 *
 * This usually occurs if the instead of passing a private node (\c ros::NodeHandle("~")) you passed in a normal node.
 *
 * @tparam T The type of the value to query. This is also the type of the returned value.
 * @param node The nodehandle to use to retrieve the argument (usually a private node)
 * @param The name of the parameter to query.
 * @return The value of the parameter.
 */
template<class T>
T getRequiredParam(ros::NodeHandle &node, const std::string name)
{
    T value;
    if (!node.getParam(name, value)) {
        ROS_ERROR("Unable to retrieve parameter <%s>.", node.resolveName(name).c_str());
        throw error::RosParamError() << error::missing_ros_param(name);
    }

    return value;
}

}