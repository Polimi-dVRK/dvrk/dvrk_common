//
// Created by nearlab on 17/07/17.
//

#pragma once

#include <opencv2/core/types.hpp>

// ROS types
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Point32.h>
#include <sensor_msgs/RegionOfInterest.h>

#include "dvrk_common/ros/ext.hpp"
#include "dvrk_common/error_base.hpp"

namespace dvrk {

    namespace error
    {
        /// Used to signal that a type cannot be converted.
        class ConversionError: public virtual ErrorBase
        {
        };
    }
/*
 *  ROS: Region Of Interest <==> CV: Rect
 */


/**
 * Build a \c sensor_msgs::RegionOfInterest message from a \c cv::Rect object and a boolean.
 *
 * @tparam T The datatype of the coordinates stored in the Rect. The \c cv::Rect default is \c int
 * @param roi The cv::Rect object defining the bounds of the region of interest
 * @param do_rectify the \c do_rectifiy property of the region of interest message
 * @return A fully formed region of interest message
 */
template <class T>
sensor_msgs::RegionOfInterest toROI(const cv::Rect_<T> &roi, const bool do_rectify = false)
{
    if (roi.x < 0 || roi.y < 0 || roi.width < 0 || roi.height < 0) {
        BOOST_THROW_EXCEPTION(
            error::ErrorBase() << error::errmsg("The input cv::Rect contains negative components")
        );
    }

    sensor_msgs::RegionOfInterest ros_roi;
    ros_roi.x_offset = static_cast<uint32_t>(std::floor(roi.x));
    ros_roi.y_offset = static_cast<uint32_t>(std::floor(roi.y));
    ros_roi.width    = static_cast<uint32_t>(std::floor(roi.width));
    ros_roi.height   = static_cast<uint32_t>(std::floor(roi.height));
    ros_roi.do_rectify = do_rectify;

    return ros_roi;
}

/**
 * Generic implementation of #toRect. The specialised versions #toRect, #toRect2i, #toRect2f and #toRect2d should be
 * preferred. The \c do_rectify property of the source will be discarded since it cannot be represented in the \c
 * cv::Rect object.
 *
 * @tparam T
 *  The type of the data member of the return cv::Rect_<T> object. For example if \c T is \c int the return value will
 *  be a cv::Rect2i.
 * @param roi The sensor_msgs::regionOfInterest messagge to convert to a cv::Rect_<T>
 * @return A cv::Rect<T> object describing the region of interest of the input \p roi object
 */
template <class T>
cv::Rect_<T> toRect_(const sensor_msgs::RegionOfInterest& roi) {
    return cv::Rect_<T>(
        static_cast<T>(std::floor(roi.x_offset)),
        static_cast<T>(std::floor(roi.y_offset)),
        static_cast<T>(std::floor(roi.width)),
        static_cast<T>(std::floor(roi.height))
    );
}

/**
 * Build a \c cv::Rect2i object from a \c sensor_msgs::RegionOfInterest object. This discards the value of the \c
 * do_rectify property of the source since it cannot be represented in the \c cv::Rect object.
 *
 * Note: \c cv::Rect is an alias for \c cv::Rect2i and calling this function is equivalent to calling #toRect2i
 *
 * @param roi The source region of interest message
 * @return A \c cv::Rect object describing the same region of interest as the source
 */
inline cv::Rect toRect(const sensor_msgs::RegionOfInterest &roi) { return toRect_<int>(roi); }

/**
 * Build a \c cv::Rect2i object from a \c sensor_msgs::RegionOfInterest object. This discards the value of the \c
 * do_rectify property of the source since it cannot be represented in the \c cv::Rect2i object.
 *
 * @param roi The source region of interest message
 * @return A \c cv::Rect2i object describing the same region of interest as the source
 */
inline cv::Rect2i toRect2i(const sensor_msgs::RegionOfInterest &roi) { return toRect_<int>(roi); }

/**
 * Build a \c cv::Rect2f object from a \c sensor_msgs::RegionOfInterest object. This discards the value of the \c
 * do_rectify property of the source since it cannot be represented in the \c cv::Rect2f object.
 *
 * @param roi The source region of interest message
 * @return A \c cv::Rect2f object describing the same region of interest as the source
 */
inline cv::Rect2f toRect2f(const sensor_msgs::RegionOfInterest &roi) { return toRect_<float>(roi); }

/**
 * Build a \c cv::Rect2d object from a \c sensor_msgs::RegionOfInterest object. This discards the value of the \c
 * do_rectify property of the source since it cannot be represented in the \c cv::Rect2d object.
 *
 * @param roi The source region of interest message
 * @return A \c cv::Rect2d object describing the same region of interest as the source
 */
inline cv::Rect2d toRect2d(const sensor_msgs::RegionOfInterest &roi) { return toRect_<double>(roi); }


/*
 * ROS: geometry_msgs::Point <=> CV: Point2/Point3
 */

/**
 * Generic implementation of #toPoint operating on geometry_msgs::Point objects. The specialised versions #toPoint,
 * #toPoint2i, #toPoint2f and #toPoint2f should  be preferred. The \c z component of the point will be discarded in
 * the 2d to 3d conversion.
 *
 * @tparam T
 *  The type of the data members of the returned cv::Point_<T> object. For example if \c T is \c int the returned value
 *  will be a cv::Point2i.
 * @param pt The geometry_msgs::Point object to convert
 * @return A cv::Point_<T> representing the same point as the input
 */
template<typename T>
cv::Point_<T> toPoint_(const geometry_msgs::Point &pt)
{
    return cv::Point_<T>(
        static_cast<T>(pt.x), static_cast<T>(pt.y)
    );
}


/**
 * Generic implementation of #toPoint operating on geometry_msgs::Point32 objects. The specialised versions #toPoint,
 * #toPoint2i, #toPoint2f and #toPoint2f should  be preferred. The \c z component of the point will be discarded in
 * the 2d to 3d conversion.
 *
 * @tparam T
 *  The type of the data members of the returned cv::Point_<T> object. For example if \c T is \c int the returned value
 *  will be a cv::Point2i.
 * @param pt The geometry_msgs::Point32 object to convert
 * @return A cv::Point_<T> representing the same point as the input
 */
template<typename T>
cv::Point_<T> toPoint_(const geometry_msgs::Point32 &pt)
{
    return cv::Point_<T>(
        static_cast<T>(pt.x), static_cast<T>(pt.y)
    );
}

/**
 * Build a cv::Point from a \c geometry_msgs::Point object. The \c z component of the point will be discarded in
 * the 2d to 3d conversion.
 *
 * @param pt A geometry_msgs::Point object to convert
 * @return A cv::Point representing the same value as the input
 */
inline cv::Point toPoint(const geometry_msgs::Point &pt) { return toPoint_<int>(pt); }

/**
 * Build a cv::Point2i from a \c geometry_msgs::Point object. The \c z component of the point will be discarded in
 * the 2d to 3d conversion.
 *
 * @param pt A geometry_msgs::Point object to convert
 * @return A cv::Point2i representing the same value as the input
 */
inline cv::Point2i toPoint2i(const geometry_msgs::Point &pt) { return toPoint_<int>(pt); }

/**
 * Build a cv::Point2f from a \c geometry_msgs::Point object. The \c z component of the point will be discarded in
 * the 2d to 3d conversion.
 *
 * @param pt A geometry_msgs::Point object to convert
 * @return A cv::Point2f representing the same value as the input
 */
inline cv::Point2f toPoint2f(const geometry_msgs::Point &pt) { return toPoint_<float>(pt); }

/**
 * Build a cv::Point2d from a \c geometry_msgs::Point object. The \c z component of the point will be discarded in
 * the 2d to 3d conversion.
 *
 * @param pt A geometry_msgs::Point object to convert
 * @return A cv::Point2d representing the same value as the input
 */
inline cv::Point2d toPoint2d(const geometry_msgs::Point &pt) { return toPoint_<double>(pt); }

/**
 * Build a cv::Point from a \c geometry_msgs::Point object. The \c z component of the point will be discarded in
 * the 2d to 3d conversion.
 *
 * @param pt A geometry_msgs::Point object to convert
 * @return A cv::Point representing the same value as the input
 */
inline cv::Point toPoint(const geometry_msgs::Point32 &pt) { return toPoint_<int>(pt); }

/**
 * Build a cv::Point2i from a \c geometry_msgs::Point32 object. The \c z component of the point will be discarded in
 * the 2d to 3d conversion.
 *
 * @param pt A geometry_msgs::Point32 object to convert
 * @return A cv::Point2i representing the same value as the input
 */
inline cv::Point2i toPoint2i(const geometry_msgs::Point32 &pt) { return toPoint_<int>(pt); }

/**
 * Build a cv::Point2f from a \c geometry_msgs::Point32 object. The \c z component of the point will be discarded in
 * the 2d to 3d conversion.
 *
 * @param pt A geometry_msgs::Point32 object to convert
 * @return A cv::Point2f representing the same value as the input
 */
inline cv::Point2f toPoint2f(const geometry_msgs::Point32 &pt) { return toPoint_<float>(pt); }

/**
 * Build a cv::Point2d from a \c geometry_msgs::Point32 object. The \c z component of the point will be discarded in
 * the 2d to 3d conversion.
 *
 * @param pt A geometry_msgs::Point32 object to convert
 * @return A cv::Point2d representing the same value as the input
 */
inline cv::Point2d toPoint2d(const geometry_msgs::Point32 &pt) { return toPoint_<double>(pt); }

/**
 * Generic implementation of #toPoint3 operating on geometry_msgs::Point objects. The specialised versions #toPoint3,
 * #toPoint3i, #toPoint3f and #toPoint3f should  be preferred.
 *
 * @tparam T
 *  The type of the data members of the returned cv::Point3_<T> object. For example if \c T is \c int the returned value
 *  will be a cv::Point3i.
 * @param pt The geometry_msgs::Point object to convert
 * @return A cv::Point3_<T> representing the same point as the input
 */
template<typename T>
cv::Point3_<T> toPoint3_(const geometry_msgs::Point &pt)
{
    return cv::Point3_<T>(
        static_cast<T>(pt.x), static_cast<T>(pt.y), static_cast<T>(pt.z)
    );
}



/**
 * Generic implementation of #toPoint3 operating on geometry_msgs::Point32 objects. The specialised versions #toPoint3,
 * #toPoint3i, #toPoint3f and #toPoint3f should  be preferred.
 *
 * @tparam T
 *  The type of the data members of the returned cv::Point3_<T> object. For example if \c T is \c int the returned value
 *  will be a cv::Point3i.
 * @param pt The geometry_msgs::Point object to convert
 * @return A cv::Point3_<T> representing the same point as the input
 */
template<typename T>
cv::Point3_<T> toPoint3_(const geometry_msgs::Point32 &pt)
{
    return cv::Point3_<T>(
        static_cast<T>(pt.x), static_cast<T>(pt.y), static_cast<T>(pt.z)
    );
}

/**
 * Build a cv::Point3i from a \c geometry_msgs::Point object.
 *
 * @param pt A geometry_msgs::Point object to convert
 * @return A cv::Point3i representing the same value as the input
 */
inline cv::Point3i toPoint3i(const geometry_msgs::Point &pt) { return toPoint3_<int>(pt); }

/**
 * Build a cv::Point3f from a \c geometry_msgs::Point object.
 *
 * @param pt A geometry_msgs::Point object to convert
 * @return A cv::Point3f representing the same value as the input
 */
inline cv::Point3f toPoint3f(const geometry_msgs::Point &pt) { return toPoint3_<float>(pt); }

/**
 * Build a cv::Point3d from a \c geometry_msgs::Point object.
 *
 * @param pt A geometry_msgs::Point object to convert
 * @return A cv::Point3d representing the same value as the input
 */
inline cv::Point3d toPoint3d(const geometry_msgs::Point &pt) { return toPoint3_<double>(pt); }

/**
 * Build a cv::Point3i from a \c geometry_msgs::Point32 object.
 *
 * @param pt A geometry_msgs::Point object to convert
 * @return A cv::Point3i representing the same value as the input
 */
    inline cv::Point3i toPoint3i(const geometry_msgs::Point32 &pt) { return toPoint3_<int>(pt); }

/**
 * Build a cv::Point3f from a \c geometry_msgs::Point32 object.
 *
 * @param pt A geometry_msgs::Point object to convert
 * @return A cv::Point3f representing the same value as the input
 */
    inline cv::Point3f toPoint3f(const geometry_msgs::Point32 &pt) { return toPoint3_<float>(pt); }

/**
 * Build a cv::Point3d from a \c geometry_msgs::Point32 object.
 *
 * @param pt A geometry_msgs::Point object to convert
 * @return A cv::Point3d representing the same value as the input
 */
    inline cv::Point3d toPoint3d(const geometry_msgs::Point32 &pt) { return toPoint3_<double>(pt); }

    
/**
 * Build a geometry_msgs::Point from a cv::Point_<T>. Since this is a 2D point type the \c z coordinate will be set to 0.
 *
 * @tparam T The data type of the input cv::Point_<T>
 * @param pt The point to convert
 * @return A geometry_msgs::Point object representing the same value as the input
 */
template <typename T>
geometry_msgs::Point toPoint(const cv::Point_<T>& pt) {
    return geometry_msgs::make_point(
        static_cast<double>(pt.x),
        static_cast<double>(pt.y),
        0.0
    );
}


/**
 * Build a geometry_msgs::Point32 from a cv::Point_<T>. Since this is a 2D point type the \c z coordinate will be set to 0.
 *
 * @tparam T The data type of the input cv::Point_<T>
 * @param pt The point to convert
 * @return A geometry_msgs::Point32 object representing the same value as the input
 */
template <typename T>
geometry_msgs::Point32 toPoint32(const cv::Point_<T>& pt) {
    return geometry_msgs::make_point32(
        static_cast<float>(pt.x),
        static_cast<float>(pt.y),
        0.0f
    );
}


/**
 * Build a geometry_msgs::Point from a cv::Point_<T>.
 *
 * @tparam T The data type of the input cv::Point_<T>
 * @param pt The point to convert
 * @return A geometry_msgs::Point object representing the same value as the input
 */
template <typename T>
geometry_msgs::Point toPoint(const cv::Point3_<T>& pt) {
    return geometry_msgs::make_point(
        static_cast<double>(pt.x),
        static_cast<double>(pt.y),
        static_cast<double>(pt.z)
    );
}


/**
 * Build a geometry_msgs::Point32 from a cv::Point_<T>.
 *
 * @tparam T The data type of the input cv::Point_<T>
 * @param pt The point to convert
 * @return A geometry_msgs::Point32 object representing the same value as the input
 */
template <typename T>
geometry_msgs::Point32 toPoint32(const cv::Point3_<T>& pt) {
    return geometry_msgs::make_point32(
        static_cast<float>(pt.x),
        static_cast<float>(pt.y),
        static_cast<float>(pt.z)
    );
}

} // End namespace dvrk
