//
// Created by tibo on 04/09/17.
//


#pragma once

#include <ros/node_handle.h>
#include <ros/wall_timer.h>

namespace dvrk {

/**
 * The Advertisement Checker class is a tool to ensure that a set of topics are being published and 
 * warn if some are not.
 *
 * The class will check that the topics are being published periodically until all the topics are 
 * seen to be advertised by the master. If a topic is not advertised a warning will be logged in the console.
 *
 * The simples use of the class is:
 * @code
 *
 * ros::NodeHandle nh;
 *
 * AdvertisementChecker checker(nh);
 * checker.check({"image_raw", "image_mono", "image_color"});
 *
 * ros::spin();
 * @endcode
 *
 */
class AdvertisementChecker
{
public:
    
    /// Constructor
    explicit AdvertisementChecker(const ros::NodeHandle& nh = ros::NodeHandle());
    
    ~AdvertisementChecker();
    
    void check(const std::vector<std::string>& topics, const double period = 15.0, const bool immediate = false);
    
    void stop();
    
private: /* Callbacks */
    
    void onTimer();
    
private: /* Members */
    
    ros::NodeHandle _node;
    
    /// Timer used to schedule checks
    ros::WallTimer _timer;
    
    /// The list of topics that are not known to be advertised
    std::vector<std::string> _topics;
    
    
};

} // end namespace dvrk
