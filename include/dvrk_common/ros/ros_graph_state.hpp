//
// Created by tibo on 26/03/18.
//

#pragma once

#include <boost/optional.hpp>

#include <map>
#include <string>


namespace dvrk {
    
    std::string dumpROSState();
    
    struct TopicInfo {
        std::string name;
        std::string datatype;
        
        std::vector<std::string> publishers;
        std::vector<std::string> subscribers;
        
        TopicInfo() = default;
        
        TopicInfo(std::string name, std::string datatype)
            : name(std::move(name)), datatype(std::move(datatype))
        {
            // Do Nothing
        }
    };
    
    /**
     * The dvrk::State class aims to provide a simple way of retrieving the entire state of the ROS
     * network in one go. It uses the _lower_ level `XmlRpc` API to achieve this.
     */
    class ROSGraphState {
    public:
        ROSGraphState();
        
        std::string getNodeName() const { return _node_name; }
        std::string getHostname() const { return _ros_hostname; }
        std::string getMasterURI() const { return _ros_master_uri; }
    
        boost::optional<const TopicInfo&> getTopic(const std::string& topic_name) const;
        
        const std::vector<std::string>& getPublishersFor(const std::string& topic_name) const;
    
        const std::vector<std::string>& getSubscribersFor(const std::string& topic_name) const;
        
        std::vector<std::string> getAllTopics() const;
        
        const std::map<std::string, TopicInfo>& getAll() const { return _ros_topics; };
        
    private:
        std::string _node_name;
        std::string _ros_hostname;
        std::string _ros_master_uri;
        
        std::map<std::string, TopicInfo> _ros_topics;
        
    };
    
}
