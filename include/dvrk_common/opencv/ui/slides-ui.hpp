//
// Created by tibo on 24/08/17.
//


#pragma once

#include <memory>

// have to include core instead of individual modules to get putText, getTextSize, ...
#include <opencv2/core.hpp>

#include "dvrk_common/opencv/util.hpp"

/**
 * The dvrk::ui namespace contains the \c SlideUI classes.
 *
 * These classes implement a verry simple, non interactive UI system used to create simple slides programmatically. It
 * can be used to show the results of an image processing task or an overview of a set of images for example.
 */
namespace dvrk
{

namespace ui
{
    
    /**
     * The UIElement class is the root class for all of the UI classes.
     */
    class UIElement
    {
    public:
        virtual ~UIElement() = default;
        
        static constexpr unsigned int Padding = 10; //px
        
        cv::Rect2i getRect()
        { return _rect; }
        
        virtual void layout(const cv::Rect2i &max_rect) = 0;
        virtual void render(cv::Mat &window) = 0;
    
    protected:
        ///
        cv::Scalar _background_colour = Colour::Black;
        
        /// The bounding box of the element
        cv::Rect2i _rect = cv::Rect(0, 0, 0, 0);
        
        /// Whether or not the element has changed since its layout was last computed
        bool _relayout_required = true;
        
    };
    
    /**
     * The window class acts as the entry point for creating new slides.
     *
     * After being initialised with a name and a size it acts as a container of child nodes that will be ddrawn onto the
     * final image. This framework is intended to be extremely simple. Items added to the window are rendered below the
     * previous item but future work might add a horizontal layout group to extend the formatting options.
     */
    class Window: public UIElement
    {
    public:
        
        explicit Window(const std::string &name, unsigned int width = 1024)
            : _name(name), _width(width)
        {}
        
        ~Window() override;
        
        void addChild(UIElement *child);
        
        void layout(const cv::Rect2i &max_rect) override;
        void render(cv::Mat &window) override;
        
        virtual void render();
        virtual void show();
        
        void save(std::string filename);
    
    private:
        std::string _name;
        unsigned int _width = 0;
        
        /// The buffer into which the window will be drawn into with Widnow::render
        cv::Mat _paint_buffer;
        
        std::vector<std::unique_ptr<UIElement>> _children;
        
    };
    
    /// Should the element be left aligned, centered of right aligned ?
    enum class Alignment
    {
        Left, Centre, Right
    };
    
    /**
     * The text node contains a string of text and the information required to draw it in the window.
     *
     * The text is not wrapped (yet). Multiline text can be passed in as a vector of strings, each new string will be
     * drawn onto the next line. No automatic text formatting is supported (yet). In future a simple html-like markup
     * might be implemented to customise size colour and alignment.
     *
     * @todo Wrap text intelligently
     * @todo Support a simple html-like syntax for text attributes
     */
    class Text: public UIElement
    {
    public: /* Enums and structs */
        
        /// How big should this text be ?
        enum class Size
        {
            ExtraSmall, Small, Normal, Big, ExtraBig, Huge, ExtraHuge
        };
    
    public:
        Text() = default;
        Text(const std::string &text);
        Text(const std::vector<std::string> &text);
        
        Text *setText(const std::string &text);
        Text *setText(const std::vector<std::string> &text);
        
        Text *setFont(const int text_font);
        Text *setScale(const float text_scale);
        Text *setScale(const Size text_scale);
        Text *setLineThickness(const int line_thickness);
        Text *setColor(const cv::Scalar &text_color);
        Text *setLineSpacing(const float line_sep);
        Text *setAlignment(const Alignment alignment);
        
        void layout(const cv::Rect2i &max_rect) override;
        void render(cv::Mat &window) override;
    
    protected: /* Enums and structs */
        
        struct TextLine
        {
            std::string text; // The actual text contents
            cv::Rect2i rect; // The bounding box of the text
        };
    
    protected:
        std::vector<TextLine> _text_lines;
        
        int _text_font = cv::HersheyFonts::FONT_HERSHEY_DUPLEX;
        float _text_scale = 0.5f;
        int _line_thickness = 1;
        cv::Scalar _text_color = Colour::White;
        float _line_sep = 5.0f;
        Alignment _alignment = Alignment::Left;
    };
    
    /**
     * The Image class represents an image and an optional legend. The legend is drawn below the image and does not
     * support wrapping (yet).
     *
     * @todo Support wrapping for the legend
     */
    class Image: public UIElement
    {
    public:
        Image() = default;
        explicit Image(const cv::Mat &image, const std::string &legend = "");
        Image(const cv::Mat &image, const Text &legend);
        
        Image *setImage(const cv::Mat &image, const std::string &legend = "");
        Image *setImage(const cv::Mat &image, const Text &legend);
        
        void layout(const cv::Rect2i &max_rect) override;
        void render(cv::Mat &window) override;
    
    private:
        
        cv::Mat _image;
        cv::Rect2i _image_rect;
        
        Text _legend;
    };
    
    /**
     * The ImageGrid class represents a grid of images. The images and their legends will be drawn left-to-right up to a
     * user specified width and top-to-button.
     */
    class ImageGrid: public UIElement
    {
    public:
        ImageGrid() = default;
        
        ImageGrid *addImage(const cv::Mat &image, const std::string &legend);
        ImageGrid *addImage(const Image &image);
        ImageGrid *addImage(const Image *image);
        
        ImageGrid *setImagesPerLine(const unsigned int images_per_line);;
        
        void layout(const cv::Rect2i &max_rect) override;
        void render(cv::Mat &window) override;
    
    private:
        
        unsigned int _images_per_line = 2;
        std::vector<Image> _images;
    };
} // end namespace dvrk::ui

} // end namespace dvrk