
/**
 * This module implements parsing functions useful for extracting structured objects from ros parameters. A simple
 * example is the \c dvrk::parseRect function. It could be specfied in a yaml file loaded with ROS but that would mean
 * that to use it we would need to use the \c XmlRpc API which can be quite unintuitive. Instead the function allows us
 * to extract the \c cv::Rect object directly from a string.
 */

#pragma once

#include <boost/optional.hpp>
#include <opencv2/core/core.hpp>

namespace dvrk
{

/// Parse a cv::Rect from a string
boost::optional <cv::Rect> parseRect(std::string rect_str);

/// Parse a cv::Point from a string
boost::optional <cv::Point> parsePoint(std::string point_str);

/// Parse a cv::Size from a string
boost::optional <cv::Size> parseSize(std::string size_str);

}