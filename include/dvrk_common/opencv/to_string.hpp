//
// Created by nearlab on 25/10/17.
//


#pragma once

#include <opencv2/core/core.hpp>
#include "dvrk_common/to_string.hpp"

/**
 * Convert a cv::Rect to a string in (x, y) + (w, h) format.
 *
 * @param os The stream onto which the rect string will be written
 * @param rect The rect to display
 *
 * @return The stream in order to enable chaining
 */
template <class T>
std::ostream &operator<<(std::ostream &os, const cv::Rect_<T> &rect) {
    os << "(" << rect.x << ", " << rect.y << ") + (" << rect.width << ", " << rect.height << ")";
    return os;
}
