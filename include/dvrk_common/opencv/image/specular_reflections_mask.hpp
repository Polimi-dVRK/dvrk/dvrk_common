//
// Created by tibo on 02/08/17.
//

#pragma once

#include <opencv2/core/mat.hpp>

namespace dvrk
{

cv::Mat create_specular_reflections_mask(const cv::Mat &src);

/**
 * Remove specular reflections from the image.
 *
 * The method used in this function was described by Barbalat & Matos in "Laryngeal Tumour  Detection and Classification
 * In Endoscopic Video" published in IEEE Journal of Biomedical and Health Infomatics (DOI: ??)
 *
 * @param bgr_img The source image encoded as BGR
 */
void create_specular_reflections_mask(const cv::Mat &src, cv::Mat &mask);

}
