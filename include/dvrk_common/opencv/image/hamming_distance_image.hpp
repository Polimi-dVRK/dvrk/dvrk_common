//
// Created by tibo on 05/09/17.
//

#pragma once

#include <opencv2/core/core.hpp>

namespace dvrk
{
    
    class HammingDistanceImage
    {
    public:
        // TODO: Manage multi channel images
        const cv::Mat& apply(const cv::Mat &lhs, const cv::Mat &rhs);
        
    private:
        cv::Mat result;
    };
    
}
