//
// Created by tibo on 03/08/17.
//


#pragma once

#include <memory>
#include <opencv2/core/core.hpp>
#include <opencv2/core/types.hpp>

namespace dvrk
{

/**
 * The modified census transform, described in ...
 */
class ModifiedCensusTransform
{
public:
    ModifiedCensusTransform() = delete;
    explicit ModifiedCensusTransform(const cv::Size &window_size);

    const cv::Mat apply(const cv::Mat &src);

    void applyParallel(const cv::Mat& src);
    

    size_t getBytesPerPixel() const { return _bytes_per_pixel; }
    cv::Size getWindowSize() const { return _window_size; }

protected:

    void setPixelAt(const cv::Point &pos, const std::vector<uint8_t> &data);
    void setPixelAt(const int row, const int col, const std::vector<uint8_t> &new_data);

    std::vector<uint8_t> getPixelAt(const cv::Point &pos) const;
    std::vector<uint8_t> getPixelAt(const int row, const int col) const;
    
    void processPixel(const cv::Mat &window, std::vector<uint8_t> &result);

protected: /* Member Variables */
    cv::Size _window_size = {8, 8}; // Maximum value that still fits in a double (8*8 = 64bit)

    unsigned int _bytes_per_pixel = 0;

    cv::Mat _data;
};

}
