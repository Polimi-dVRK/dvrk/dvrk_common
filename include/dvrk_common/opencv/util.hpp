//
// Created by tibo on 19/07/17.
//


#pragma once

#include <opencv2/core/core.hpp>

namespace dvrk
{

    namespace Colour {
        // CSS3 - Basic Colour Keywords
        const cv::Scalar Black   = {  0,   0,   0};
        const cv::Scalar Silver  = {192, 192, 192};
        const cv::Scalar Gray    = {128, 128, 128};
        const cv::Scalar White   = {255, 255, 255};
        const cv::Scalar Maroon  = {  0,   0, 128};
        const cv::Scalar Red     = {  0,   0, 255};
        const cv::Scalar Purple  = {128,   0, 128};
        const cv::Scalar Fuchsia = {255, 255, 255};
        const cv::Scalar Green   = {  0, 128,   0};
        const cv::Scalar Lime    = {  0, 255,   0};
        const cv::Scalar Olive   = {128,   0, 128};
        const cv::Scalar Yellow  = {255,   0, 255};
        const cv::Scalar Navy    = {128,   0,   0};
        const cv::Scalar Blue    = {255,   0,   0};
        const cv::Scalar Teal    = {128, 128,   0};
        const cv::Scalar Aqua    = {255, 255,   0};
        
        // CSS3 - Extended Colour Keywords
        const cv::Scalar AliceBlue            = {255, 248, 240};
        const cv::Scalar AntiqueWhite         = {215, 235, 250};
        const cv::Scalar Aquamarine           = {212, 255, 127};
        const cv::Scalar Azure                = {255, 255, 240};
        const cv::Scalar Beige                = {220, 245, 245};
        const cv::Scalar Bisque               = {196, 228, 255};
        const cv::Scalar BlanchedAlmond       = {205, 235, 255};
        const cv::Scalar BlueViolet           = {226,  43, 138};
        const cv::Scalar Brown                = { 42,  42, 165};
        const cv::Scalar BurlyWood            = {135, 184, 222};
        const cv::Scalar CadetBlue            = {160, 158,  95};
        const cv::Scalar Chartreuse           = {  0, 255, 127};
        const cv::Scalar Chocolate            = { 30, 105, 210};
        const cv::Scalar Coral                = { 80, 127, 255};
        const cv::Scalar CornflowerBlue       = {237, 149, 100};
        const cv::Scalar CornSilk             = {220, 248, 255};
        const cv::Scalar Crimson              = { 60,  20, 220};
        const cv::Scalar DarkBlue             = {139,   0,   0};
        const cv::Scalar DarkCyan             = {139, 139,   0};
        const cv::Scalar DarkGoldenRod        = { 11, 134, 184};
        const cv::Scalar DarkGray             = {169, 169, 169};
        const cv::Scalar DarkGreen            = {  0, 100,   0};
        const cv::Scalar DarkKhaki            = {107, 183, 189};
        const cv::Scalar DarkMagenta          = {139,   0, 139};
        const cv::Scalar DarkOliveGreen       = { 47, 105,  85};
        const cv::Scalar DarkOrange           = {  0, 140, 255};
        const cv::Scalar DarkOrchid           = {204,  50, 153};
        const cv::Scalar DarkRed              = {  0,   0, 139};
        const cv::Scalar DarkSalmon           = {122, 150, 233};
        const cv::Scalar DarkSeaGreen         = {143, 188, 143};
        const cv::Scalar DarkSlateBlue        = {139,  61,  72};
        const cv::Scalar DarkSlateGray        = { 79,  79,  47};
        const cv::Scalar DarkTurquoise        = {209, 206,   0};
        const cv::Scalar DarkViolet           = {211,   0, 148};
        const cv::Scalar DeepPink             = {147,  20, 255};
        const cv::Scalar DeepSkyBlue          = {255, 191,   0};
        const cv::Scalar DimGray              = {105, 105, 105};
        const cv::Scalar DodgerBlue           = {205, 144,  30};
        const cv::Scalar FireBrick            = { 34,  34, 178};
        const cv::Scalar FloralWhite          = {240, 250, 255};
        const cv::Scalar ForestGreen          = { 34, 139,  34};
        const cv::Scalar Gainsboro            = {220, 220, 220};
        const cv::Scalar GhostWhite           = {255, 248, 248};
        const cv::Scalar Gold                 = {  0, 215, 255};
        const cv::Scalar GoldenRod            = { 32, 165, 218};
        const cv::Scalar GreenYellow          = { 47, 255, 173};
        const cv::Scalar HoneyDew             = {240, 255, 240};
        const cv::Scalar HotPink              = {180, 105, 255};
        const cv::Scalar IndianRed            = { 92,  92, 205};
        const cv::Scalar Indigo               = {130,   0,  75};
        const cv::Scalar Ivory                = {240, 255, 255};
        const cv::Scalar Khaki                = {140, 230, 240};
        const cv::Scalar Lavender             = {250, 230, 230};
        const cv::Scalar LavenderBlush        = {245, 240, 255};
        const cv::Scalar LawnGreen            = {  0, 252, 124};
        const cv::Scalar LemonChiffon         = {205, 250, 255};
        const cv::Scalar LighBlue             = {230, 216, 173};
        const cv::Scalar LightCoral           = {128, 128, 240};
        const cv::Scalar LightCyan            = {255, 255, 224};
        const cv::Scalar LightGoldenrodYellow = {211, 250, 250};
        const cv::Scalar LightGray            = {211, 211, 211};
        const cv::Scalar LightPink            = {193, 182, 255};
        const cv::Scalar LightSalmon          = {122, 160, 255};
        const cv::Scalar LightSeaGreen        = {170, 178,  32};
        const cv::Scalar LightSkyBlue         = {250, 206, 135};
        const cv::Scalar LightSlateGray       = {153, 136, 119};
        const cv::Scalar LightSteelBlue       = {222, 196, 176};
        const cv::Scalar LightYellow          = {224, 255, 255};
        const cv::Scalar LimeGreen            = { 50, 205,  50};
        const cv::Scalar Linen                = {230, 240, 250};
        const cv::Scalar MediumAquarine       = {170, 205, 105};
        const cv::Scalar MediumBlue           = {205,   0,   0};
        const cv::Scalar MediumOrchid         = {211,  85, 186};
        const cv::Scalar MediumPurple         = {219, 112, 147};
        const cv::Scalar MediumSeaGreen       = {113, 179,  60};
        const cv::Scalar MediumSlateBlue      = {238, 104, 123};
        const cv::Scalar MediumSpringGreen    = {154, 250,   0};
        const cv::Scalar MediumTurquoise      = {205, 209,  72};
        const cv::Scalar MediumVioletRed      = {133,  21, 199};
        const cv::Scalar MidnightBlue         = {112,  25,  25};
        const cv::Scalar MintCream            = {250, 255, 245};
        const cv::Scalar MistyRose            = {225, 228, 255};
        const cv::Scalar Moccasin             = {181, 228, 255};
        const cv::Scalar NavajoWhite          = {230, 245, 255};
        const cv::Scalar OldLace              = {230, 254, 253};
        const cv::Scalar OliveDrab            = { 35, 142, 107};
        const cv::Scalar Orange               = {  0, 165, 255};
        const cv::Scalar OrangeRed            = {  0,  69, 255};
        const cv::Scalar Orchid               = {214, 112, 218};
        const cv::Scalar PaleGoldenrod        = {170, 232, 238};
        const cv::Scalar PaleGreen            = {152, 251, 152};
        const cv::Scalar PaleTurquoise        = {238, 238, 175};
        const cv::Scalar PaleVioletRed        = {147, 112, 219};
        const cv::Scalar PapayaWhip           = {213, 239, 255};
        const cv::Scalar Peachpuff            = {185, 218, 255};
        const cv::Scalar Peru                 = { 63, 133, 205};
        const cv::Scalar Pink                 = {203, 192, 255};
        const cv::Scalar Plum                 = {221, 160, 221};
        const cv::Scalar PowederBlue          = {230, 224, 176};
        const cv::Scalar RosyBrown            = {143, 143, 188};
        const cv::Scalar RoyalBlue            = {225, 105,  65};
        const cv::Scalar SaddleBrown          = { 19,  69, 139};
        const cv::Scalar Salmon               = {114, 128, 250};
        const cv::Scalar SandyBrown           = { 94, 164, 244};
        const cv::Scalar SeaGreen             = { 87, 139,  46};
        const cv::Scalar Seashell             = {238, 245, 255};
        const cv::Scalar Sienna               = { 45,  82, 160};
        const cv::Scalar SkyBlue              = {235, 206, 135};
        const cv::Scalar SlateBlue            = {205,  90, 106};
        const cv::Scalar SlateGray            = {144, 128, 112};
        const cv::Scalar Snow                 = {250, 250, 255};
        const cv::Scalar SpringGreen          = {127, 255,   0};
        const cv::Scalar SteelBlue            = {180, 130,  70};
        const cv::Scalar Tan                  = {140, 180, 210};
        const cv::Scalar Thistle              = {216, 191, 216};
        const cv::Scalar Tomato               = { 71,  99, 255};
        const cv::Scalar Turquoise            = {208, 224,  64};
        const cv::Scalar Violet               = {238, 130, 238};
        const cv::Scalar Wheat                = {179, 222, 245};
        const cv::Scalar WhiteSmoke           = {245, 245, 245};
        const cv::Scalar YellowGreen          = { 50, 205, 154};

        const cv::Scalar Info = LightCyan;
        const cv::Scalar Warning = LightSalmon;
        const cv::Scalar Error = Tomato;
        const cv::Scalar Success = MediumSpringGreen;
    }

    void opencvKeyboardHandler(const std::string &window_name, const char keycode);
    
    bool is_equal(const cv::Mat& lhs, const cv::Mat rhs);
    
    std::string matTypeToString(const cv::Mat& mat);
}