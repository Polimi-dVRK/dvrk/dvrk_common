
#include <opencv2/highgui.hpp>

namespace dvrk {
    
    /**
     * This is the list of keys on my keyboard. (Italian QWERTY). This list doesn't cover the full
     * ASCII range and I will add more keys as required. The keycodes correspond to the output of
     * the cv::waitKey function.
     *
     * Example usage:
     *
     *     KeyCode key = dvrk::waitKey(1 / 24.0);
     *     if (key == dvrk::KeyCode::Escape) exit(0);
     *
     */
    enum class KeyCode {
        
        Backspace = 8,
        Tab = 9,
        Enter = 12,
    
        Escape = 27,
        
        Apostrophe = 39,
        
        Star = 42,
        Plus = 43,
        Comma = 44,
        Minus = 45,
        Dot = 46,
        NumPadSlash = 47,
        
        Num0 = 48,
        Num1 = 49,
        Num2 = 50,
        Num3 = 51,
        Num4 = 52,
        Num5 = 53,
        Num6 = 54,
        Num7 = 55,
        Num8 = 56,
        Num9 = 57,
        
        Home = 80,
        End = 87,
        
        Slash = 92,
        
        A = 97,
        B = 98,
        C = 99,
        D = 100,
        E = 101,
        F = 102,
        G = 103,
        H = 104,
        I = 105,
        J = 106,
        K = 107,
        L = 108,
        M = 109,
        N = 110,
        O = 111,
        P = 112,
        Q = 113,
        R = 114,
        S = 115,
        T = 116,
        U = 117,
        V = 118,
        W = 119,
        X = 120,
        Y = 121,
        Z = 122,
        
        NumLock = 127,
        
        F1 = 190,
        F2 = 191,
        F3 = 192,
        F4 = 193,
        F5 = 194,
        F6 = 195,
        F7 = 196,
        F8 = 197,
        F9 = 198,
        F10 = 199,
        F11 = 200,
        F12 = 201,
        
        LeftShit = 225,
        RightShift = 226,
        
        CapsLock = 229,
        
        LeftAlt = 233,
        RightAlt = 234, // Assumed. For some reason it causes a segfault
        LeftMeta = 235, // AKA Windows Key
        RightMeta = 236,
        Cancel = 255,
    };
    
    /**
     * Make a \c KeyCode from the return value of cv::waitKey. It is important that it use an
     * unsigned type.
     *
     * @param key The raw keycode
     * @return A typed KeyCode
     */
    inline KeyCode make_key(unsigned int key) {
        return static_cast<KeyCode>(key);
    }
    
    /**
     * A wrapper around the cv::waitKey function that return a strongly typed dvrk::KeyCode.
     *
     * @param millis A number of milliseconds to wait.
     * @return The KeyCode that was pressed
     */
    inline KeyCode waitKey(int millis) {
        return make_key(cv::waitKey(millis));
    }

    void toggleFullScreen(const std::string& window_name);
    
    
    /**
     * Draw a cross on an image at the specified position.
     *
     * @param image The image on which to draw
     * @param position The position at which to place the centre of the cross
     * @param size The size of the cross in pixels
     * @param colour The colour of the cross
    */
    void draw_cross(cv::Mat& image, cv::Point position, int size, cv::Scalar colour);
    
    /**
     * Draw a cross on an image at the specified position.
     *
     * @param image The image on which to draw
     * @param position The position at which to place the centre of the cross
     * @param size The size of the cross in pixels
     * @param colour The colour of the cross
     * @param width The width of the lines (rounded to closest odd number)
     * @param colour The colour of the cross
     */
    void draw_cross(cv::Mat& image, cv::Point position, int size, int width, cv::Scalar colour);
}
