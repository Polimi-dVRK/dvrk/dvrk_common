//
// Created by luca18 on 9/26/17.
//

/**
 * This module implements a function useful for extracting environment parameters.
 */

#pragma once

#include <boost/optional/optional.hpp>

namespace dvrk
{
    boost::optional<std::string> getenv(const char* key);
}

