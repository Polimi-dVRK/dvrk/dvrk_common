
/**
 * \file
 *
 * This module implements string conversion functions that allow us to convert pretty much anything to a std::string..
 */

#pragma once

#include <sstream>
#include <bitset>

namespace dvrk
{

/**
 * Generic implementation of \c to_string compatible with any type that has an \c operator<< function defined
 *
 * @tparam T The type of object to serialize. The compiler should be able to guess this.
 *
 * @param obj The object to serialise
 * @return The string representation of the object.
 */
template<class T>
std::string to_string(const T &obj)
{
    std::ostringstream oss;
    oss << obj;

    return oss.str();
}

/**
 * Serialize a \c std::vetor<T> to string as long as an implementation of \c operator<< exists for the type \c T.
 *
 * @tparam T The type of elements contained in the vector.
 * @param os The output stream to which the serialised result shoudl be written to.
 * @param vec The vector to serialise.
 * @return The original output stream.
 */
template <class T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &vec) {
    os << "[ ";

    for (size_t i = 0; i < vec.size(); i++) {
        os << vec[i];
        if (i != vec.size() - 1)
            os << ", ";
    }

    os << " ]";
    return os;
}

inline std::string as_bit_string(std::vector<uint8_t> data) {
    std::ostringstream oss;
    
    for (auto byte : data) { oss << std::bitset<8>(byte) << " "; }
    return oss.str();
}

}
