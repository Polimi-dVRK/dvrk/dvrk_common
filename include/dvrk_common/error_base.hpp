//
// Created by tibo on 12/07/17.
//

#pragma once

#include <boost/exception/all.hpp>

namespace dvrk
{

namespace error
{

/**
* Base class for all exception thrown by the library and client applications.
*/
    struct ErrorBase: virtual boost::exception,
                      virtual std::exception
    {
    };

    /// Some context about the error that occurred
    typedef boost::error_info<struct tag_errmsg, const std::string> errmsg;

    struct RuntimeError: virtual ErrorBase
    {};
    
    /// Used to signal an error during the intialisation phase of a resource. E.g. if a constructor fails.
    struct InitialisationError: virtual ErrorBase
    {
    };

    /// Used to signal that an operation does not make sense
    struct LogicError: virtual ErrorBase
    {
    };

    /// Used to signal that two vector or list-like objects that should have the same size have differents sizes
    struct SizeMismatch: virtual ErrorBase
    {
    };

    /// Used to provide context to a #SizeMismatch. This is the expected size of the vector or list.
    typedef boost::error_info<struct tag_expected_size, size_t> expected_size;

    /// Used to provide context to a #SizeMismatch. This is the actual size of the vector or list.
    typedef boost::error_info<struct tag_actual_size, size_t> actual_size;

    struct FileOpenError: virtual ErrorBase
    {
    };
    struct FileWriteError: virtual ErrorBase
    {
    };

    typedef boost::error_info<struct tag_filename, std::string> filename;

    /// Used to signal that an attempt was made to use a resource before it was completely initialised
    class UninitialisedUseError: public ErrorBase
    {
    };
    
    class KeyError: public ErrorBase {};
    typedef boost::error_info<struct tag_errkey, std::string> errkey;

} // end namespace dvrk::error

} // end namespace dvrk
