//
// Created by tibo on 06/09/17.
//

#pragma once


#include <memory>

#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <image_transport/image_transport.h>

#include "dvrk_common/opencv/image/modified_census_transform.hpp"

namespace dvrk
{
    /**
 * The ModifiedCensusTransformNodelet provides a Nodelet that computes the modified census transform of an image.
 *
 * The images are obtained from the topic `image_mono` which should provides 8 bit grayscale images. The resulting
 * images are published on the topic `modified_census`.
 */
    class ModifiedCensusTransformNodelet: public nodelet::Nodelet
    {
    public:
        
        void onInit() override;
        
        void onConnect();
        
        void onImage(const sensor_msgs::ImageConstPtr &img);
    
    private:
        
        std::shared_ptr <image_transport::ImageTransport> _it;
        image_transport::Subscriber _subscriber;
        image_transport::Publisher _publisher;
        
        std::shared_ptr <dvrk::ModifiedCensusTransform> _mct;
        
        /// Length of the message queue of the image subscriber, exposed as the parameter "queue_size"
        int _queue_size = 0;
        
        /// The size of the window ued for the Modified Census Transform. The Window will always be square.
        int _window_size = 0;
    };
    
}
