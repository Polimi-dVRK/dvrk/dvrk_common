//
// Created by tibo on 08/09/17.
//

#pragma once

#include <memory>

#include <nodelet/nodelet.h>
#include <image_transport/image_transport.h>

#include "dvrk_common/ros/camera/stereo_camera.hpp"
#include "dvrk_common/opencv/image/hamming_distance_image.hpp"

namespace dvrk
{
    /**
     * The HammingDistanceNodelet provides a Nodelet that computes the hamming distance between two input images.
     *
     * The images are obtained from the topics `image_0` and `image_1` which should be appropriately remapped by the
     * caller. The resulting images are published on the topic `hamming_distance`.
     */
    class HammingDistanceNodelet: public nodelet::Nodelet
    {
        // An ApproximatTime policy used to synchronise two image topics
        typedef message_filters::sync_policies::ApproximateTime<
            sensor_msgs::Image, sensor_msgs::Image
        > ApproximateTimePolicy;
        
        typedef message_filters::Synchronizer<ApproximateTimePolicy> ApproximateTimeSynchroniser;

    public:
        
        void onInit() override;
        
        void onConnect();
        
        void onImage(
            const sensor_msgs::ImageConstPtr &image0,
            const sensor_msgs::ImageConstPtr &image1
        );
    
    private:
        
        std::unique_ptr<image_transport::ImageTransport> _it;
        
        image_transport::SubscriberFilter _first, _second;
        std::unique_ptr<ApproximateTimeSynchroniser> _synchroniser;
        
        image_transport::Publisher _publisher;
        
        HammingDistanceImage _hamming;
        
        /// Length of the message queue of the image subscriber, exposed as the parameter "queue_size"
        int _queue_size = 5;
    };
    
}
