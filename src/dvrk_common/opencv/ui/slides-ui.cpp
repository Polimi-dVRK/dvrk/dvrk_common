//
// Created by tibo on 24/08/17.
//

#include <cmath>

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/highgui/highgui_c.h> // For cvGetWindowHandle
#include <opencv2/imgcodecs.hpp>
#include <dvrk_common/error_base.hpp>

#include "dvrk_common/opencv/ui/slides-ui.hpp"

using namespace dvrk::ui;

Window::~Window()
{
    if (cvGetWindowHandle(_name.c_str()))
        cv::destroyWindow(_name);
}

/**
 * Add an object to the window. Elements are stored in the order they were added and drawn from top to bottom, i.e. the
 * first elment added will always be shown above the second element added.
 *
 * @param child The element to add to the window.
 */
void Window::addChild(UIElement* child)
{
    assert(child != nullptr);
    
    _relayout_required = true;
    _children.emplace_back(child);
}


/**
 * Compute the layout of all the children. This computes the position and size of each individual child element so that
 * they can be rendered.
 *
 * @param max_rect
 */
void Window::layout(const cv::Rect2i &max_rect)
{
    // The image should extend to encompass all the content vertically so we discard the height part of the Rect.
    cv::Rect2i available_space = max_rect;
    
    for (auto &child : _children) {
        child->layout(available_space);
        available_space.y += child->getRect().height + UIElement::Padding;
    }
    
    _rect = cv::Rect2i(max_rect.tl(), cv::Size(max_rect.width, available_space.y));
    _relayout_required = false;
}


// @note: this function probably doesn't work
void Window::render(cv::Mat& window) {
    if (_relayout_required)
        layout(cv::Rect(cv::Point(0, 0), window.size()));
    
    for (auto& child: _children) {
        child->render(window);
    }
}


/**
 * Render the window to a new buffer. This will replace the buffer that the window is drawn to each time it is called
 * which may cause significant slow downs if you attempt to use the class for an interactive UI.
 * This function will not show the window. It can be shown with Window::show().
 */
void Window::render() {
    layout(cv::Rect2i(0, 0, _width, 0));
    _paint_buffer = cv::Mat(_rect.size(), CV_8UC3, _background_colour);
    
    render(_paint_buffer);
}


void Window::show()
{
    cv::imshow(_name, _paint_buffer);
    cv::waitKey(1);
}


void Window::save(std::string filename)
{
    if (_paint_buffer.empty()) {
        BOOST_THROW_EXCEPTION(error::UninitialisedUseError()
            << error::errmsg("Cannot save the window without first rendering it")
        );
    }
    
    if (!cv::imwrite(filename, _paint_buffer)) {
        BOOST_THROW_EXCEPTION(error::FileWriteError()
            << error::filename(filename)
        );
    }
}


Text::Text(const std::string &text)
{
    setText(text);
}


Text::Text(const std::vector<std::string> &text)
{
    setText(text);
}


Text* Text::setText(const std::string &text)
{
    _text_lines = std::vector<Text::TextLine>{ {text, cv::Rect2i()} };
    return this;
}


Text* Text::setText(const std::vector<std::string> &text)
{
    _text_lines.clear();
    for (const auto& line : text) {
        _text_lines.push_back({ line, cv::Rect2i() });
    }
    
    return this;
}


void Text::layout(const cv::Rect2i &max_rect)
{
    if (_text_lines.empty()) {
        _rect = max_rect;
        _rect.height = 0;
    }
    
    cv::Rect2i total_area = max_rect;
    total_area.height = UIElement::Padding;
    
    // Compute the bounding box and position of each line
    for (Text::TextLine& line: _text_lines) {
        const auto line_sz = cv::getTextSize(line.text, _text_font, _text_scale, _line_thickness, nullptr);
        
        switch(_alignment) {
            case Alignment::Left:
                line.rect.x = max_rect.x + UIElement::Padding;
                break;
            
            case Alignment::Centre:
                line.rect.x = (total_area.width - line_sz.width) / 2;
                break;
            
            case Alignment::Right:
                line.rect.x = (max_rect.width - (line_sz.width + UIElement::Padding));
                break;
        }
        
        line.rect.y = total_area.y + line_sz.height;
        line.rect.width = line_sz.width;
        line.rect.height = line_sz.height;
        
        total_area.height += line_sz.height + _line_sep;
    }
    
    _rect = total_area;
}


void Text::render(cv::Mat &window)
{
    for(const Text::TextLine& line : _text_lines) {
        const cv::Point2i line_pos(line.rect.x, line.rect.y + line.rect.height);
        cv::putText(window, line.text, line_pos, _text_font, _text_scale, _text_color, _line_thickness);
    }
}


Text *Text::setFont(const int text_font)
{
    _text_font = text_font;
    return this;
};


Text *Text::setScale(const float text_scale)
{
    _text_scale = text_scale;
    return this;
};


Text *Text::setLineThickness(const int line_thickness)
{
    _line_thickness = line_thickness;
    return this;
};


Text *Text::setColor(const cv::Scalar &text_color)
{
    _text_color = text_color;
    return this;
};


Text *Text::setLineSpacing(const float line_sep)
{
    _line_sep = line_sep;
    return this;
}


Text *Text::setAlignment(const Alignment alignment)
{
    _alignment = alignment;
    return this;
}


Text *Text::setScale(const Text::Size text_scale)
{
    switch(text_scale) {
        case Text::Size::ExtraSmall:
            _text_scale = 0.25f;
            break;
            
        case Text::Size::Small:
            _text_scale = .375f;
            break;
            
        case Text::Size::Normal:
            _text_scale = .5f;
            break;
            
        case Text::Size::Big:
            _text_scale = 0.675;
            break;
            
        case Text::Size::ExtraBig:
            _text_scale = 0.75;
            break;
    
        case Text::Size::Huge:
            _text_scale = 1.0f;
            break;
    
        case Text::Size::ExtraHuge:
            _text_scale = 1.5f;
            break;
    }
    
    return this;
};


Image::Image(const cv::Mat &image, const std::string &legend)
{
    setImage(image, legend);
}


Image::Image(const cv::Mat &image, const Text &legend)
{
    setImage(image, legend);
}


Image *Image::setImage(const cv::Mat &image, const std::string &legend)
{
    setImage(image, Text(legend));
    return this;
}


Image *Image::setImage(const cv::Mat &image, const Text &legend)
{
    _image = image.clone();
    _legend = legend;
    
    return this;
}


void Image::layout(const cv::Rect2i &max_rect)
{
    const unsigned int usable_width = max_rect.width - 2 * UIElement::Padding;
    float image_scale = static_cast<float>(usable_width) / _image.cols;
    
    _image_rect = cv::Rect2i(
        max_rect.x + UIElement::Padding,
        max_rect.y + UIElement::Padding,
        usable_width,
        std::ceil(_image.rows * image_scale)
    );
    
    // Now that we know the position of the image we can position the legend:
    const cv::Point2i legend_tl(
        max_rect.x,
        _image_rect.y + _image_rect.height
    );
    _legend.layout(cv::Rect2i(legend_tl, cv::Size2i(max_rect.width, 0)));
    
    // Finally we can update our own bounding box
    const cv::Rect2i legend_rect = _legend.getRect();
    _rect = cv::Rect2i(
        max_rect.x,
        max_rect.y,
        max_rect.width,
        _image_rect.height + legend_rect.height + 2*UIElement::Padding
    );
}


void Image::render(cv::Mat &window)
{
    cv::Mat drawable_image = _image;
    
    if (_image.size() != _image_rect.size()) {
        const double scale = _image_rect.width / _image.size().width;
        const auto interpolation = scale > 1 ? cv::INTER_NEAREST : cv::INTER_AREA;
    
        cv::resize(_image, drawable_image, _image_rect.size(), 0, 0, interpolation);
    }
    
    assert(drawable_image.channels() <= 3);
    if (drawable_image.channels() == 1) {
        cv::cvtColor(drawable_image, drawable_image, cv::COLOR_GRAY2BGR);
    } else if (drawable_image.channels() == 2) {
        // We want to make a 3 channel image that we can display from the 2 channel image
        std::vector<cv::Mat> channels;
        cv::split(drawable_image, channels);
        
        cv::Mat empty_channel = cv::Mat::zeros(drawable_image.size(), CV_MAKE_TYPE(drawable_image.depth(), 1));
        channels.push_back(empty_channel);
        
        cv::merge(channels, drawable_image);
    }
    
    assert(drawable_image.channels() == 3);
    drawable_image.copyTo(window(_image_rect));
    
    _legend.render(window);
}


ImageGrid *ImageGrid::addImage(const cv::Mat &image, const std::string &legend)
{
    _relayout_required = true;
    _images.emplace_back(image, legend);
    return this;
}


ImageGrid *ImageGrid::addImage(const Image &image)
{
    _relayout_required = true;
    _images.push_back(image);
    return this;
}


ImageGrid *ImageGrid::addImage(const Image *image)
{
    _relayout_required = true;
    _images.push_back(*image);
    return this;
}


ImageGrid *ImageGrid::setImagesPerLine(const unsigned int images_per_line)
{
    _images_per_line = images_per_line;
    return this;
}


void ImageGrid::layout(const cv::Rect2i &max_rect)
{
    if (_images.empty()) return;
    
    // We start by laying out all of the images as if they were all at (max_rect.x, max_rect.y) in order to compute
    // their size. We can then use that to locate the tallest image on each line to derive the height of each line.
    
  
    const unsigned int width_per_image = max_rect.width / _images_per_line;
    const size_t rows = _images.size() / _images_per_line + 1;
    
    int current_y_offset = 0, next_y_offset = max_rect.y;
    for (size_t row = 0; row < rows; row++) {
        current_y_offset = next_y_offset;
        
        for (size_t col = 0; col < _images_per_line; col++) {
            const size_t idx = (row * _images_per_line) + col;
            // Since the last row may not be full we need to make sure that there actually is an image to retrieve
            if (idx >= _images.size()) break;
            
            auto& image = _images.at(idx);
            
            image.layout(cv::Rect(
                max_rect.x + width_per_image * col,
                max_rect.y + current_y_offset,
                width_per_image,
                max_rect.height
            ));
            
            const auto image_layout = image.getRect();
            next_y_offset = std::max(next_y_offset, image_layout.y + image_layout.height);
        }
        
        // We have reached the end of the line, reset the counters

    }
    
    _rect = cv::Rect2i(max_rect.x, max_rect.y, max_rect.width, next_y_offset);
    _relayout_required = false;
}


void ImageGrid::render(cv::Mat &window)
{
    assert(!_relayout_required);
    for (auto& image : _images) {
        image.render(window);
    }
}


