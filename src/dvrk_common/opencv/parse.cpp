//
// Created by tibo on 12/07/17.
//

#include <algorithm>
#include <regex>

#include "dvrk_common/opencv/parse.hpp"

/**
 * Extract the definition of a cv::Rect from a string. The string should be formatted as:
 *  x_offset, y_offset, width, height
 *
 * @todo Support floating point coordinates or return a \c cv::Rect2i to make it clear the it doesn't handle floats
 *
 * @param rect_str The string from which to attempt to parse the \c cv::Rect
 * @return An optional type containing the \c cv::Rect if it could be passed, empty otherwise.
 */
boost::optional<cv::Rect> dvrk::parseRect(std::string rect_str) {
    /*
     * The regex used to parse the rect is basically a succession of "\s*(\d*)\s*" blocks:
     * - \s* 0 or more whitespace charactes
     * - (\d+) 1 or more digits
     * - \s* 0 more whitespace characters
     *
     * This will match strings of the type : "10, 10, 100, 100" containing positive integers. Negative numbers or
     * floats will not parse.
     */
    const std::string re_pattern = R"(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+))";

    const std::regex re(re_pattern);
    std::smatch matches;
    if (std::regex_search(rect_str, matches, re)) {
        try {
            std::vector <uint32_t> rect;

            // Note: The firs match contains the entire matched string. Since we are interested in the individual
            // captures we should skip it.
            std::transform(
                ++matches.cbegin(), matches.cend(),
                std::back_inserter(rect),
                [](auto el)
                { return std::stoi(el.str()); }
            );

            return cv::Rect(rect.at(0), rect.at(1), rect.at(2), rect.at(3));
        }
        catch (std::invalid_argument) {
            // One of the sub matches could not be converted to an int
            return boost::none;
        }
    }

    return boost::none;
}

/**
 * Extract the definition of a cv::Point from a string. The string should be formatted as: x, y. The parsing logic is
 * smart enough to handle extra whitespace in the string.
 *
 * @todo Support floating point coordinates or return a \c cv::Point2i to make it clear the it doesn't handle floats
 *
 * @param point_str The string from which to attempt to parse the \c cv::Point
 * @return An optional type containing the \c cv::Point if it could be parsed, empty otherwise.
 */
boost::optional<cv::Point> dvrk::parsePoint(std::string point_str) {
    /*
     * The regex used to parse the point is basically a succession of "\s*(\d*)\s*" blocks:
     * - \s* 0 or more whitespace charactes
     * - (-?\d+) an optional '-' signo followed by 1 or more digits
     * - \s* 0 more whitespace characters
     *
     * This will match strings of the type : "10, 10" containing integers. Floats will not parse.
     */
    const std::string re_pattern = R"(\s*(-?\d+)\s*,\s*(-?\d+)\s*)";

    const std::regex re(re_pattern);
    std::smatch matches;
    if (std::regex_search(point_str, matches, re)) {
        try {
            std::vector <uint32_t> point;

            // Note: The firs match contains the entire matched string. Since we are interested in the individual
            // captures we should skip it.
            std::transform(
                ++matches.cbegin(), matches.cend(),
                std::back_inserter(point),
                [](auto el)
                { return std::stoi(el.str()); }
            );

            return cv::Point(point.at(0), point.at(1));
        }
        catch (std::invalid_argument) {
            // One of the sub matches could not be converted to an int
            return boost::none;
        }
    }

    return boost::none;
}


/**
 * Extract the definition of a cv::Size from a string. The string should be formatted as: WxH. The parsing logic is
 * smart enough to handle extra whitespace in the string.
 *
 * @param size_str The string from which to attempt to parse the \c cv::Size
 * @return An optional type containing the \c cv::Size if it could be parsed, empty otherwise.
 */
boost::optional<cv::Size> dvrk::parseSize(std::string size_str){
    /*
     * The regex used to parse the point is basically a succession of "\s*(\d*)\s*" blocks:
     * - \s* 0 or more whitespace charactes
     * - (\d+) 1 or more digits
     * - \s* 0 more whitespace characters
     *
     * This will match strings of the type : "10 x 10" containing integers. Floats and negative numbers will not parse.
     */
    const std::string re_pattern = R"(\s*(\d+)\s*x\s*(\d+)\s*)";

    const std::regex re(re_pattern);
    std::smatch matches;
    if (std::regex_search(size_str, matches, re)) {
        try {
            std::vector <uint32_t> size;

            // Note: The firs match contains the entire matched string. Since we are interested in the individual
            // captures we should skip it.
            std::transform(
                ++matches.cbegin(), matches.cend(),
                std::back_inserter(size),
                [](auto el)
                { return std::stoi(el.str()); }
            );

            return cv::Size(size.at(0), size.at(1));
        }
        catch (std::invalid_argument) {
            // One of the sub matches could not be converted to an int
            return boost::none;
        }
    }

    return boost::none;
}
