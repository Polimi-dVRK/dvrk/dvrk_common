
#include <string>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "dvrk_common/error_base.hpp"
#include "dvrk_common/opencv/highgui.hpp"

using namespace std::string_literals;

namespace dvrk {
    
    void toggleFullScreen(const std::string& window_name) {
        // For some reason opencv returns enum values as `double`. We must cast it back manually
        auto window_shape = static_cast<cv::WindowFlags>(
            cv::getWindowProperty(window_name, cv::WND_PROP_FULLSCREEN));
        
        switch (window_shape) {
            case cv::WINDOW_FULLSCREEN:
                cv::setWindowProperty(window_name,
                    cv::WND_PROP_FULLSCREEN,
                    cv::WINDOW_NORMAL);
                break;
            
            case cv::WINDOW_NORMAL:
                cv::setWindowProperty(window_name,
                    cv::WND_PROP_FULLSCREEN,
                    cv::WINDOW_FULLSCREEN);
                break;
            
            default:BOOST_THROW_EXCEPTION(error::RuntimeError()
                    << error::errmsg("Unspported cv::WND_PROP_FULLSCREEN value: "s
                        + std::to_string(window_shape)));
        }
    }
    
    
    void draw_cross(cv::Mat& image, cv::Point position, int size, cv::Scalar colour) {
        assert(position.x > 0 && position.y > 0);
        
        cv::Point p1( std::max(position.x - size / 2, 0), position.y );
        cv::Point p2( std::min(position.x + size / 2, image.cols), position.y );
        cv::line(image, p1, p2, colour);
        
        cv::Point p3( position.x, std::max(position.y - size / 2, 0) );
        cv::Point p4( position.x, std::min(position.y + size / 2, image.rows) );
        cv::line(image, p3, p4, colour);
    }


    void draw_cross(cv::Mat& image, cv::Point position, int size, int width, cv::Scalar colour) {
        assert(position.x > 0 && position.y > 0);
        
        int half_width = static_cast<int>( std::ceil(width / 2.) );
        
        for (int offset = -half_width; offset < half_width; ++offset) {
            cv::Point p1( std::max(position.x - size / 2, 0), position.y + offset );
            cv::Point p2( std::min(position.x + size / 2, image.cols), position.y + offset );
            cv::line(image, p1, p2, colour);
        
            cv::Point p3( position.x + offset, std::max(position.y - size / 2, 0) );
            cv::Point p4( position.x + offset, std::min(position.y + size / 2, image.rows) );
            cv::line(image, p3, p4, colour);
        }
    }

    
}
