//
// Created by tibo on 02/08/17.
//

#include <opencv2/imgproc.hpp>
#include "dvrk_common/opencv/image/specular_reflections_mask.hpp"

using namespace dvrk;

cv::Mat dvrk::create_specular_reflections_mask(const cv::Mat &src)
{
    cv::Mat mask;
    create_specular_reflections_mask(src, mask);

    return mask;
}


void ::dvrk::create_specular_reflections_mask(const cv::Mat &src, cv::Mat &mask)
{
    assert(src.type() == CV_8UC3);

    // The algorithm operates in HSV colour space based on the observation that specular reflection can be characterised
    // as areas of high brightness but low saturation
    cv::Mat hsv_img;
    cv::cvtColor(src, hsv_img, CV_RGB2HSV);

    // Extract the color channels in order to operate on them independently
    std::vector<cv::Mat> hsv_channels(3);
    cv::split(hsv_img, hsv_channels);

    // Apply thresholding along the S and V channels of the image. The thresholds are computed as such that we retain
    // only pixels where:
    //  - S < .35 * S_max
    //  - V > .70 * V_max

    double s_max, v_max;
    cv::minMaxLoc(hsv_channels[1], nullptr, &s_max);
    assert(s_max <= std::numeric_limits<unsigned char>::max());

    cv::minMaxLoc(hsv_channels[2], nullptr, &v_max);
    assert(v_max <= std::numeric_limits<unsigned char>::max());

    auto s_threshold = std::round(0.35 * static_cast<unsigned char>(s_max));
    auto v_threshold = std::round(0.7 * static_cast<unsigned char>(v_max));

    cv::Mat s_mask, v_mask;
    cv::threshold(hsv_channels[1], s_mask, s_threshold, 255, cv::ThresholdTypes::THRESH_BINARY_INV);
    cv::threshold(hsv_channels[2], v_mask, v_threshold, 255, cv::ThresholdTypes::THRESH_BINARY);

    // Compute a mask combining both the S and V masks
    cv::Mat sv_mask;
    cv::bitwise_and(s_mask, v_mask, sv_mask);

    // Find the initial_contours of the masked areas, we use these to compute the surface area of each detection
    std::vector<std::vector< cv::Point >> contours;
    cv::findContours(
        sv_mask, contours, cv::RetrievalModes::RETR_EXTERNAL, cv::ContourApproximationModes::CHAIN_APPROX_NONE);

    cv::Mat large_contours_mask = cv::Mat::zeros(src.size(), CV_8UC1);
    cv::Mat small_contours_mask = cv::Mat::zeros(src.size(), CV_8UC1);

    for (size_t i = 0; i < contours.size(); i++) {
        const auto& contour = contours[i];

        const auto surface_area = cv::contourArea(contour, false);
        if (surface_area > 50 && surface_area < 1000) {
            cv::drawContours(large_contours_mask, contours, i, cv::Scalar(255, 255, 255), cv::LineTypes::FILLED);
        } else if (surface_area <= 50) {
            cv::drawContours(small_contours_mask, contours, i, cv::Scalar(255, 255, 255), cv::LineTypes::FILLED);
        }
    }

    cv::Mat small_struc_elem = cv::getStructuringElement(cv::MorphShapes::MORPH_ELLIPSE, cv::Size(3, 3));
    cv::dilate(large_contours_mask, large_contours_mask, small_struc_elem, cv::Point(-1, -1), 3);

    cv::Mat large_struc_elem = cv::getStructuringElement(cv::MorphShapes::MORPH_ELLIPSE, cv::Size(5, 5));
    cv::dilate(small_contours_mask, small_contours_mask, large_struc_elem, cv::Point(-1, -1), 3);

    // The final mask is the combination of the small and large masks
    cv::bitwise_or(large_contours_mask, small_contours_mask, mask);
}
