//
// Created by tibo on 03/08/17.
//

#include <cmath>

#include "dvrk_common/ros/camera/camera_errors.hpp"
#include "dvrk_common/opencv/image/modified_census_transform.hpp"

using namespace dvrk;

ModifiedCensusTransform::ModifiedCensusTransform(const cv::Size &window_size)
    : _window_size(window_size)
{
    if (window_size.width % 2 == 0 || window_size.height % 2 == 0) {
        BOOST_THROW_EXCEPTION(error::ErrorBase()
            << error::errmsg("Even sized windows are not supported")
        );
    }

    // Compute the required size of the destination. For each pixel in the window we need 2bits but since the filter
    // skips the central pixel of each support frame we need 2 less bits.
    //
    // This causes an issue when the window size is 1 since it that case we would have 0 bits per pixel. In practice we
    // should disallow this since the operation would be a noop but it is useful for testing since the next smallest
    // possible value 3x3 already results in a multi-byte byte per pixel value.
    const int bits_per_pixel = (_window_size.width * _window_size.height) * 2 - 2;
    _bytes_per_pixel = static_cast<size_t>(std::max(1.0, std::ceil(bits_per_pixel / 8.0)));

}


const cv::Mat ModifiedCensusTransform::apply(const cv::Mat &src)
{
    assert(src.depth() == CV_8U && !src.empty());
    
    // Make sure to reset the data store each time we process a new image
    _data = cv::Mat::zeros(src.size(), CV_8UC(_bytes_per_pixel));

    // Allocate the vector used to store the census data for a single pixel and re-use it each time
    std::vector<uint8_t> px_census(_bytes_per_pixel);

    // Iterate over the entire source image without touching the border.
    const cv::Size sz = src.size();
    const cv::Size border = _window_size / 2;
    for (auto i = border.width; i < std::max(0, sz.width - border.width); i++) {
        for (auto j = border.height; j < std::max(0, sz.height - border.height); j++) {
            // Create the support mask for the current pixel
            cv::Rect window_roi({i - border.width, j - border.height}, _window_size);
            cv::Mat window(src, window_roi);
            
            px_census.clear();
            processPixel(window, px_census);
            setPixelAt({i, j}, px_census);
        }
    }

    return _data;
}

void ModifiedCensusTransform::processPixel(const cv::Mat &window, std::vector<uint8_t> &result)
{
    // The mean is cast to integer since we will be comparing it with the pixel values that are integers themselves
    const auto window_mean = static_cast<uint8_t>(cv::mean(window)[0]);

    const cv::Point2i centre_point(
        static_cast<int>(std::floor(window.rows / 2.0)),
        static_cast<int>(std::floor(window.cols / 2.0))
    );
    const uint8_t centre_pixel = window.at<uint8_t>(centre_point);

    uint8_t min, max;
    std::tie(min, max) = std::minmax(centre_pixel, window_mean);
    
    size_t px_count = 0;
    uint8_t current_byte = 0b00000000;
    for (auto row = 0; row < window.rows; row++) {
        for (auto col = 0; col < window.cols; col++) {
            if (col == centre_point.x && row == centre_point.y) // Skip the centre pixel
                continue;
    
            px_count += 1;
            const uint8_t this_pixel = window.at<uint8_t>(row, col);

            if (this_pixel <= min) {
                current_byte |= 0b00000000;
            } else if (this_pixel < centre_pixel && this_pixel > min) {
                current_byte |= 0b00000001;
            } else if (this_pixel > centre_pixel && this_pixel < max) {
                current_byte |= 0b00000010;
            } else if (this_pixel >= max) {
                current_byte |= 0b00000011;
            }

            if (px_count % 4 == 0) {
                result.push_back(current_byte);
                current_byte = 0b00000000;
            } else {
                current_byte <<= 2;
            }
        } // end for col
    } // end for row

    if (px_count % 4 != 0) {
        result.push_back(current_byte);
    }
}

void ModifiedCensusTransform::setPixelAt(const cv::Point &pos, const std::vector<uint8_t> &data)
{
    setPixelAt(pos.y, pos.x, data);
}

void ModifiedCensusTransform::setPixelAt(const int row, const int col, const std::vector<uint8_t> &new_data)
{
    if (new_data.size() != _bytes_per_pixel) {
        BOOST_THROW_EXCEPTION(error::SizeMismatch()
            << error::errmsg("The pixel data buffer has an unexpected size")
            << error::expected_size(_bytes_per_pixel)
            << error::actual_size(new_data.size())
        );
    }

    std::copy(new_data.begin(), new_data.end(), _data.ptr<uint8_t>(row, col));
}

std::vector<uint8_t> ModifiedCensusTransform::getPixelAt(const cv::Point &pos) const
{
    return getPixelAt(pos.y, pos.x);
}


std::vector<uint8_t> ModifiedCensusTransform::getPixelAt(const int row, const int col) const
{
    const uint8_t* src_start = _data.ptr<uint8_t>(row, col);
    const uint8_t* src_end = src_start + _data.elemSize();

    std::vector<uint8_t> pixel_data(_bytes_per_pixel);
    std::copy(src_start, src_end, pixel_data.begin());

    return pixel_data;
}
