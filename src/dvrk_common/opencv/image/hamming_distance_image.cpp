//
// Created by tibo on 07/09/17.
//

#include "dvrk_common/opencv/image/hamming_distance_image.hpp"

using namespace dvrk;

/** Compute the bitwise hamming distance between two images.
 *
 * The Hamming distance is a measure of the distance between two objects (see
 * [wikipedia](https://en.wikipedia.org/wiki/Hamming_distance) for more details).
 *
 * In the context of image processing the hamming distance is understood to be the difference between two pixels taken
 * at the same position in two images. Specifically, it is the number of bits that differ in these two pixels.This
 * bitwise hamming distance can be performed quite simply by xoring the two pixels and counting the number of 1s in the
 * result. Both of these operations can be performed extremely fast on modern hardware.
 *
 * @param lhs The first image to compare
 * @param rhs The second image to compare
 * @return
 *  A CV_16U matrix where each pixel contains the number of bit differences between the pixels at the same position of
 *  \c lhs and \rhs.
 */
const cv::Mat& HammingDistanceImage::apply(const cv::Mat &lhs, const cv::Mat &rhs)
{
    // TODO; Understand images with more than 1 channel and more tan 8bits per channel
    // TODO: Smart packing in __builtin_popcount
    // TODO: XOR the two images together first

    assert(lhs.depth() == CV_8U && rhs.depth() == CV_8U);

    // A CV_16U matrix should be big enough, that allows us to have 65536 bits per pixel
    if (result.size() != lhs.size())
        result = cv::Mat(lhs.size(), CV_16U);
    
    // Since we limit ourselves to 8U data the number fo channels is also the number of bytes per pixel
    const int image_channels = lhs.channels();
    
    for (int row = 0; row < lhs.rows; ++row) {
        for (int col = 0; col < lhs.cols; ++col) {
            const auto lhs_data = lhs.ptr<uint8_t>(row, col);
            const auto rhs_data = rhs.ptr<uint8_t>(row, col);
            
            uint16_t hamming_distance = 0;
            for (int offset = 0; offset < image_channels; ++offset) {
                // Count the number of 1s
                hamming_distance += __builtin_popcount(lhs_data[offset] ^ rhs_data[offset]);
            }
            result.at<uint16_t>(row, col) = hamming_distance;
        }
    }
    
    return result;
}
