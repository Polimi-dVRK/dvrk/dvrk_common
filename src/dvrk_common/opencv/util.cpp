//
// Created by tibo on 19/07/17.
//

#include <ros/console.h>
#include <ros/node_handle.h>
#include <opencv2/highgui.hpp>

#include "dvrk_common/opencv/util.hpp"

/**
 * Check to see if the pressed key is 'f' or 'ESC' and react accordingly. In the first case fullscreen is toggled, in
 * the second the node is killed.
 *
 * @note
 *  It is only possible to fullscreen a window that was created with \c cv::namedWindow(name, cv::WINDOW_NORMAL)
 *
 * @example
 *
 *  const std::string wname("My window");
 *  cv::namedWindow(wname, cv::WINDOW_NORMAL);
 *
 *  //  Display stuff in the window
 *
 *  auto kc = cv::waitKey(16);
 *  dvrk::opencvKeyboardHandler(wname, kc);
 *
 * @param window_name The name of the OpenCV window to operate on
 * @param keycode The keycode of the key pressed
 */
void dvrk::opencvKeyboardHandler(const std::string &window_name, const char keycode)
{
    switch (keycode) {
        case 'f': {
            // For some reason opencv returns enum values as `double`. We must cast it back manually
            auto window_shape = static_cast<cv::WindowFlags>(
                cv::getWindowProperty(window_name, cv::WND_PROP_FULLSCREEN));

            switch (window_shape) {
                case cv::WINDOW_FULLSCREEN:
                    cv::setWindowProperty(window_name, cv::WND_PROP_FULLSCREEN, cv::WINDOW_NORMAL);
                    break;

                case cv::WINDOW_NORMAL:
                    cv::setWindowProperty(window_name, cv::WND_PROP_FULLSCREEN, cv::WINDOW_FULLSCREEN);
                    break;

                default:
                    ROS_WARN_STREAM("Unspported cv::WND_PROP_FULLSCREEN value: " << window_shape);
            }
            break;
        }

        case 27: { // ESC key
            ros::shutdown();
        }
    }
}

/**
 * Check whether two matrices are identical or not.
 *
 * This fuction tries to perform as many cheap checks as possible before actually checking the contents of the two
 * matrices to make it as fast as possible. If you see any checks that I may have missed please let me know.
 *
 * @param lhs A mtrix
 * @param rhs The matrix to  comapre with \c lhs
 * @return Whether \c lhs and \rhs are equal or not. Empty matrices of the same type are considered to all be equal.
 */
bool dvrk::is_equal(const cv::Mat &lhs, const cv::Mat rhs)
{
    if (lhs.type() != rhs.type())
        return false;
    
    if (lhs.empty() && rhs.empty())
        return true;
    
    if (lhs.size() != rhs.size())
        return false;
    
    if (lhs.data == rhs.data)
        return true;
    
    if (lhs.channels() == 1) {
        cv::Mat differences;
        cv::compare(lhs, rhs, differences, CV_CMP_NE);
        return (cv::countNonZero(differences) == 0);
        
    } else {
        std::vector<cv::Mat> lhs_channels, rhs_channels;
        cv::split(lhs, lhs_channels);
        cv::split(rhs, rhs_channels);
    
        cv::Mat differences;
        for (size_t i = 0; i < lhs_channels.size(); ++i) {
            // NOTE: The cv::bitwise_xor function might be faster.
            cv::compare(lhs_channels[i], rhs_channels[i], differences, CV_CMP_NE);
            if (cv::countNonZero(differences) != 0)
                return false;
        }
        
        return true;
    }
    
    // Should be unreachable code
    return false;
}


/**
 * Convert the type of cv::Mat object to a string. The resulting string is equivalent to the CV_ macro used when
 * creating the matrix except for cases where the the number of channels is greater than 4. To create such a matrix we
 * would use for example CV_8UC(7) but this function will return 8UC7. 
 *
 * @param mat The matrix to consider
 * @return A string containing the type of the matrix
 */
std::string dvrk::matTypeToString(const cv::Mat &mat)
{
    int mat_type = mat.type();
    std::string type_str;
    
    uchar depth = mat_type & CV_MAT_DEPTH_MASK;
    uchar channels = 1 + (mat_type >> CV_CN_SHIFT);
    
    switch ( depth ) {
        case CV_8U:  type_str += "8U"; break;
        case CV_8S:  type_str += "8S"; break;
        case CV_16U: type_str += "16U"; break;
        case CV_16S: type_str += "16S"; break;
        case CV_32S: type_str += "32S"; break;
        case CV_32F: type_str += "32F"; break;
        case CV_64F: type_str += "64F"; break;
        default:     type_str += "USER"; break;
    }
    
    type_str += "C";
    type_str += std::to_string(channels);
    
    return type_str;

}
