//
// Created by luca18 on 9/26/17.
//

#include <cstdlib>
#include "dvrk_common/system.hpp"


/**
 * Attempt to retrieve the specified environment variable.
 *
 * @param key The name of the enviroment variable to retrieve
 * @return An optional type containing the variable if it exists, empty otherwise.
 */

boost::optional<std::string> dvrk::getenv(const char* key)
{
    if (const char* env_var = std::getenv(key)) {
        return { std::string(env_var) };
    }

    return {};
}