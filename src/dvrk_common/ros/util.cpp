//
// Created by luca18 on 9/26/17.
//

#include <boost/filesystem.hpp>

#include "dvrk_common/system.hpp"
#include "dvrk_common/ros/util.hpp"
#include "dvrk_common/error_base.hpp"

/**
 * Get the ROS_HOME path.
 *
 * The ROS_HOME path by default is ~/.ros but it can be overridden by the ${ROS_HOME} enviroment variable.
 *
 * @note This function is POSIX-specific. This won't work on windows since the ${HOME} variables will be unset. When
 * (if ?) ROS2 ever becomes a thing we might to revisit this choice and actually consider the platform that the lib
 * was compiled on.
 *
 * @return The path to the ROS_HOME directory.
 */

const boost::filesystem::path dvrk::getROSHome() {
    namespace fs = boost::filesystem;
    
    if (auto ros_home = dvrk::getenv("ROS_HOME"))
        return *ros_home;

    if (auto home = dvrk::getenv("HOME"))
        return (fs::path(*home) / ".ros");
    
    BOOST_THROW_EXCEPTION(dvrk::error::RuntimeError()
        << dvrk::error::errmsg("Unable to guess ROS_HOME location, the HOME and ROS_HOME variables are both unset.")
    );
}
