//
// Created by tibo on 26/03/18.
//

#include <ros/ros.h>

#include "dvrk_common/error_base.hpp"
#include "dvrk_common/ros/ros_graph_state.hpp"

namespace dvrk {

    std::string dumpROSState() {
            dvrk::ROSGraphState ros_state;
        
        std::ostringstream oss;
        
        oss << "## ROS Network state: \n";
        oss << "  ROS_HOSTNAME = " << ros_state.getHostname() << "\n";
        oss << "  ROS_MASTER_URI = " << ros_state.getMasterURI() << "\n";
        oss << "\n";
        oss << "  Available Topics:\n";
        
        for (const auto& pair: ros_state.getAll()) {
            const dvrk::TopicInfo topic_info = pair.second;
            
            oss << "    + " << topic_info.name << " [" << topic_info.datatype << "]\n";
            
            oss << "    +---> Publishers [" << topic_info.publishers.size() << "]: \n";
            for (const auto& publisher: topic_info.publishers) {
                oss << "    |     " << publisher << "\n";
            }
            
            oss << "    +---> Subscribers [" << topic_info.subscribers.size() << "]:\n";
            for (const auto& subscriber: topic_info.subscribers) {
                oss << "    |     " << subscriber << "\n";
            }
        }
    
        return oss.str();
    }
    
    
    ROSGraphState::ROSGraphState() {
        _node_name = ros::this_node::getName();
        _ros_hostname = ros::master::getHost();
        _ros_master_uri = ros::master::getURI();
    
        std::vector<ros::master::TopicInfo> ros_topics;
        if (!ros::master::getTopics(ros_topics)) {
            ROS_ERROR( "Unable to query ROS_MASTER for registered topics");
        }
    
        for (const auto& topic: ros_topics) {
            _ros_topics[topic.name] = TopicInfo(topic.name, topic.datatype);
        }
    
        // We've got all the easy stuff. Now we use the XmlRpc to get all the juicy bits
    
        XmlRpc::XmlRpcValue request(_node_name), response, payload;
        if (!ros::master::execute("getSystemState", request, response, payload, false)) {
            return;
        }
    
        ROS_ASSERT(payload.getType() == XmlRpc::XmlRpcValue::TypeArray);
        ROS_ASSERT(payload.size() == 3);
    
        XmlRpc::XmlRpcValue publishers = payload[0], subscribers = payload[1];
    
        ROS_ASSERT(publishers.getType() == XmlRpc::XmlRpcValue::TypeArray);
        for (int topic_idx = 0; topic_idx < publishers.size(); ++topic_idx) {
            XmlRpc::XmlRpcValue& this_topic = publishers[topic_idx];
        
            ROS_ASSERT(this_topic.getType() == XmlRpc::XmlRpcValue::TypeArray);
            ROS_ASSERT(this_topic.size() == 2);
        
            ROS_ASSERT(this_topic[0].getType() == XmlRpc::XmlRpcValue::TypeString);
            TopicInfo& this_topic_info = _ros_topics.at(this_topic[0]);
        
            ROS_ASSERT(this_topic[1].getType() == XmlRpc::XmlRpcValue::TypeArray);
            XmlRpc::XmlRpcValue& topic_publishers = this_topic[1];
        
            for (int publisher_idx = 0; publisher_idx < topic_publishers.size(); ++publisher_idx) {
                ROS_ASSERT(topic_publishers[publisher_idx].getType() == XmlRpc::XmlRpcValue::TypeString);
            
                this_topic_info.publishers.emplace_back(topic_publishers[publisher_idx]);
            }
        }
    
        ROS_ASSERT(subscribers.getType() == XmlRpc::XmlRpcValue::TypeArray);
        for (int topic_idx = 0; topic_idx < subscribers.size(); ++topic_idx) {
            XmlRpc::XmlRpcValue& this_topic = subscribers[topic_idx];
        
            ROS_ASSERT(this_topic.getType() == XmlRpc::XmlRpcValue::TypeArray);
            ROS_ASSERT(this_topic.size() == 2);
        
            ROS_ASSERT(this_topic[0].getType() == XmlRpc::XmlRpcValue::TypeString);
            TopicInfo& this_topic_info = _ros_topics.at(this_topic[0]);
        
            ROS_ASSERT(this_topic[1].getType() == XmlRpc::XmlRpcValue::TypeArray);
            XmlRpc::XmlRpcValue& topic_subscribers = this_topic[1];
        
            for (int subscriber_idx = 0; subscriber_idx < topic_subscribers.size();
                 ++subscriber_idx) {
                ROS_ASSERT(topic_subscribers[subscriber_idx].getType() == XmlRpc::XmlRpcValue::TypeString);
            
                this_topic_info.subscribers.emplace_back(topic_subscribers[subscriber_idx]);
            }
        }
    }
    
    
    boost::optional<const TopicInfo&> ROSGraphState::getTopic(const std::string& topic_name) const {
        if (!_ros_topics.count(topic_name))
            BOOST_THROW_EXCEPTION(error::KeyError() << error::errkey(topic_name));
        
        return { _ros_topics.at(topic_name) };
    }
    
    
    const std::vector<std::string>& ROSGraphState::getPublishersFor(const std::string& topic_name) const {
        if (!_ros_topics.count(topic_name))
            BOOST_THROW_EXCEPTION(error::KeyError() << error::errkey(topic_name));
        
        return _ros_topics.at(topic_name).publishers;
    }
    
    
    const std::vector<std::string>& ROSGraphState::getSubscribersFor(const std::string& topic_name) const {
        if (!_ros_topics.count(topic_name))
            BOOST_THROW_EXCEPTION(error::KeyError() << error::errkey(topic_name));
    
        return _ros_topics.at(topic_name).subscribers;
    }
    
    
    std::vector<std::string> ROSGraphState::getAllTopics() const {
        std::vector<std::string> topics;
        
        for (const auto& topic: _ros_topics) {
            topics.push_back(topic.second.name);
        }
        
        return topics;
    }
    
}
