//
// Created by tibo on 19/07/17.
//

#include "dvrk_common/math.hpp"
#include "dvrk_common/ros/ext.hpp"

/// Equality operator for two \c sensor_msgs::RegionOfInterest messages
bool sensor_msgs::operator==(const RegionOfInterest& lhs, const RegionOfInterest& rhs) {
    return (lhs.x_offset == rhs.x_offset) && (lhs.y_offset == rhs.y_offset)
        && (lhs.width == rhs.width) && (lhs.height == rhs.height) && (lhs.do_rectify == rhs.do_rectify);
}

geometry_msgs::Point geometry_msgs::make_point(const double x, const double y, const double z)
{
    geometry_msgs::Point retval;
    retval.x = x;
    retval.y = y;
    retval.z = z;

    return retval;
}


geometry_msgs::Point32 geometry_msgs::make_point32(const float x, const float y, const float z)
{
    geometry_msgs::Point32 retval;
    retval.x = x;
    retval.y = y;
    retval.z = z;

    return retval;
}


bool ::geometry_msgs::operator==(const geometry_msgs::Point &lhs, const geometry_msgs::Point &rhs)
{
    return dvrk::approx_equal(lhs.x, rhs.x)
        && dvrk::approx_equal(lhs.y, rhs.y)
        && dvrk::approx_equal(lhs.z, rhs.z);
}


bool ::geometry_msgs::operator==(const geometry_msgs::Point32 &lhs, const geometry_msgs::Point32 &rhs)
{
    return dvrk::approx_equal(lhs.x, rhs.x)
        && dvrk::approx_equal(lhs.y, rhs.y)
        && dvrk::approx_equal(lhs.z, rhs.z);
}
