//
// Created by nearlab on 05/10/17.
//

#include <cv_bridge/cv_bridge.h>
#include <image_transport/camera_common.h>

#include "dvrk_common/ros/camera/camera_errors.hpp"
#include "dvrk_common/ros/camera/extended_stereo_camera.hpp"

using namespace dvrk;

/**
 * Default constructor.
 *
 * @param nh The node handle used to initialise the ImageTransport and create the required
 * subscribers.
 */
ExtendedStereoCamera::ExtendedStereoCamera(ros::NodeHandle nh)
    : _nh(nh)
    , _it(_nh)
    , _synchroniser(ApproximateTimePolicy(5))
    , _adv_checker(nh)
{
    _synchroniser.registerCallback(&ExtendedStereoCamera::onStereoImagesReceived, this);
}



/**
 * Constructor.
 *
 * Initialises the camera and subscribes to all the topics in one shot.
 *
 * @param nh The node handle used to initialise the ImageTransport and create the required
 * subscribers.
 * @param left_mono_topic The topic from which to read grayscale images for the left camera
 * @param right_mono_topic The topic from which to read grayscale images for the right camera
 * @param left_rgb_topic The topic from which to read colour images for the left camera
 * @param right_rgb_topic The topic from which to read color images for the right camera
 */
ExtendedStereoCamera::ExtendedStereoCamera(
    ros::NodeHandle nh,
    const std::string &left_mono_topic,
    const std::string &right_mono_topic,
    const std::string &left_rgb_topic,
    const std::string &right_rgb_topic
)
    : ExtendedStereoCamera(nh)
{
    subscribe(left_mono_topic, right_mono_topic, left_rgb_topic, right_rgb_topic);
}


/**
 * Constructor.
 *
 * Initialises the camera and subscribes to all the topics in one shot.
 *
 * @param nh The node handle used to initialise the ImageTransport and create the required
 * subscribers.
 * @param left_mono_topic The topic from which to read grayscale images for the left camera
 * @param right_mono_topic The topic from which to read grayscale images for the right camera
 * @param left_rgb_topic The topic from which to read colour images for the left camera
 * @param right_rgb_topic The topic from which to read color images for the right camera
 * @param cb A function to call each time a new image set is received.
 */
ExtendedStereoCamera::ExtendedStereoCamera(
    ros::NodeHandle nh,
    const std::string& left_mono_topic,
    const std::string& right_mono_topic,
    const std::string& left_rgb_topic,
    const std::string& right_rgb_topic,
    ExtendedStereoCamera::image_cb_t cb
)
    : ExtendedStereoCamera(nh)
{
    subscribe(left_mono_topic, right_mono_topic, left_rgb_topic, right_rgb_topic);
    setOnImageCallback(std::move(cb));
}


/**
 * Subscribe to the camera topics.
 *
 * @note:
 *  If you call StereoCamera::subscribe from within a nodelet and the image topics are remapped you
 *  might never see images. This is because image:_transport::getCameraInfoTopic does not check for
 *  remappings and assumes that the image topic it receives is the actual image topic.
 *
 * @param left_mono_topic The topic from which to read grayscale images for the left camera
 * @param right_mono_topic The topic from which to read grayscale images for the right camera
 * @param left_rgb_topic The topic from which to read colour images for the left camera
 * @param right_rgb_topic The topic from which to read color images for the right camera
 * @param hints TransportHints to request a specific transport. For most cases you can leave this
 * at the default value.
 */
void ExtendedStereoCamera::subscribe(
    const std::string &left_mono_topic,
    const std::string &right_mono_topic,
    const std::string &left_color_topic,
    const std::string &right_color_topic,
    const image_transport::TransportHints &hints)
{
    const auto resolved_left_mono_topic = _nh.resolveName(left_mono_topic);
    const auto resolve_right_mono_topic = _nh.resolveName(right_mono_topic);
    
    auto left_camera_info_topic = image_transport::getCameraInfoTopic(resolved_left_mono_topic);
    auto right_camera_info_topic = image_transport::getCameraInfoTopic(resolve_right_mono_topic);
    
    _left_image_mono_sub.subscribe(_it, left_mono_topic, 1, hints);
    _left_image_rgb_sub.subscribe(_it, left_color_topic, 1, hints);
    _left_image_camera_info_sub.subscribe(_nh, left_camera_info_topic, 1);
    
    _right_image_mono_sub.subscribe(_it, right_mono_topic, 1, hints);
    _right_image_rgb_sub.subscribe(_it, right_color_topic, 1, hints);
    _right_image_camera_info_sub.subscribe(_nh, right_camera_info_topic, 1);
    
    _synchroniser.connectInput(
        _left_image_mono_sub, _left_image_rgb_sub, _left_image_camera_info_sub,
        _right_image_mono_sub, _right_image_rgb_sub, _right_image_camera_info_sub
    );
    _subscribed = true;
    
    _adv_checker.check({
        _left_image_mono_sub.getTopic(), _left_image_rgb_sub.getTopic(),
        _right_image_mono_sub.getTopic(), _right_image_rgb_sub.getTopic(),
        _left_image_camera_info_sub.getTopic(), _right_image_camera_info_sub.getTopic()
    });
    
    ROS_INFO(
        "StereoCamera connected to: \n"
        "   - Mono Images: %s, %s\n"
        "   - Color Images: %s, %s\n"
        "   - Camera Info: %s, %s",
        _left_image_mono_sub.getTopic().c_str(), _right_image_mono_sub.getTopic().c_str(),
        _left_image_rgb_sub.getTopic().c_str(), _right_image_rgb_sub.getTopic().c_str(),
        _left_image_camera_info_sub.getTopic().c_str(), _right_image_camera_info_sub.getTopic().c_str()
    );
}



/**
 * Subscribe to the camera topics and set the onImage callback function.
 *
 * @param left_mono_topic The topic from which to read grayscale images for the left camera
 * @param right_mono_topic The topic from which to read grayscale images for the right camera
 * @param left_rgb_topic The topic from which to read colour images for the left camera
 * @param right_rgb_topic The topic from which to read color images for the right camera
 * @param cb A function to call each time a new image set is received.
 */
void ExtendedStereoCamera::subscribe(
    const std::string &left_mono_topic,
    const std::string &right_mono_topic,
    const std::string &left_color_topic,
    const std::string &right_color_topic,
    ExtendedStereoCamera::image_cb_t cb
)
{
    subscribe(left_mono_topic, right_mono_topic, left_color_topic, right_color_topic);
    setOnImageCallback(std::move(cb));
}


/**
 *
 * @param left_mono_topic The topic from which to read grayscale images for the left camera
 * @param right_mono_topic The topic from which to read grayscale images for the right camera
 * @param left_rgb_topic The topic from which to read colour images for the left camera
 * @param right_rgb_topic The topic from which to read color images for the right camera
 * @param hints
 *  TransportHints to request a specific transport. For most cases you can leave thisat the default
 *  value.
 * @param cb A function to call each time a new image pair is received.
 */
void ExtendedStereoCamera::subscribe(
    const std::string &left_mono_topic,
    const std::string &right_mono_topic,
    const std::string &left_color_topic,
    const std::string &right_color_topic,
    const image_transport::TransportHints &hints,
    ExtendedStereoCamera::image_cb_t cb)
{
    subscribe(left_mono_topic, right_mono_topic, left_color_topic, right_color_topic, hints);
    setOnImageCallback(std::move(cb));
}


/**
 * Unsubscribe from the image and camera_info topics and stop receiving images.
 *
 * This is useful to reduce the load on the ROS network when subscribing to nodes that turn off
 * stop emitting when their subscriber count drops to zero like the nodes of the \c image_proc
 * package.
 */
void ExtendedStereoCamera::unsubscribe()
{
    clearOnImageCallback();
    
    _left_image_mono_sub.unsubscribe();
    _left_image_rgb_sub.unsubscribe();
    _left_image_camera_info_sub.unsubscribe();
    _right_image_mono_sub.unsubscribe();
    _right_image_rgb_sub.unsubscribe();
    _right_image_camera_info_sub.unsubscribe();
    
    _subscribed = false;
    _stereo_image_count = 0;
    
    _adv_checker.stop();
}


bool ExtendedStereoCamera::waitForFirstImage(float timeout, unsigned int freq)
{
    auto start_time = ros::Time::now();
    
    ros::Rate loop_rate(freq);
    while(ros::ok()) {
        if (_stereo_image_count > 0)
            return true;
        
        if (timeout > 0  && (ros::Time::now() - start_time).toSec() > timeout) {
            return false;
        }
        
        loop_rate.sleep();
        ros::spinOnce();
    }
    
    // The loop was interrupted by ros::ok() before an image was received
    return false;
}


/**
 * Set the maximum interval between two images.
 *
 * This function is important ! Please read this carefully !
 *
 * The \c ApproximateTime policy doesn't know anything about your messages. It will happily match one image of the left
 * camera with the next image of the right camera. This is bad since anytime the scene moves the stereo reconstruction
 * will be working with images that are offset by one frame. This means that the accuracy of the reconstruction will
 * suffer.
 *
 * To avoid this you can set a maximum interval between between messages. This should be set to less than half of the
 * theoretical interval between frames. For example, if your camera captures images at 25Hz the inter-frame interval is
 * 40ms. Thus the maximum interval should be less than 20ms.
 *
 * @param max_interval
 *  The maximum interval between frames that should be considered to have arrived approximately at the same time.
 */
void ExtendedStereoCamera::setMaxImageInterval(const ros::Duration &max_interval)
{
    auto policy = _synchroniser.getPolicy();
    if (policy == nullptr) {
        ROS_ERROR("Unable to retrieve policy from Synchroniser object.");
        return;
    }
    
    policy->setMaxIntervalDuration(max_interval);
}


const ExtendedStereoImage ExtendedStereoCamera::getLatestImage()
{
    if (_stereo_image_count == 0) {
        BOOST_THROW_EXCEPTION(error::NoImageReceivedError()
            << error::camera_topic(_left_image_mono_sub.getTopic())
            << error::camera_topic(_right_image_mono_sub.getTopic())
            << error::camera_topic(_left_image_rgb_sub.getTopic())
            << error::camera_topic(_right_image_rgb_sub.getTopic())
        );
    }
    
    return {_left_mono_image, _right_mono_image, _left_rgb_image, _right_rgb_image};
}


/**
 * Get the latest sensor_msgs::CameraInfo for the left camera
 */
const sensor_msgs::CameraInfo &ExtendedStereoCamera::getLeftCameraInfo()
{
    return _camera_model.left().cameraInfo();
}


/**
 * Get the latest sensor_msgs::CameraInfo for the right camera
 */
const sensor_msgs::CameraInfo &ExtendedStereoCamera::getRightCameraInfo()
{
    return _camera_model.right().cameraInfo();
}


/**
 * Get the stereo camera model.
 */
const image_geometry::StereoCameraModel ExtendedStereoCamera::getCameraModel()
{
    return _camera_model;
}


void ExtendedStereoCamera::onStereoImagesReceived(
    const sensor_msgs::ImageConstPtr &left_mono,
    const sensor_msgs::ImageConstPtr &left_rgb,
    const sensor_msgs::CameraInfoConstPtr &left_camera_info,
    const sensor_msgs::ImageConstPtr &right_rgb,
    const sensor_msgs::ImageConstPtr &right_mono,
    const sensor_msgs::CameraInfoConstPtr &right_camera_info
)
{
    _left_mono_image = *left_mono;
    _left_rgb_image = *left_rgb;
    _right_mono_image = *right_mono;
    _right_rgb_image = *right_rgb;
    _camera_model.fromCameraInfo(left_camera_info, right_camera_info);
    _stereo_image_count += 1;
    
    if (_on_image_cb) {
        _on_image_cb(left_mono, right_mono, left_rgb, right_rgb, _camera_model);
    }
    
}
