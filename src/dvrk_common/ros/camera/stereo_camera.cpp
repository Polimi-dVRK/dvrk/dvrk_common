//
// Created by tibo on 21/07/17.
//

#include <cv_bridge/cv_bridge.h>
#include <image_transport/camera_common.h>

#include "dvrk_common/ros/camera/camera_errors.hpp"
#include "dvrk_common/ros/camera/stereo_camera.hpp"

using namespace dvrk;


/**
 * Default constructor.
 *
 * @param nh The node handle used to initialise the ImageTransport and create the required
 * subscribers.
 */
StereoCamera::StereoCamera(ros::NodeHandle nh)
    : _nh(nh)
    , _it(nh)
    , _synchroniser(ApproximateTimePolicy(5))
    , _adv_checker(nh)
{
    _synchroniser.registerCallback(&StereoCamera::onStereoImagesReceived, this);
}


/**
 * Constructor.
 *
 * @param nh
 *  The node handle used to initialise the ImageTransport and create the required subscribers.
 * @param left_camera_topic The topic from which to read images for the left camera
 * @param right_camera_topic The topic from which to read images for the left camera
 */
StereoCamera::StereoCamera(
    ros::NodeHandle nh,
    const std::string &left_camera_topic,
    const std::string &right_camera_topic
)
    : StereoCamera(nh)
{
    subscribe(left_camera_topic, right_camera_topic);
}


/**
 * Constructor.
 *
 * @param nh
 *  The node handle used to initialise the ImageTransport and create the required subscribers.
 * @param left_camera_topic The topic from which to read images for the left camera
 * @param right_camera_topic The topic from which to read images for the left camera
 * @param cb A function to call each time a new image pair is received.
 */
StereoCamera::StereoCamera(
    ros::NodeHandle nh,
    const std::string& left_camera_topic,
    const std::string& right_camera_topic,
    StereoCamera::image_cb_t cb
)
    : StereoCamera(nh)
{
    subscribe(left_camera_topic, right_camera_topic);
    setOnImageCallback(std::move(cb));
}


/**
 * Subscribe to the camera topics.
 *
 * @note:
 *  If you call StereoCamera::subscribe from within a nodelet and the image topics are remapped you
 *  might never see images. This is because image:_transport::getCameraInfoTopic does not check for
 *  remappings and assumes that the image topic it receives is the actual image topic.
 *
 * @param left_camera_topic The topic on which to listen for images from the left camera
 * @param right_camera_topic The topic on which to listen for images from the right camera
 * @param hints
 *  TransportHints to request a specific transport. For most cases you can leave thisat the default
 *  value.
 */
void StereoCamera::subscribe(
    const std::string &left_camera_topic,
    const std::string &right_camera_topic,
    const image_transport::TransportHints &hints
)
{
    const auto resolved_left_camera_topic = _nh.resolveName(left_camera_topic);
    const auto resolved_right_camera_topic = _nh.resolveName(right_camera_topic);

    const auto left_camera_info_topic = 
        image_transport::getCameraInfoTopic(resolved_left_camera_topic);
    const auto right_camera_info_topic = 
        image_transport::getCameraInfoTopic(resolved_right_camera_topic);
    
    _left_image_sub.subscribe(_it, resolved_left_camera_topic, 1, hints);
    _left_image_camera_info_sub.subscribe(_nh, left_camera_info_topic, 1);
    
    _right_image_sub.subscribe(_it, resolved_right_camera_topic, 1, hints);
    _right_image_camera_info_sub.subscribe(_nh, right_camera_info_topic, 1);

    _synchroniser.connectInput(
        _left_image_sub, _left_image_camera_info_sub,
        _right_image_sub, _right_image_camera_info_sub
    );
    _subscribed = true;

    _adv_checker.check({
        _left_image_sub.getTopic(), _right_image_sub.getTopic(),
        _left_image_camera_info_sub.getTopic(), _right_image_camera_info_sub.getTopic()
    });
    
    ROS_INFO_STREAM(
        "" << "StereoCamera connected to: \n"
           << " - Images; " << _left_image_sub.getTopic().c_str() << ", " << _right_image_sub.getTopic().c_str() << "\n"
           << " - Camera Info: " << _left_image_camera_info_sub.getTopic().c_str() << ", " << _right_image_camera_info_sub.getTopic().c_str() << "\n"
    );
}


/**
 * Subscribe to the camera topics and set the onImage callback function.
 *
 * @param left_camera_topic The topic on which to listen for images from the left camera
 * @param right_camera_topic The topic on which to listen for images from the right camera
 * @param cb A function to call each time a new image pair is received.
 */
void StereoCamera::subscribe(
    const std::string &left_camera_topic,
    const std::string &right_camera_topic,
    image_cb_t cb
) {
    subscribe(left_camera_topic, right_camera_topic);
    setOnImageCallback(std::move(cb));
}


/**
 * Subscribe to the camera topics and set the onImage callback function.
 *
 * @param left_camera_topic The topic on which to listen for images from the left camera
 * @param right_camera_topic The topic on which to listen for images from the right camera
 * @param hints
 *  TransportHints to request a specific transport. For most cases you can leave thisat the default
 *  value.
 * @param cb A function to call each time a new image pair is received.
 */
void StereoCamera::subscribe(
    const std::string &left_camera_topic,
    const std::string &right_camera_topic,
    const image_transport::TransportHints &hints,
    image_cb_t cb
) {
    subscribe(left_camera_topic, right_camera_topic, hints);
    setOnImageCallback(std::move(cb));
}


/**
 * Unsubscribe from the image and camera_info topics and stop receiving images.
 *
 * This is useful to reduce the load on the ROS network when subscribing to nodes that turn off
 * stop emitting when their subscriber count drops to zero like the nodes of the \c image_proc
 * package.
 */
void StereoCamera::unsubscribe()
{
    clearOnImageCallback();
    
    _left_image_sub.unsubscribe();
    _left_image_camera_info_sub.unsubscribe();
    _right_image_sub.unsubscribe();
    _right_image_camera_info_sub.unsubscribe();
    
    _subscribed = false;
    _stereo_image_count = 0;
    
    _adv_checker.stop();
}


bool StereoCamera::waitForFirstImage(float timeout, unsigned int freq)
{
    auto start_time = ros::Time::now();

    ros::Rate loop_rate(freq);
    while(ros::ok()) {
        if (_stereo_image_count > 0)
            return true;

        if (timeout > 0  && (ros::Time::now() - start_time).toSec() > timeout) {
            return false;
        }

        loop_rate.sleep();
        ros::spinOnce();
    }

    // The loop was interrupted by ros::ok() before an image was received
    return false;
}


const StereoImage StereoCamera::getLatestImage()
{
    if (_stereo_image_count == 0) {
        BOOST_THROW_EXCEPTION(error::NoImageReceivedError()
                << error::camera_topic(_left_image_sub.getTopic())
                << error::camera_topic(_right_image_sub.getTopic())
        );
    }

    return {_left_image, _right_image};
}


const sensor_msgs::Image& StereoCamera::getLeftImage() {
    if (_stereo_image_count == 0) {
        BOOST_THROW_EXCEPTION(error::NoImageReceivedError()
            << error::camera_topic(_left_image_sub.getTopic())
        );
    }
    
    return _left_image;
}


const sensor_msgs::Image& StereoCamera::getRightImage() {
    if (_stereo_image_count == 0) {
        BOOST_THROW_EXCEPTION(error::NoImageReceivedError()
            << error::camera_topic(_right_image_sub.getTopic())
        );
    }
    
    return _right_image;
}



/**
 * Set the maximum interval between two images.
 *
 * This function is important ! Please read this carefully !
 *
 * The \c ApproximateTime policy doesn't know anything about your messages. It will happily match one image of the left
 * camera with the next image of the right camera. This is bad since anytime the scene moves the stereo reconstruction
 * will be working with images that are offset by one frame. This means that the accuracy of the reconstruction will
 * suffer.
 *
 * To avoid this you can set a maximum interval between between messages. This should be set to less than half of the
 * theoretical interval between frames. For example, if your camera captures images at 25Hz the inter-frame interval is
 * 40ms. Thus the maximum interval should be less than 20ms.
 *
 * @param max_interval
 *  The maximum interval between frames that should be considered to have arrived approximately at the same time.
 */
void StereoCamera::setMaxImageInterval(const ros::Duration& max_interval)
{
    auto policy = _synchroniser.getPolicy();
    if (policy == nullptr) {
        ROS_ERROR("Unable to retrieve policy from Synchroniser object.");
        return;
    }

    policy->setMaxIntervalDuration(max_interval);
}


void StereoCamera::onStereoImagesReceived(
    const sensor_msgs::ImageConstPtr &left,
    const sensor_msgs::CameraInfoConstPtr &left_camera_info,
    const sensor_msgs::ImageConstPtr &right,
    const sensor_msgs::CameraInfoConstPtr &right_camera_info
)
{
    _left_image = *left;
    _right_image = *right;
    _camera_model.fromCameraInfo(left_camera_info, right_camera_info);
    _stereo_image_count += 1;
    
    if (_on_image_cb) {
        _on_image_cb(left, right, _camera_model);
    }
}


/**
 * Get the latest sensor_msgs::CameraInfo for the left camera
 */
const sensor_msgs::CameraInfo &StereoCamera::getLeftCameraInfo()
{
    return _camera_model.left().cameraInfo();
}


/**
 * Get the latest sensor_msgs::CameraInfo for the right camera
 */
const sensor_msgs::CameraInfo &StereoCamera::getRightCameraInfo()
{
    return _camera_model.right().cameraInfo();
}


/**
 * Get the stereo camera model.
 */
const image_geometry::StereoCameraModel StereoCamera::getCameraModel()
{
    return _camera_model;
}
