//
// Created by tibo on 07/06/17.
//

#include "dvrk_common/ros/camera/camera_errors.hpp"
#include "dvrk_common/ros/camera/simple_camera.hpp"

using namespace dvrk;

/**
 * Constructor
 *
 * @param node The ROS node used to initialise the \c image_transport::ImageTransport.
 */
SimpleCamera::SimpleCamera(const ros::NodeHandle &node)
    : _node(node), _it(node)
{
    // Do nothing
}


/**
 * Constructor
 *
 * @param node The ROS node used to initialise the \c image_transport::ImageTransport.
 * @param topic The topic on which to listen for images.
 */
SimpleCamera::SimpleCamera(const ros::NodeHandle &node, const std::string &topic)
    : SimpleCamera(node)
{
    subscribe(topic);
}


/**
 * Constructor
 *
 * @param node The ROS node used to initialise the \c image_transport::ImageTransport.
 * @param topic The topic on which to listen for images.
 * @param cb The callback function to call each time a new image is received.
 */
SimpleCamera::SimpleCamera(
    const ros::NodeHandle &node,
    const std::string &topic,
    SimpleCamera::image_cb_t cb
)
    : SimpleCamera(node)
{
       subscribe(topic, std::move(cb));
}

void SimpleCamera::subscribe(const std::string &topic, const image_transport::TransportHints &hints)
{
    _camera = _it.subscribeCamera(topic, 1, &SimpleCamera::OnImageReceived, this, hints);
    _adv_checker.check({ _camera.getTopic(), _camera.getInfoTopic() });
}


void SimpleCamera::subscribe(const std::string &topic, image_cb_t cb)
{
    subscribe(topic);
    setOnImageCallback(std::move(cb));
}


void SimpleCamera::subscribe(
    const std::string &topic,
    const image_transport::TransportHints &hints,
    image_cb_t cb
) {
    subscribe(topic, hints);
    setOnImageCallback(std::move(cb));
}


void SimpleCamera::unsubscribe() {
    _camera.shutdown();
    _adv_checker.stop();
}


/**
 * Retrieve the latest image.
 *
 * To ensure that this function will return a real image you should call #SimpleCamera::getImageCount() first and check
 * that the image count is greater tha 0. Otherwise the `data` member of the image will be empty.
 *
 * @return A \c boost::optional containg a \c sensor_msgs::Image containing the latest image received if an image has
 * been recevied within the lifetime of the node. Otherwise the optional will be empty and evaluate to false.
 */
const sensor_msgs::Image & SimpleCamera::getLatestImage() const
{
    if (_image_count == 0) {
        BOOST_THROW_EXCEPTION(
            error::NoImageReceivedError() << error::camera_topic( _camera.getTopic())
        );
    }

    return _latest_image;
}


/**
 * Wait for at most #timeout seconds for the first image to appear on the monitored topic. It is checked at a the
 * frequency specified by #freq.
 *
 * This is useful at the beginning of a script to ensure that images have started to arrive instead of having to check
 * manually for empty images.
 *
 * @param timeout How long to wait for the first image to arrive.
 * @param freq The frequency at which to check if an image has been recieved
 * @return true if the image counter is greater than 1 by the time the #timeout runs out, false otherwise
 */
bool SimpleCamera::waitForFirstImage(float timeout, unsigned int freq) {
    auto start_time = ros::Time::now();

    ros::Rate loop_rate(freq);
    while(ros::ok()) {
        if (_image_count > 0)
            return true;

        if (timeout > 0  && (ros::Time::now() - start_time).toSec() > timeout) {
            return false;
        }

        loop_rate.sleep();
        ros::spinOnce();
    }
    
    // The loop was interrupted by ros::ok() before an image was received
    return false;
}


/**
 * Get the number of images received by the camera so far.
 *
 * @return The number of images received by the camera so far.
 */
u_int SimpleCamera::getImageCount() const
{
    return _image_count;
}

/**
 * Callback called when a new image is received.
 *
 * @param img The image
 * @param camera_info The intrinsic parameters of the camera
 */
void SimpleCamera::OnImageReceived(
    const sensor_msgs::ImageConstPtr &img,
    const sensor_msgs::CameraInfoConstPtr &camera_info
)
{
    _latest_image = *img;
    _camera_model.fromCameraInfo(camera_info);
    _image_count += 1;

    if (_on_image_cb) {
        _on_image_cb(img, _camera_model);
    }
    
}
