//
// Created by tibo on 04/09/17.
//

#include <ros/master.h>
#include <ros/names.h>

#include "dvrk_common/ros/tools/advertisement_checker.hpp"

using namespace dvrk;

AdvertisementChecker::AdvertisementChecker(const ros::NodeHandle &nh)
    : _node(nh)
{
    // Do Nothing
}

/**
 * Check if the specified topics exist on the ros::NodeHandle passed into the constructor. 
 * The topics will be resolved before being checked to avoid false positives.
 *
 * @param topics The topics who's existence you want to check
 * @param duration How long to wait for the topics to become available
 * @param immediate Should the topic check be run now or should we wait for the timer to trigger it ?
 */
void AdvertisementChecker::check(const std::vector<std::string>& topics, const double period, const bool immediate)
{
    stop();
    
    // Resolve all the topics we received to ensure that we actually look for the right topics 
    _topics.reserve(topics.size());
    for (const auto& topic : topics)  {
        _topics.emplace_back(ros::names::resolve(topic));
    }

    _timer = _node.createWallTimer(ros::WallDuration(period), boost::bind(&AdvertisementChecker::onTimer, this));
    
    if (immediate) // Make sure to start checking immediatley
        onTimer();
}


AdvertisementChecker::~AdvertisementChecker()
{
    if (!_topics.empty()) {
        ROS_WARN("This dvrk::AdvertisementChecker is shutting down before all the requested topics are advertised. Is this normal?");
    }
}


/**
 * Stop the timer and consequently stop checking to see if all the topics are advertised.
 */
void AdvertisementChecker::stop()
{
    _timer.stop();
}

/**
 * Called each time the timer callback fires.
 *
 * In the callback we loop through the list of topics advertised by the ROS master and check to see if our required
 * topics (\c _topics) are advertised. if a topic is advertised we remove it from the list of required topics. This
 * continues until the list of required topics is empty.
 */
void AdvertisementChecker::onTimer()
{
    std::vector<ros::master::TopicInfo> advertised_topics;
    ros::master::getTopics(advertised_topics);
    
    std::vector<std::string> missing_topics;
    for (const auto this_topic: _topics) {
        auto found = std::find_if(advertised_topics.begin(), advertised_topics.end(), [&this_topic](const auto &topic) {
            return (this_topic == topic.name);
        });
        
        if (found == advertised_topics.end()) {
            ROS_WARN_NAMED(
                "AdvertisementChecker",
                "The required topic <%s> is not advertised yet.", this_topic.c_str()
            );
            missing_topics.push_back(this_topic);
        }
    }
    
    if (missing_topics.empty()) _timer.stop();
    _topics = missing_topics;
}
