//
// Created by tibo on 18/09/17.
//



#include <memory>

#include <opencv2/highgui.hpp>

#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include "dvrk_common/ros/camera/simple_camera.hpp"

class ImageViewerNodelet: public nodelet::Nodelet
{
public:
    
    void onInit() override;

private:
    
    std::unique_ptr<image_transport::ImageTransport> _it;
    std::unique_ptr<dvrk::SimpleCamera> _camera;
    
};

void ImageViewerNodelet::onInit()
{
    ros::NodeHandle& nh = getNodeHandle();
    // cv::namedWindow("Nodelet - Side by Side View", cv::WINDOW_NORMAL);
    
    _it = std::make_unique<image_transport::ImageTransport>(nh);
    _camera = std::make_unique<dvrk::SimpleCamera>(nh);
    _camera->subscribe("image_raw", [this](
        const sensor_msgs::ImageConstPtr& image,
        const image_geometry::PinholeCameraModel&
    ) {
        cv::Mat cv_image = cv_bridge::toCvShare(image, "bgr8")->image;
        cv::imshow(this->_camera->getTopic(), cv_image);
        cv::waitKey(1);
    });
}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(ImageViewerNodelet, nodelet::Nodelet)
