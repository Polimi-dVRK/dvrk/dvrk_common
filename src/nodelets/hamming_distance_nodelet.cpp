//
// Created by tibo on 08/09/17.
//

#include <cv_bridge/cv_bridge.h>

#include "dvrk_common/ros/params.hpp"
#include "nodelets/hamming_distance_nodelet.hpp"

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(dvrk::HammingDistanceNodelet, nodelet::Nodelet)

using namespace dvrk;

void HammingDistanceNodelet::onInit()
{
    ros::NodeHandle& nh = getNodeHandle();
    ros::NodeHandle& private_nh = getPrivateNodeHandle();
    
    if (const auto queue_size = getParam<int>(private_nh, "queue_size"))
        _queue_size = queue_size.get();
    
    _it = std::make_unique<image_transport::ImageTransport>(nh);
    _synchroniser = std::make_unique<ApproximateTimeSynchroniser>(
        ApproximateTimePolicy(_queue_size), _first, _second
    );
    _synchroniser->registerCallback(&HammingDistanceNodelet::onImage, this);
    
    image_transport::SubscriberStatusCallback connect_cb =
        boost::bind(&HammingDistanceNodelet::onConnect, this);
     _publisher = _it->advertise("hamming_distance", 1, connect_cb, connect_cb);
}


/**
 * Callback used for (dis)connect events on the output topic.
 *
 * This enables and disables the publisher and subscriber based on the number of subscribers to the output to avoid
 * useless work. If there are no subscribers then it makes no sense to publish images so the subscriber is shutdown.
 */
void HammingDistanceNodelet::onConnect()
{
    if (_publisher.getNumSubscribers() == 0) {
        _first.unsubscribe();
        _second.unsubscribe();
        return;
    }
    
    if (!_first.getSubscriber()) {
        ROS_INFO("Connecting HammingDistance susbscribers");
        
        image_transport::TransportHints hints("raw", ros::TransportHints(), getPrivateNodeHandle());
        _first.subscribe(*_it, "image_0", 1, hints);
        _second.subscribe(*_it, "image_1", 1, hints);
        
        ROS_INFO("First = %s", _first.getTopic().c_str());
        ROS_INFO("Second = %s", _second.getTopic().c_str());
    }
}


/**
 * Callback called when a pair of images has been susccessfully received. It computes the hamming distance between the
 * two and publishes the result.
 *
 * @param image0 The first image
 * @param image1 The second image
 */
void HammingDistanceNodelet::onImage(
    const sensor_msgs::ImageConstPtr &image0,
    const sensor_msgs::ImageConstPtr &image1
) {
    const cv_bridge::CvImageConstPtr cv_image0 = cv_bridge::toCvShare(image0);
    const cv_bridge::CvImageConstPtr cv_image1 = cv_bridge::toCvShare(image1);
    
     cv_bridge::CvImage result(image0->header, "mono16", _hamming.apply(cv_image0->image, cv_image1->image));
    _publisher.publish(result.toImageMsg());
}


