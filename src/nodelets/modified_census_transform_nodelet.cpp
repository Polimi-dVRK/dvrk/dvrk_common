//
// Created by tibo on 05/09/17.
//

#include <cv_bridge/cv_bridge.h>

#include "dvrk_common/opencv/util.hpp"
#include "nodelets/modified_census_transform_nodelet.hpp"

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(dvrk::ModifiedCensusTransformNodelet, nodelet::Nodelet)

using namespace dvrk;

void ModifiedCensusTransformNodelet::onInit()
{
    ros::NodeHandle& nh = getNodeHandle();
    ros::NodeHandle& private_nh = getPrivateNodeHandle();
    _it.reset(new image_transport::ImageTransport(nh));
    
    private_nh.param("queue_size", _queue_size, 5);
    private_nh.param("mct_window_size", _window_size, 3);
    
    image_transport::SubscriberStatusCallback connect_cb = boost::bind(&ModifiedCensusTransformNodelet::onConnect, this);
    _publisher = _it->advertise("modified_census", 1, connect_cb, connect_cb);
}


/**
 * Callback used for (dis)connect events on the output topic.
 *
 * This enables and disables the publisher and subscriber based on the number of subscribers to the output to avoid
 * useless work. If there are no subscribers then it makes no sense to publish images so the subscriber is shutdown.
 */
void ModifiedCensusTransformNodelet::onConnect() {
    if (_publisher.getNumSubscribers() == 0) {
        _subscriber.shutdown();
        return;
    }
    
    if (!_subscriber) {
        image_transport::TransportHints hints("raw", ros::TransportHints(), getPrivateNodeHandle());
        _subscriber = _it->subscribe("image_mono", _queue_size,  &ModifiedCensusTransformNodelet::onImage, this);
    }
}


/**
 * Callback called when a new image is received. It computes the modified census transform of the input image and
 * publishes the result.
 *
 * @param img The image to process
 */
void ModifiedCensusTransformNodelet::onImage(const sensor_msgs::ImageConstPtr &img)
{
    auto cv_img = cv_bridge::toCvShare(img, "mono8")->image;
    
    if (!_mct) {
        _mct.reset(new dvrk::ModifiedCensusTransform(cv::Size(_window_size, _window_size)));
    }
    
    const cv::Mat& result = _mct->apply(cv_img);
    cv_bridge::CvImage processed_image(img->header, matTypeToString(result), result);
    _publisher.publish(processed_image.toImageMsg());
}
