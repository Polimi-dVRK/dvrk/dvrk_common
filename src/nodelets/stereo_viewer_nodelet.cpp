//
// Created by tibo on 11/09/17.
//


#include <memory>

#include <opencv2/highgui.hpp>

#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include "dvrk_common/ros/camera/stereo_camera.hpp"

void onStereoViewerCameraImage(
    const sensor_msgs::ImageConstPtr& left,
    const sensor_msgs::ImageConstPtr& right,
    const image_geometry::StereoCameraModel&
)
{
    cv::Mat cv_left = cv_bridge::toCvShare(left, "bgr8")->image;
    cv::Mat cv_right = cv_bridge::toCvShare(right, "bgr8")->image;
    
    assert(cv_left.cols == cv_right.cols && cv_left.type() == cv_right.type());
    cv::Mat side_by_side(cv_left.rows, cv_left.cols + cv_right.cols, cv_left.type());
    
    cv_left.copyTo(side_by_side(cv::Rect(0, 0, cv_left.cols, cv_left.rows)));
    cv_right.copyTo(side_by_side(cv::Rect(cv_left.cols, 0, cv_right.cols, cv_right.rows)));
    
    cv::imshow("Nodelet - Side by Side View", side_by_side);
    cv::waitKey(1);
}

class StereoViewerNodelet: public nodelet::Nodelet
{
public:
    
    void onInit() override;
    
private:
    
    std::unique_ptr<image_transport::ImageTransport> _it;
    std::unique_ptr<dvrk::StereoCamera> _camera;
    
};

void StereoViewerNodelet::onInit()
{
    ros::NodeHandle& nh = getNodeHandle();
    // cv::namedWindow("Nodelet - Side by Side View", cv::WINDOW_NORMAL);
    
    _it = std::make_unique<image_transport::ImageTransport>(nh);
    _camera = std::make_unique<dvrk::StereoCamera>(nh);
    _camera->subscribe("left/image_raw", "right/image_raw", onStereoViewerCameraImage);
}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(StereoViewerNodelet, nodelet::Nodelet)
