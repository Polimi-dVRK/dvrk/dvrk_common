//
// Created by tibo on 26/03/18.
//

#include <ros/ros.h>
#include <dvrk_common/ros/ros_graph_state.hpp>

int main(int argc, char** argv) {
    
    ros::init(argc, argv, "rosdump");
    ROS_INFO_STREAM(dvrk::dumpROSState());

    return 0;
}
